/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.arguments;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.web.reactive.filter.OrderedWebFilter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Command {

  Arguments owner;
  private final String name;
  private final Map<String, Argument> arguments;

  private Command(String name) {
    this.name = name;
    this.owner = owner;
    this.arguments = new HashMap<>();
  }

  public static Command newInstance(String name) {
    return new Command(name);
  }

  public Command addArgument(String name, int valueCount, boolean required) {
    this.arguments.put(name, new Argument(name, valueCount, required));
    return this;
  }

  public String validate(ApplicationArguments appArgs) {
    Map<String, List<String>> args = appArgs.getOptionNames().stream()
        .collect(Collectors.toMap(n->n, appArgs::getOptionValues));

    for (Argument arg : this.arguments.values()) {
      List<String> values = args.get(arg.getName());
      if ((values == null || values.isEmpty()) && arg.isRequired()) {
        return "Argument " + arg.getName() + " required";
      }
      if (values.size() > arg.getValueCoun()) {
        return "Too many values for argument " + arg.getName();
      }
    }
    //Valid
    return null;
  }

  public String getArgumentValue(String name, ApplicationArguments appArgs) {
    List<String> values = appArgs.getOptionValues(name);
    if (values == null || values.isEmpty()) {
      return null;
    }
    return values.get(0);
  }

  public List<String> getArgumentValues(String name, ApplicationArguments appArgs) {
    List<String> values = appArgs.getOptionValues(name);
    if (values == null) {
      return new ArrayList<>(0);
    }
    return values;
  }

  public File getArgumentValueAsFile(String name, ApplicationArguments appArgs) {
    String value = getArgumentValue(name, appArgs);
    if (value == null) {
      return null;
    }
    File file = new File(value);
    return file;
  }

  public Arguments owner() {
    return this.owner;
  }

}
