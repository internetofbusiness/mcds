/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli;

import ee.tieto.mcds.cli.arguments.Arguments;
import ee.tieto.mcds.cli.arguments.Command;
import ee.tieto.mcds.cli.generator.TestXbrlGenerator;

import ee.tieto.mcds.cli.schematron.SchematronTools;
import ee.tieto.mcds.cli.validate.ValidatorTools;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsoleApplication implements ApplicationRunner {

  private final Printer out;
  private final TestXbrlGenerator generator;
  private final SchematronTools schematron;
  private final ValidatorTools validator;
  private final Arguments argsStruct;

  public ConsoleApplication(Printer out, TestXbrlGenerator generator, SchematronTools schematron, ValidatorTools validator) {
    this.out = out;
    this.generator = generator;
    this.schematron = schematron;
    this.validator = validator;
    this.argsStruct = initArgsStruct();
  }

  public static void main(String[] args) {
    SpringApplication.run(ConsoleApplication.class, args);
  }

  @Override
  public void run(ApplicationArguments args) {
    String message = this.argsStruct.validateCommands(args);
    if (message != null) {
      printError(message);
      return;
    }
    String command = this.argsStruct.getCommand(args);

    switch (command) {
      case "example":
        generator.generateXbrl(args);
        break;
      case "schematron":
        doSchematron(args);
        break;
      case "validate":
        validator.validate(args, this.argsStruct);
        break;
      case "help":
        printHelp();
        break;
      default:
        out.println("ERROR - unimplemented command: " + command);
        out.println();
        printHelp();
        break;
    }
    out.println();
    out.println("Done");
  }

  private void doSchematron(ApplicationArguments args) {
    String message = this.argsStruct.validateCommand("schematron", args);
    if (message != null) {
      printError(message);
      return;
    }
    schematron.doIt(this.argsStruct.getArgumentValue("schematron", "version", args));
  }

  private Arguments initArgsStruct() {
    return Arguments.newInstance()
        .addCommand("help", Command.newInstance("help"))
        .addCommand("example", Command.newInstance("example"))
        .addCommand("schematron", Command.newInstance("example")
            .addArgument("version", 1, true))
        .addCommand("validate", Command.newInstance("validate")
            .addArgument("xbrl", 1, true)
            .addArgument("version", 1, true));
  }

  private void printError(String message) {
    out.println(message);
    out.println();
    printHelp();
  }

  private void printHelp() {
    out.println("MCDS cli tool");
    out.println("usage: command --option=value ... --option=value");
    out.println("");
    out.println("Commands:");
    out.println("\texample - generates example xbrl-gl file");
    out.println("\tschematron - Schematron rules to xsl preparation");
    out.println("\tvalidate - validate xbrl-gl file");
    out.println("\thelp - this help");
  }

}
