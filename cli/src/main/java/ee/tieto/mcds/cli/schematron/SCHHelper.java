/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.schematron;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SCHHelper {

  public static SCHInfo collectInfo(byte[] bytes) throws Exception {
    ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
    return collectInfo(bais);
  }


  public static SCHInfo collectInfo(InputStream ins) throws Exception {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true);
    factory.setIgnoringComments(true);
    factory.setIgnoringElementContentWhitespace(true);
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document doc = builder.parse(ins);
    Element root = doc.getDocumentElement();

    XPath phasesXpath = XPathFactory.newInstance().newXPath();
    phasesXpath.setNamespaceContext(new MyNamespaceContext());
    XPathExpression expr = phasesXpath.compile("./sch:phase");
    NodeList pathList = (NodeList)expr.evaluate(root, XPathConstants.NODESET);

    List<String> phases = new ArrayList<>(pathList.getLength());
    for (int i = 0; i < pathList.getLength(); i++) {
      Element phaseElement = (Element)pathList.item(i);
      phases.add(phaseElement.getAttribute("id"));
    }

    SCHInfo info = new SCHInfo();
    info.setPhases(phases);
    return info;
  }

  private static class MyNamespaceContext implements NamespaceContext {
    @Override
    public String getNamespaceURI(String prefix) {
      return "sch".equals(prefix) ? "http://purl.oclc.org/dsdl/schematron" : null;
    }

    @Override
    public String getPrefix(String namespaceURI) {
      return null;
    }

    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
      return null;
    }
  }
}
