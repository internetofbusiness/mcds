/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.validate;

import ee.tieto.mcds.cli.Printer;
import ee.tieto.mcds.cli.arguments.Arguments;
import ee.tieto.mcds.validator.BusinessValidator;
import ee.tieto.mcds.validator.BusinessValidator.ValidationContext;
import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.Validator;
import ee.tieto.mcds.validator.XbrlValidatorFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;

@Component
public class ValidatorTools {
  private static final String COMMAND_NAME = "validate";

  private final Printer out;
  private final XbrlValidatorFactory validatorFactory;

  public ValidatorTools(Printer out, XbrlValidatorFactory validatorFactory) {
    this.out = out;
    this.validatorFactory = validatorFactory;
  }

  public void validate(ApplicationArguments args, Arguments argStruct) {
    String message = argStruct.validateCommand(COMMAND_NAME, args);
    if (message != null) {
      printError(message);
      return;
    }

    this.validatorFactory.init();

    File xbrlFile = argStruct.getArgumentValueAsFile(COMMAND_NAME, "xbrl", args);
    if (!xbrlFile.canRead()) {
      printError("Can't read file " + xbrlFile.getAbsolutePath());
      return;
    }

    String version = argStruct.getArgumentValue(COMMAND_NAME, "version", args);

    Validator validator = validatorFactory.getSchemaValidator(version);
    if (validator == null) {
      throw new RuntimeException("Unsupported xbrl-gl version - " + version);
    }
    ValidationResult result = null;
    try (InputStream is = new BufferedInputStream(new FileInputStream(xbrlFile))) {
      result = validator.validate(new BufferedInputStream(is));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    if (!result.isValid()) {
      writeResult(xbrlFile, result);
      return;
    }
    validateBusiness(xbrlFile, version);
  }

  private void validateBusiness(File xbrlFile, String version) {
    List<BusinessValidator> validators = validatorFactory.getBusinessValidators(version, ValidationContext.ENTRY);
    if (validators == null || validators.isEmpty()) {
      out.println("No validators for business rules validatiopn");
      return;
    }

    try {
      Document result = validators.get(0).validate(new BufferedInputStream(new FileInputStream(xbrlFile)));
      writeResult(xbrlFile, result);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Document fileToDocument(File file) {
    SAXReader reader = new SAXReader();
    try {
      return reader.read(file);
    } catch (DocumentException e) {
      throw new RuntimeException(e);
    }
  }

  private void writeResult(File xbrlFile, Document result) {
    File outFile = new File(xbrlFile.getPath() + ".result.xml");
    OutputFormat format = new OutputFormat();
    format.setNewlines(true);
    format.setIndent(true);
    format.setIndent("\t");
    try (Writer streamWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile)))){
      XMLWriter writer = new XMLWriter(streamWriter, format);
      writer.write(result);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void writeResult(File xbrlFile, ValidationResult result) {
    File outFile = new File(xbrlFile.getPath() + ".result_xsd");
    try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile)))){
      writer.write(result.toString());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void printError(String message) {
    out.println(message);
    out.println();
    printHelp();
  }

  private void printHelp() {
    out.println("Validate xbrl-gl file");
    out.println("Usage:");
    out.println("\tvalidate --help");
    out.println("\tvalidate --rules=rules_file --xbrl=xbrl-gl_file");
    out.println("");
    out.println("Arguments:");
    out.println("\txbrl - xbrl-gel file to validate");
    out.println("\tversion - xbrl-gl rules version to use");
    out.println("\thelp - this help");
  }

}
