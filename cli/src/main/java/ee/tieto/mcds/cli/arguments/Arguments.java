/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.arguments;

import org.springframework.boot.ApplicationArguments;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Arguments {

  private final Map<String, Command> commands = new HashMap<>();

  private Arguments() {
  }

  public static Arguments newInstance() {
    return new Arguments();
  }

  public Arguments addCommand(String name, Command command) {
    command.owner = this;
    this.commands.put(name, command);
    return this;
  }

  public String validateCommands(ApplicationArguments args) {
    List<String> commands = args.getNonOptionArgs();
    if (commands == null || commands.isEmpty()) {
      return "Command required";
    }

    if (commands.size() > 1) {
      return "Only one command allowed, currently " + commands;
    }

    if (!this.commands.containsKey(commands.get(0))) {
      return "Unknown command - " + commands.get(0);
    }
    //Valid
    return null;
  }

  public String validateCommand(String command, ApplicationArguments args) {
    if (!this.commands.containsKey(command)) {
      return "Unknown command - " + command;
    }
    return this.commands.get(command).validate(args);
  }

  public String getCommand(ApplicationArguments args) {
    return args.getNonOptionArgs().get(0);
  }

  public String getArgumentValue(String commandName, String argumentName, ApplicationArguments args) {
    return commands.get(commandName).getArgumentValue(argumentName, args);
  }

  public List<String> getArgumentValues(String commandName, String argumentName, ApplicationArguments args) {
    return commands.get(commandName).getArgumentValues(argumentName, args);
  }

  public File getArgumentValueAsFile(String commandName, String argumentName, ApplicationArguments args) {
    return commands.get(commandName).getArgumentValueAsFile(argumentName, args);
  }

}
