/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.schematron;

import com.helger.commons.io.file.FileHelper;
import com.helger.commons.io.resource.inmemory.ReadableResourceByteArray;
import com.helger.schematron.svrl.CSVRL;
import com.helger.schematron.xslt.ISchematronXSLTBasedProvider;
import com.helger.schematron.xslt.SCHTransformerCustomizer;
import com.helger.schematron.xslt.SchematronResourceSCHCache;
import com.helger.xml.XMLHelper;
import com.helger.xml.namespace.MapBasedNamespaceContext;
import com.helger.xml.serialize.write.XMLWriter;
import com.helger.xml.serialize.write.XMLWriterSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.XMLConstants;
import java.io.*;
import java.util.Arrays;

@Component
public class XslPreparator {

  @Value("${mcds.validation.rulesdirectory}")
  private String rulesDirectory;

  public void prepare(String rulesVersion) {
    File ruleLocation = new File(rulesDirectory + "/" + rulesVersion + "/schematron/sch");
    File[] schFiles = ruleLocation.listFiles((dir, name) -> name.endsWith(".sch"));
    if (schFiles == null) {
      return;
    }
    Arrays.stream(schFiles).forEach(this::prepareFile);
  }

  private void prepareFile(File schFile) {
    try {
    byte[] schBytes = readFile(schFile);
      SCHInfo schInfo = SCHHelper.collectInfo(schBytes);
      schInfo.getPhases().forEach(p -> preparePhase(schBytes, getXsltFile(schFile, p), p));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  protected void preparePhase(byte[] sch, File xslFile, String phase) {
    try {
      final ReadableResourceByteArray resource = new ReadableResourceByteArray(sch, false);

      final SCHTransformerCustomizer aCustomizer = new SCHTransformerCustomizer()
          .setForceCacheResult(false);
      if (phase != null) {
        aCustomizer.setPhase (phase);
      }

      final ISchematronXSLTBasedProvider aXsltProvider =
          SchematronResourceSCHCache.createSchematronXSLTProvider(resource, aCustomizer);

      final MapBasedNamespaceContext aNSContext =
          new MapBasedNamespaceContext().addMapping("svrl", CSVRL.SVRL_NAMESPACE_URI);
      // Add all namespaces from XSLT document root
      final String sNSPrefix = XMLConstants.XMLNS_ATTRIBUTE + ":";
      XMLHelper.forAllAttributes(aXsltProvider.getXSLTDocument().getDocumentElement(), (sAttrName, sAttrValue) -> {
        if (sAttrName.startsWith(sNSPrefix)) {
          aNSContext.addMapping(sAttrName.substring(sNSPrefix.length()), sAttrValue);
        }
      });

      final XMLWriterSettings aXWS = new XMLWriterSettings();
      aXWS.setNamespaceContext(aNSContext).setPutNamespaceContextPrefixesInRoot(true);

      final OutputStream aOS = FileHelper.getOutputStream(xslFile);
      if (aOS == null) {
        throw new IllegalStateException("Failed to open output stream for file " + xslFile.getAbsolutePath());
      }
      XMLWriter.writeToStream(aXsltProvider.getXSLTDocument(), aOS, aXWS);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private File getXsltFile(File schFile, String phase) {
    String name = schFile.getName();
    if (name.contains(".")) {
      name = name.substring(0, name.indexOf("."));
    }
    if (phase != null) {
      name += "_" + phase;
    }
    name += ".xsl";
    return new File(schFile.getParentFile().getParentFile(), "/xsl/" + name);
  }

  private byte[] readFile(File file) throws Exception {
    int bufsSize = 1024;
    byte[] buffer = new byte[bufsSize];
    int len;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
      do {
        len = in.read(buffer);
        if (len != 0) {
          baos.write(buffer, 0, len);
        }
      } while (len == bufsSize);
    }
    return baos.toByteArray();
  }
}
