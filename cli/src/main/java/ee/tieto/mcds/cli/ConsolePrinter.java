/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli;

import org.springframework.stereotype.Component;

import java.io.PrintStream;
import java.util.Arrays;

@Component
public class ConsolePrinter implements Printer {

  private final PrintStream out = System.out;

  public void print(String message, Object... args) {
    if (args == null || args.length == 0) {
      this.print(message);
    } else {
      out.printf(message, args);
    }
  }

  public void print(String message) {
    out.print(message);
  }

  public void println() {
    out.println();
  }

  public void println(String message, Object... args) {
    if (args == null || args.length == 0) {
      this.println(message);
    } else {
      out.printf(message, args);
    }
  }

  public void println(String message) {
    out.println(message);
  }

  @Override
  public void print(Throwable e) {
    println(e.getMessage());
    println(Arrays.toString(e.getStackTrace()));
  }

}
