/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.validate;

import java.io.File;
import java.nio.file.Path;

public class Validator {

  private final File rulesFile;
  private final File xbrlFile;
  private final File outFile;

  public Validator(File rulesFile, File xbrlFile, File outFile) {
    this.rulesFile = rulesFile;
    this.xbrlFile = xbrlFile;
    this.outFile = outFile;
  }


  public void validate() {
//    String message = validateSchema();
//    Path rulesPath = Path.of(rulesFile);
//    rulesPath.getFileName().
  }
}
