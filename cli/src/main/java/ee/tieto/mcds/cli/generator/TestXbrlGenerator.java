/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.generator;

import ee.tieto.mcds.cli.ConsolePrinter;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TestXbrlGenerator {
  private static final ConsolePrinter out = new ConsolePrinter();

  private static final int MAX_ACCOUNTING_ENTRIES = 100;
  private static final int MAX_ENTRY_HEADERS = 100;
  private static final int MAX_ENTRY_DETAILS = 5;

  private static final String XML_PATH = "./naidisarve/naidis.xml";
  private static final String GENERATED_XML_PATH = "./naidisarve/generated.xml";

  public void generateXbrl(ApplicationArguments args) {


    try {
      Generator generator = new Generator(MAX_ACCOUNTING_ENTRIES, MAX_ENTRY_HEADERS, MAX_ENTRY_DETAILS);
      generator.generate(XML_PATH, GENERATED_XML_PATH);
    } catch (Exception e) {
      out.println(e.getMessage());
    }
    out.println("Done");
  }

}
