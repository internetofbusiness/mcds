/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.cli.generator;

import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Generator {

  private static final QName ACCOUNTING_ENTRIES_QNAME = DocumentFactory.getInstance().createQName(
      "accountingEntries", "gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
  private static final QName ENTRY_HEADER_QNAME = DocumentFactory.getInstance().createQName(
      "entryHeader", "gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
  private static final QName ENTRY_DETAIL_QNAME = DocumentFactory.getInstance().createQName(
      "entryDetail", "gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");

  private final DocumentFactory factory;
  private final int maxAcountingEntriesCount;
  private final int maxEntryHeadersCount;
  private final int maxEntryDetailsCount;

  public Generator(int maxAcountingEntriesCount, int maxEntryHeadersCount, int maxEntryDetailsCount) {
    this.factory = DocumentFactory.getInstance();
    this.maxAcountingEntriesCount = maxAcountingEntriesCount;
    this.maxEntryHeadersCount = maxEntryHeadersCount;
    this.maxEntryDetailsCount = maxEntryDetailsCount;
  }

  public void generate(String exampleName, String resultName) throws Exception {

    File exampleFile = new File(exampleName);
    if (!exampleFile.isFile() || !exampleFile.canRead()) {
      throw new Exception("File " + exampleFile.getAbsolutePath() + " not readeable");
    }

    SAXReader reader = new SAXReader();
    Document example = reader.read(exampleFile);
    Document result = createResult(example);
    List<Node> accountingEntries = createXPath("gl-cor:accountingEntries")
        .selectNodes(example.getRootElement());
    if (accountingEntries.isEmpty()) {
      write(result, resultName);
      return;
    }
    addAccountingEntries(result.getRootElement(), (Element) accountingEntries.get(0));
    write(result, resultName);
  }

  private void addAccountingEntries(Element result, Element accountingEntries) {
    for (int i=0; i<this.maxAcountingEntriesCount; i++) {
      Element ne = copyElement(accountingEntries);
      copyContent(accountingEntries, ne, ENTRY_HEADER_QNAME);
      result.add(ne);
      List<Node> entityHeaders = createXPath("gl-cor:entryHeader")
          .selectNodes(accountingEntries);
      if (entityHeaders.isEmpty()) {
        return;
      }
      addEntityHeaders(ne, (Element)entityHeaders.get(0));
    }
  }

  private void addEntityHeaders(Element accountingEntries, Element entityHeader) {
    for (int i=0; i<this.maxEntryHeadersCount; i++) {
      Element ne = copyElement(entityHeader);
      copyContent(entityHeader, ne, ENTRY_DETAIL_QNAME);
      accountingEntries.add(ne);
      List<Node> entryDetail = createXPath("gl-cor:entryDetail")
          .selectNodes(entityHeader);
      if (entryDetail.isEmpty()) {
        return;
      }
      addDetails(ne, (Element)entryDetail.get(0));
    }
  }

  private void addDetails(Element entityHeader, Element entryDetail) {
    for (int i=0; i<this.maxEntryDetailsCount; i++) {
      Element ne = (Element)entryDetail.clone();
      ne.detach();
      entityHeader.add(ne);
    }
  }


  private Document createResult(Document example) {
    Element elem = copyElement(example.getRootElement());
    copyContent(example.getRootElement(), elem, Generator.ACCOUNTING_ENTRIES_QNAME);
    return factory.createDocument(elem);
  }

  private void write(Document doc, String resultName) throws Exception {
    File f = new File(resultName);
    FileOutputStream fos = new FileOutputStream(f);
    OutputStreamWriter writer = new OutputStreamWriter(new BufferedOutputStream(fos), StandardCharsets.UTF_8);
    writer.write(doc.asXML());
    writer.close();
  }

  private Element copyElement(Element element) {
    Element ne = factory.createElement(element.getQName());
    for (Namespace ns : element.additionalNamespaces()) {
      ne.addNamespace(ns.getPrefix(), ns.getURI());
    }
    for (Attribute atr : element.attributes()) {
      ne.addAttribute(atr.getQName(), atr.getValue());
    }
    //copyContent(element, ne, exclude);
    return ne;
  }

  private boolean copyContent(Element oe, Element ne, QName exclude) {
    for (Element oelem : oe.elements()) {
      if (exclude != null && oelem.getQName().equals(exclude)) {
        continue;
      }
      Element newElem = copyElement(oelem);
      ne.add(newElem);
      if (oelem.isTextOnly()) {
        newElem.addText(oelem.getText());
      } else {
        if (copyContent(oelem, newElem, null)) {
          return true;
        }
      }
    }
    return false;
  }

  private XPath createXPath(String expression) {
    XPath xpath = DocumentHelper.createXPath(expression);
    Map<String, String> pathNamespaces = new HashMap<>();
    pathNamespaces.put("xbrli", "http://www.xbrl.org/2003/instance");
    pathNamespaces.put("gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
    pathNamespaces.put("gl-bus", "http://www.xbrl.org/int/gl/bus/2015-03-25");
    xpath.setNamespaceURIs(pathNamespaces);
    return xpath;
  }
}
