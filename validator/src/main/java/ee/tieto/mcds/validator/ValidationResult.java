/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    private String id;
    private boolean valid = true;
    private final List<MessageRow> messages = new ArrayList<>();

    public static ValidationResult newOK() {
        return new ValidationResult();
    }

    public void addMessage(Severity severity, String message) {
        this.valid = false;
        MessageRow row = new MessageRow(severity, message);
        this.messages.add(row);
    }

    public boolean isValid() {
        return this.valid;
    }

    public List<MessageRow> getMessages() {
        return messages;
    }

    public String getId() {
        return this.id;
    }

    public void setId(Number id) {
        if (id == null) {
            this.id = "null";
        }
        this.id = id.toString();
    }

    public String toString() {
        if (this.messages.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < messages.size(); i++) {
            sb.append("{").append(messages.get(i));
            if (i < messages.size()-1) {
                sb.append(",");
            }
            sb.append("}");
        }
        sb.append("]");
        return sb.toString();
    }


    public enum Severity {
        FATAL_ERROR("FatalError"),
        ERROR("Error"),
        WARNING("Warning");

        private final String severityText;

        Severity(String severityText) {
            this.severityText = severityText;
        }

        public String getText() {
            return this.severityText;
        }
    }

    public static class MessageRow {
        private Severity severity;
        private String message;

        public MessageRow(Severity severity, String message) {
            this.severity = severity;
            this.message = message;
        }

        public Severity getSeverity() {
            return severity;
        }

        public void setSeverity(Severity severity) {
            this.severity = severity;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String toString() {
            return this.severity.getText() + ": " + this.message;
        }

        @Override
        public MessageRow clone() {
            MessageRow row = new MessageRow(this.severity, this.message);
            return row;
        }
    }

    @Override
    public ValidationResult clone() {
        ValidationResult result = new ValidationResult();
        result.valid = this.valid;
        this.messages.forEach(r -> result.messages.add(r.clone()));
        return result;
    }
}
