/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator.schema;

import ee.tieto.mcds.validator.ValidationErrorHandler;
import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.ValidationResult.Severity;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class XsdValidationErrorHandlerImpl extends DefaultHandler implements ValidationErrorHandler {

    private final ValidationResult result = new ValidationResult();

    @Override
    public void error(SAXParseException e) {
        result.addMessage(Severity.ERROR, "Line:" + e.getLineNumber() + " Column:" + e.getColumnNumber() + " " + e.getMessage());
    }

    @Override
    public void fatalError(SAXParseException e) {
        result.addMessage(Severity.FATAL_ERROR, "Line:" + e.getLineNumber() + " Column:" + e.getColumnNumber() + " " + e.getMessage());
    }

    @Override
    public void warning(SAXParseException e) {
        result.addMessage(Severity.WARNING, "Line:" + e.getLineNumber() + " Column:" + e.getColumnNumber() + " " + e.getMessage());
    }

    @Override
    public ValidationResult getValidationResult() {
        return this.result;
    }
}
