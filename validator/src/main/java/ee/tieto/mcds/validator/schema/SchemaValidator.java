/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator.schema;

import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.Validator;
import org.xml.sax.SAXParseException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public final class SchemaValidator implements Validator {
  private static final Logger log = Logger.getLogger(SchemaValidator.class.getName());

  private final Schema schema;

  public SchemaValidator(Schema schema) {
    this.schema = schema;
  }

  public ValidationResult validate(InputStream xml) throws McdsSystemException {
    XsdValidationErrorHandlerImpl errorHandler = new XsdValidationErrorHandlerImpl();
    try {
      javax.xml.validation.Validator validator = schema.newValidator();
      validator.setErrorHandler(errorHandler);
      Source xmlSource = new StreamSource(xml);
      validator.validate(xmlSource);
      return errorHandler.getValidationResult();
    } catch (SAXParseException e) {
      return errorHandler.getValidationResult();
    } catch (Exception e) {
      throw new McdsSystemException(e.getMessage(), e);
    }
  }

  @Override
  public ValidationResult validate(String xbrl) throws McdsSystemException {
    try {
      ByteArrayInputStream bain = new ByteArrayInputStream(xbrl.getBytes(StandardCharsets.UTF_8));
      return validate(bain);
    } catch (Exception e) {
      throw new McdsSystemException(e.getMessage(), e);
    }
  }
}
