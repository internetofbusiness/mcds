/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator;

import ee.tieto.mcds.validator.ValidatorConfig.Version;
import ee.tieto.mcds.validator.BusinessValidator.ValidationContext;
import ee.tieto.mcds.validator.schema.SchemaValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Component
public class XbrlValidatorFactory {
  private static final Logger log = LoggerFactory.getLogger(XbrlValidatorFactory.class);

  private static final String XSL_RULES_DIR = "schematron/xsl";

  private final ValidatorConfig conf;

  private final Map<String, Validator> schemaValidators = new HashMap<>();
  private final Map<String, Map<ValidationContext, List<BusinessValidator>>> scematronValidators = new HashMap<>();

  public XbrlValidatorFactory(ValidatorConfig conf) {
    this.conf = conf;
  }

  public Validator getSchemaValidator(String xbrlGlVarsion) {
    return this.schemaValidators.get(xbrlGlVarsion);
  }

  public List<BusinessValidator> getBusinessValidators(String version, ValidationContext validationContext) {
    Map<ValidationContext, List<BusinessValidator>> contexts = this.scematronValidators.get(version);
    if (contexts == null || contexts.isEmpty()) {
      return null;
    }
    return contexts.get(validationContext);
  }

  //@PostConstruct
  public void init() {
    try {
      initSchemaValidators();
      initScematronValidators();
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
  }

  private void initSchemaValidators() {
    conf.getVersions().forEach(version -> {
      Validator validator = createSchemaValidator(version.getName());
      this.schemaValidators.put(version.getName(), validator);
    });
  }

  private Validator createSchemaValidator(String version) {
    SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    factory.setResourceResolver(new ResourceResolver());
    Schema schema;
    try {

      schema = factory.newSchema(findSourceFile(version));
    } catch (SAXException e) {
      throw new RuntimeException(e);
    }
    return new SchemaValidator(schema);
  }

  private File findSourceFile(String version) {
    File baseDir = new File(conf.getRulesdirectory() + "/" + version);
    return new File(baseDir, conf.getVersion(version).getXsd());
  }

  private void initScematronValidators() {
    conf.getVersions().forEach(this::schematronByVersions);
  }

  private void schematronByVersions(Version version) {
    Map<ValidationContext, List<BusinessValidator>> versionContexts =
        this.scematronValidators.computeIfAbsent(version.getName(), k -> new HashMap<>());
    List<String> files = version.getContexts().getGlobal().getFiles();
    versionContexts.put(
        ValidationContext.XBRL, schematronByContext(version.getName(), files));
    files = version.getContexts().getEntries().getFiles();
    versionContexts.put(
        ValidationContext.ENTRIES, schematronByContext(version.getName(), files));
    files = version.getContexts().getEntry().getFiles();
    versionContexts.put(
        ValidationContext.ENTRY, schematronByContext(version.getName(), files));
  }

  private List<BusinessValidator> schematronByContext(
      String versionName,
      List<String> files) {
    List<BusinessValidator> validators = new ArrayList<>(files.size());
    if (files.size() > 0) {
      for (String n : files) {
        Path path = Paths.get(conf.getRulesdirectory(), versionName, XSL_RULES_DIR, n);
        Templates template = createXslTemplate(path);
        BusinessValidator validator = BusinessValidator.newInstance(template);
        validators.add(validator);
      }
    }
    return validators;
  }

  private Templates createXslTemplate(Path file) {
    try {
      TransformerFactory factory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);
      log.info("Loading xml schema: {}", file);
      Source source = new StreamSource(file.toString());
      return factory.newTemplates(source);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private static final class ResourceResolver implements LSResourceResolver {

    @Override
    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
      log.trace("type:{} namespaceURI:{} publicId:{} systemId:{} baseURI:{}",
          type, namespaceURI, publicId, systemId, baseURI);

      Path basePath = Path.of(URI.create(baseURI)).getParent();
      Path systemPath = Path.of(systemId);
      systemPath = basePath.resolve(systemPath).normalize();
      log.info("Loading xml schema: {}", systemPath);
      return new LSInputImpl(publicId, systemPath.toUri().toASCIIString(), systemPath.toUri().toASCIIString());
    }
  }

  private static final class LSInputImpl implements LSInput {
    private Reader characterStream;
    private InputStream byteStream;
    private String stringData;
    private String systemId;
    private String publicId;
    private String baseURI;
    private String encoding;

    private LSInputImpl(String publicId, String systemId,
                        String baseURI ) {
      this.systemId = systemId;
      this.publicId = publicId;
      this.baseURI = baseURI;
    }

    @Override
    public Reader getCharacterStream() {
      return characterStream;
    }

    @Override
    public void setCharacterStream(Reader characterStream) {
      this.characterStream = characterStream;
    }

    @Override
    public InputStream getByteStream() {
      return byteStream;
    }

    @Override
    public void setByteStream(InputStream byteStream) {
      this.byteStream = byteStream;
    }

    @Override
    public String getStringData() {
      return stringData;
    }

    @Override
    public void setStringData(String stringData) {
      this.stringData = stringData;
    }

    @Override
    public String getSystemId() {
      return systemId;
    }

    @Override
    public void setSystemId(String systemId) {
      this.systemId = systemId;
    }

    @Override
    public String getPublicId() {
      return publicId;
    }

    @Override
    public void setPublicId(String publicId) {
      this.publicId = publicId;
    }

    @Override
    public String getBaseURI() {
      return baseURI;
    }

    @Override
    public void setBaseURI(String baseURI) {
      this.baseURI = baseURI;
    }

    @Override
    public String getEncoding() {
      return encoding;
    }

    @Override
    public void setEncoding(String encoding) {
      this.encoding = encoding;
    }

    @Override
    public boolean getCertifiedText() {
      return false;
    }

    @Override
    public void setCertifiedText(boolean certifiedText) {
    }
  }


}
