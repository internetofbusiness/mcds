/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@ConfigurationProperties("mcds.validation")
public class ValidatorConfig {
  public enum ContextType {GLOBAL, ENTRIES, ENTRY}

  private String rulesdirectory;
  private List<Version> versions;

  public String getRulesdirectory() {
    return rulesdirectory;
  }

  public void setRulesdirectory(String rulesdirectory) {
    this.rulesdirectory = rulesdirectory;
  }

  public List<Version> getVersions() {
    return versions;
  }

  public Version getVersion(String versionName) {
    if (versionName == null) {
      throw new NullPointerException("versionName required");
    }
    Version ret = null;
    for (Version ver : this.versions) {
      if (versionName.equals(ver.getName())){
        ret = ver;
        break;
      }
    }
    if (ret == null) {
      throw new IllegalStateException("Can not find validator version by name " + versionName);
    }
    return ret;
  }

  public void setVersions(List<Version> versions) {
    this.versions = versions;
  }

  public String getregNrXpath(String version, ContextType contextType) {
    return getContext(version, contextType).orElse(null).getOrganizationRegnrXpath();
  }

  public String getEntryTypeXpath(String version, ContextType contextType) {
    return getContext(version, contextType).orElse(null).getEntryTypeXpath();
  }

  private Optional<Context> getContext(String version, ContextType contextType) {
    if (version == null || contextType == null) {
      throw new IllegalArgumentException("version and context required");
    }
    Optional<Version> ver = this.versions.stream().filter(v-> v.getName().equals(version)).findFirst();
    if (ver.isEmpty()) {
      return Optional.empty();
    }
    Contexts cons = ver.get().getContexts();
    if (cons == null) {
      return Optional.empty();
    }
    Context con = null;
    switch (contextType) {
      case GLOBAL:
        con = cons.getGlobal();
        break;
      case ENTRIES:
        con = cons.getEntries();
        break;
      case ENTRY:
        con = cons.getEntry();
        break;
    }
    if (con == null) {
      return Optional.empty();
    }
    return Optional.of(con);
  }

  public static class Version {
    private String name;
    private String xsd;
    private Contexts contexts;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getXsd() {
      return xsd;
    }

    public void setXsd(String xsd) {
      this.xsd = xsd;
    }

    public Contexts getContexts() {
      return contexts;
    }

    public void setContexts(Contexts contects) {
      this.contexts = contects;
    }

  }

  public static class Contexts {
    private Context global;
    private Context entries;
    private Context entry;

    public Context getGlobal() {
      return global;
    }

    public void setGlobal(Context global) {
      this.global = global;
    }

    public Context getEntries() {
      return entries;
    }

    public void setEntries(Context entries) {
      this.entries = entries;
    }

    public Context getEntry() {
      return entry;
    }

    public void setEntry(Context entry) {
      this.entry = entry;
    }
  }

  public static class Context {
    private String organizationRegnrXpath;

    public String getEntryTypeXpath() {
      return entryTypeXpath;
    }

    public void setEntryTypeXpath(String entryTypeXpath) {
      this.entryTypeXpath = entryTypeXpath;
    }

    private String entryTypeXpath;
    private List<String> files;

    public String getOrganizationRegnrXpath() {
      return organizationRegnrXpath;
    }

    public void setOrganizationRegnrXpath(String organizationRegnrXpath) {
      this.organizationRegnrXpath = organizationRegnrXpath;
    }

    public List<String> getFiles() {
      return files;
    }

    public void setFiles(List<String> files) {
      this.files = files;
    }
  }
}
