/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator;

import ee.tieto.mcds.validator.schematron.SchematronValidator;
import org.dom4j.Document;
import org.dom4j.Node;

import javax.xml.transform.Templates;
import java.io.InputStream;

public interface BusinessValidator {

    enum ValidationContext {
        XBRL, ENTRIES, ENTRY
    }

    static BusinessValidator newInstance(Templates templates) {
        return new SchematronValidator(templates);
    }

    ValidationResult validate(final Node node);
    ValidationResult validate(final Node node, final ValidationResult validationResult);
    Document validate(final InputStream xbrl);
}
