/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.validator.schematron;

import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.ValidationResult.Severity;
import ee.tieto.mcds.validator.BusinessValidator;
import org.dom4j.*;
import org.dom4j.io.DocumentResult;
import org.dom4j.io.DocumentSource;
import org.dom4j.io.SAXReader;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SchematronValidator implements BusinessValidator {

  private final Templates xslTemplate;

  public SchematronValidator(Templates xslTemplate) {
    this.xslTemplate = xslTemplate;
  }

  @Override
  public ValidationResult validate(Node node) {
    return validate(node, null);
  }

  @Override
  public Document validate(InputStream xbrlStream) {
    SAXReader reader = new SAXReader();
    try {
      Document xbrl = reader.read(xbrlStream);
      Document result = validateInternal(xbrl.getRootElement());
      return result;
    } catch (DocumentException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public ValidationResult validate(Node node, final ValidationResult validationResult) {
    try {
      Document resultDoc = validateInternal(node);
      ValidationResult vr = validationResult;
      if (vr == null) {
        vr = new ValidationResult();
      }
      return toValidationResult(resultDoc, vr);
    } catch (Exception e) {
      //TODO MCDS exception
      throw new RuntimeException(e);
    }
  }

  private Document validateInternal(Node node) {
    try {
      Transformer transformer = xslTemplate.newTransformer();
      DocumentSource doc = new DocumentSource(node);
      DocumentResult result = new DocumentResult();
      transformer.transform(doc, result);
      return result.getDocument();
    } catch (Exception e) {
      //TODO MCDS exception
      throw new RuntimeException(e);
    }
  }

  private ValidationResult toValidationResult(Document doc, ValidationResult result) {
    XPath failed = DocumentHelper.createXPath("svrl:schematron-output/svrl:failed-assert");
    Map<String, String> pathNamespaces = new HashMap<>();
    pathNamespaces.put("svrl", "http://purl.oclc.org/dsdl/svrl");
    failed.setNamespaceURIs(pathNamespaces);
    List<Node> nodes = failed.selectNodes(doc);

    if (nodes.isEmpty()) {
      return result;
    }

    for (Node node : nodes) {
      Element elem = (Element)node;
      StringBuilder sb = new StringBuilder(elem.attribute("id").getValue());
      sb.append(" - ");
      Node textNode = node.selectSingleNode("svrl:text");
      sb.append(textNode == null ? elem.attribute("test").getValue() : textNode.getText());
      sb.append("  Location - ").append(elem.attribute("location").getValue());
      result.addMessage(Severity.ERROR, sb.toString());
    }

    return result;
  }
}
