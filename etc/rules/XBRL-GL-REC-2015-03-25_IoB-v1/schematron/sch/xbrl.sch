<!--
  ~ My Company Data System (MCDS)
  ~ Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            defaultPhase="validator">
    <sch:ns prefix="xbrli" uri="http://www.xbrl.org/2003/instance"/>
    <sch:ns prefix="xbrll" uri="http://www.xbrl.org/2003/linkbase"/>
    <sch:ns prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
    <sch:ns prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
    <sch:ns prefix="gl-cor" uri="http://www.xbrl.org/int/gl/cor/2015-03-25"/>
    <sch:ns prefix="gl-muc" uri="http://www.xbrl.org/int/gl/muc/2015-03-25"/>
    <sch:ns prefix="gl-bus" uri="http://www.xbrl.org/int/gl/bus/2015-03-25"/>
    <sch:ns prefix="gl-plt" uri="http://www.xbrl.org/int/gl/plt/2015-03-25"/>
    <sch:ns prefix="iso4217" uri="http://www.xbrl.org/2003/iso4217"/>
    <sch:ns prefix="iso639" uri="http://www.xbrl.org/2005/iso639"/>

    <sch:phase id="validator">
        <sch:active pattern="entryRules">completed</sch:active>
    </sch:phase>


    <sch:pattern name="entryRules" id="entryRules">

        <!-- accountingEntries rules -->
        <sch:rule context="/xbrli:xbrl/gl-cor:accountingEntries">
            <!-- R-01.004 -->
            <sch:assert
                    id="R-01.004"
                    test="gl-cor:documentInfo/gl-muc:defaultCurrency[(starts-with(text(), 'iso4217:'))]"
            >The value of gl-muc:defaultCurrency must start with the "iso4217:"</sch:assert>
            <!-- R-01.006 -->
            <!--
            <sch:assert
                    id="R-01.006"
                    test="count(gl-cor:entityInformation/gl-bus:organizationIdentifiers/gl-bus:organizationIdentifier) = 1"
            >Organisatsioonil peab olema täpselt üks äriregistri kood</sch:assert>
            -->
            <sch:assert
                    id="R-01.006"
                    test="count(gl-cor:entityInformation/gl-bus:organizationIdentifiers[./gl-bus:organizationDescription/text()='business_reg_number']/gl-bus:organizationIdentifier) = 1
                        and normalize-space(gl-cor:entityInformation/gl-bus:organizationIdentifiers[./gl-bus:organizationDescription/text()='business_reg_number']/gl-bus:organizationIdentifier)!=''"
            >The organization must have exactly one register code, where gl-bus:organizationDescription=business_reg_number</sch:assert>
        </sch:rule>

        <!-- R-0000-1 -->
        <sch:rule context="/xbrli:xbrl/gl-cor:accountingEntries/gl-cor:entryHeader/gl-cor:entryDetail">
            <sch:assert
                    id="R-0000-1"
                    test="normalize-space(child::gl-cor:documentNumber)!='' or not(boolean(child::gl-cor:documentType)) or child::gl-cor:documentType != 'invoice'"
            >If gl-cor:documentType is 'invoice', then gl-cor:documentNumber required</sch:assert>

            <!-- 0000-2 -->
            <sch:assert
                    id="R-0000-2"
                    test="normalize-space(child::gl-cor:documentDate)!='' or not(boolean(child::gl-cor:documentType)) or child::gl-cor:documentType != 'invoice'"
            >If gl-cor:documentType is 'invoice', then gl-cor:documentDate required</sch:assert>

            <!-- gl-cor:postingDate required -->
            <sch:assert
                    id="gl-cor:postingDate_required"
                    test="boolean(child::gl-cor:postingDate)"
            >entryDetail postingDate required</sch:assert>
        </sch:rule>

    </sch:pattern>

</sch:schema>
