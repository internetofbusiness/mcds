/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds;

public class McdsSystemException extends McdsException{
  private static final String DEFAULT_MESSAGE = "Unexpected system error";
  private static final int DEFAULT_CODE = 500;

  public McdsSystemException(String message) {
    super(message, null, ExceptionType.SYSTEM_UNKNOWN, DEFAULT_CODE);
  }

  public McdsSystemException(String message, Throwable e) {
    super(
        message == null ? DEFAULT_MESSAGE : message,
        e, ExceptionType.SYSTEM_UNKNOWN, DEFAULT_CODE);
  }

  public McdsSystemException(Throwable e) {
    super(DEFAULT_MESSAGE, e, ExceptionType.SYSTEM_UNKNOWN, DEFAULT_CODE);
  }
}
