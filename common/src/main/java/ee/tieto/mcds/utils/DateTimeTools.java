/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.utils;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.time.temporal.WeekFields;

public class DateTimeTools {

  public static LocalDate startOfYear(LocalDate current, int adjuster) {
    int year = current.getYear() + adjuster;
    return LocalDate.of(year, 1, 1);
  }

  public static LocalDate endOfYear(LocalDate current, int adjuster) {
    int year = current.getYear() + adjuster;
    return LocalDate.of(year, 12, 31);
  }

  public static LocalDate startOfHalfYear(LocalDate current, int adjuster) {
    int year = current.getYear();
    int month = current.getMonth().getValue();
    month = month < 7 ? 1 : 7;
    LocalDate ret = LocalDate.of(year, month, 1);
    ret = ret.plus(adjuster * 6, ChronoUnit.MONTHS);
    return ret;
  }

  public static LocalDate endOfHalfYear(LocalDate current, int adjuster) {
    int year = current.getYear();
    int month = current.getMonth().getValue();
    month = month < 7 ? 6 : 12;
    LocalDate ret = LocalDate.of(year, month, 1);
    ret = ret.plus(adjuster * 6, ChronoUnit.MONTHS);
    ret = ret.withDayOfMonth(ret.lengthOfMonth());
    return ret;
  }

  public static LocalDate startOfQuarter(LocalDate current, int adjuster) {
    int year = current.getYear() ;
    int quartter = current.get(IsoFields.QUARTER_OF_YEAR);
    LocalDate ret  = YearMonth.of(year, 1).with(IsoFields.QUARTER_OF_YEAR, quartter).atDay(1);
    ret = ret.plusMonths(adjuster * 3);
    return ret;
  }

  public static LocalDate endOfQuarter(LocalDate current, int adjuster) {
    int year = current.getYear();
    int quartter = current.get(IsoFields.QUARTER_OF_YEAR);
    LocalDate ret  = YearMonth.of(year, 1).with(IsoFields.QUARTER_OF_YEAR, quartter)
        .plusMonths(2).atDay(1);
    ret = ret.plusMonths(adjuster * 3);
    ret = ret.withDayOfMonth(ret.lengthOfMonth());
    return ret;
  }

  public static LocalDate startOfMonth(LocalDate current, int adjuster) {
    int year = current.getYear();
    int month = current.getMonth().getValue();
    LocalDate ret = LocalDate.of(year, month, 1);
    ret = ret.plusMonths(adjuster);
    return ret;
  }

  public static LocalDate endOfMonth(LocalDate current, int adjuster) {
    int year = current.getYear();
    int month = current.getMonth().getValue();
    LocalDate ret = LocalDate.of(year, month, 1);
    ret = ret.withDayOfMonth(ret.lengthOfMonth());
    ret = ret.plusMonths(adjuster);
    return ret;
  }

  public static LocalDate startOfWeek(LocalDate current, int adjuster) {
    LocalDate ret = current.with(WeekFields.ISO.dayOfWeek(), 1L);
    ret = ret.plus(adjuster, ChronoUnit.WEEKS);
    return ret;
  }

  public static LocalDate endOfWeek(LocalDate current, int adjuster) {
    LocalDate ret = current.with(WeekFields.ISO.dayOfWeek(), 7L);
    ret = ret.plus(adjuster, ChronoUnit.WEEKS);
    return ret;
  }

  public static LocalDate startOfDay(LocalDate current, int adjuster) {
    LocalDate ret = current.plusDays(adjuster);
    return ret;
  }

  public static LocalDate endOfDay(LocalDate current, int adjuster) {
    LocalDate ret = current.plusDays(adjuster);
    return ret;
  }

}
