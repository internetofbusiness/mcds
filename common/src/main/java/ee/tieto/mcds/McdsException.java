/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds;

public class McdsException extends RuntimeException {
  public enum ExceptionType {
    BUSINESS_VALIDATION(409),
    FORBIDDEN(403),
    NOT_FOUND(404),
    SYSTEM_UNKNOWN(500),
    EXTERNAL_SYSTEM(500);

    private final int httpCode;

    ExceptionType(int httpCode) {
      this.httpCode = httpCode;
    }
    public int getHttpCode() {
      return this.httpCode;
    }
  }

  protected ExceptionType exceptionType;
  protected int code;

  public McdsException(String message, Throwable e, ExceptionType exceptionType, int code) {
    super(message, e);
    this.exceptionType = exceptionType;
    this.code = code;
  }

  public int getHttpStatus() {
    return this.exceptionType.getHttpCode();
  }

  public int getCode() {
    return this.code;
  }

}
