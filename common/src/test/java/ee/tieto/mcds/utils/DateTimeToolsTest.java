/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class DateTimeToolsTest {

  private final LocalDate CURRENT = LocalDate.of(2020, 10, 24);

  @Test
  void startOfYear() {
    LocalDate date = DateTimeTools.startOfYear(CURRENT, 0);
    String message = assertDate(date, 2020, 1, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfYear(CURRENT, 2);
    message = assertDate(date, 2022, 1, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfYear(CURRENT, -2);
    message = assertDate(date, 2018, 1, 1);
    Assertions.assertNull(message, message);
  }

  @Test
  void endOfYear() {
    LocalDate date = DateTimeTools.endOfYear(CURRENT, 0);
    String message = assertDate(date, 2020, 12, 31);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfYear(CURRENT, 2);
    message = assertDate(date, 2022, 12, 31);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfYear(CURRENT, -2);
    message = assertDate(date, 2018, 12, 31);
    Assertions.assertNull(message, message);
  }

  @Test
  void startOfHalfYear() {
    LocalDate date = DateTimeTools.startOfHalfYear(CURRENT, 0);
    String message = assertDate(date, 2020, 7, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfHalfYear(CURRENT, 2);
    message = assertDate(date, 2021, 7, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfHalfYear(CURRENT, -3);
    message = assertDate(date, 2019, 1, 1);
    Assertions.assertNull(message, message);
  }

  @Test
  void endOfHalfYear() {
    LocalDate date = DateTimeTools.endOfHalfYear(CURRENT, 0);
    String message = assertDate(date, 2020, 12, 31);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfHalfYear(CURRENT, 2);
    message = assertDate(date, 2021, 12, 31);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfHalfYear(CURRENT, -3);
    message = assertDate(date, 2019, 6, 30);
    Assertions.assertNull(message, message);
  }

  @Test
  void startOfQuarter() {
    LocalDate date = DateTimeTools.startOfQuarter(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfQuarter(CURRENT, 2);
    message = assertDate(date, 2021, 4, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfQuarter(CURRENT, -6);
    message = assertDate(date, 2019, 4, 1);
    Assertions.assertNull(message, message);
  }

  @Test
  void endOfQuarter() {
    LocalDate date = DateTimeTools.endOfQuarter(CURRENT, 0);
    String message = assertDate(date, 2020, 12, 31);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfQuarter(CURRENT, 2);
    message = assertDate(date, 2021, 6, 30);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfQuarter(CURRENT, -6);
    message = assertDate(date, 2019, 6, 30);
    Assertions.assertNull(message, message);
  }

  @Test
  void startOfMonth() {
    LocalDate date = DateTimeTools.startOfMonth(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfMonth(CURRENT, 4);
    message = assertDate(date, 2021, 2, 1);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfMonth(CURRENT, -14);
    message = assertDate(date, 2019, 8, 1);
    Assertions.assertNull(message, message);
  }

  @Test
  void endOfMonth() {
    LocalDate date = DateTimeTools.endOfMonth(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 31);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfMonth(CURRENT, 4);
    message = assertDate(date, 2021, 2, 28);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfMonth(CURRENT, -14);
    message = assertDate(date, 2019, 8, 31);
    Assertions.assertNull(message, message);
  }

  @Test
  void startOfWeek() {
    LocalDate date = DateTimeTools.startOfWeek(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 19);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfWeek(CURRENT, 4);
    message = assertDate(date, 2020, 11, 16);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfWeek(CURRENT, -5);
    message = assertDate(date, 2020, 9, 14);
    Assertions.assertNull(message, message);
  }

  @Test
  void endOfWeek() {
    LocalDate date = DateTimeTools.endOfWeek(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 25);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfWeek(CURRENT, 4);
    message = assertDate(date, 2020, 11, 22);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfWeek(CURRENT, -5);
    message = assertDate(date, 2020, 9, 20);
    Assertions.assertNull(message, message);
  }

  @Test
  void startOfDay() {
    LocalDate date = DateTimeTools.startOfDay(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 24);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfDay(CURRENT, 10);
    message = assertDate(date, 2020, 11, 3);
    Assertions.assertNull(message, message);

    date = DateTimeTools.startOfDay(CURRENT, -25);
    message = assertDate(date, 2020, 9, 29);
    Assertions.assertNull(message, message);
  }

  @Test
  void endOfDay() {
    LocalDate date = DateTimeTools.endOfDay(CURRENT, 0);
    String message = assertDate(date, 2020, 10, 24);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfDay(CURRENT, 10);
    message = assertDate(date, 2020, 11, 3);
    Assertions.assertNull(message, message);

    date = DateTimeTools.endOfDay(CURRENT, -25);
    message = assertDate(date, 2020, 9, 29);
    Assertions.assertNull(message, message);
  }

  private String assertDate (LocalDate test, int year, int month, int day) {
    int y = test.getYear();
    int m = test.getMonth().getValue();
    int d = test.getDayOfMonth();

    if (y != year) {
      return "Year " + y + " expected " + year;
    }

    if (m != month) {
      return "Month " + m + " expected " + month;
    }

    if (d != day) {
      return "Day " + d + " expected " + day;
    }

    return null;
  }
}