# MCDS

## Installation

### Prerequisites
  - Java JDK 11 installed
  - PostgreSQL 12 server installed

### Build application
Clone MCDS project and build by Maven:

`mvn clean install`

### Setting up database
Scripts to initialize the database are located in the project directory `postgres/init`
  - **010_schema_and_app_user.sql** to initialize the schema and application user
    
    **NB!** The password must be changed in the script
  - **020_schema_objects.sql** to cteate schema objects
  - **030_system_classifiers.sql** to create MCDS system classifiers
  - **040_mcds manager.sql** to create ERP with manager rights

### MCDS application setup
1. Create mcds home directory
2. Copy `rest/target/rest-1.0.0-exec.jar` file to MCDS home directory
3. Copy all files and directoryes from `etc/application` to MCDS home directory
4. Edit commented `application.yaml`, at least set right database properties

### MCDS start and stop
To start, run at the command line

`java -Xms2G -Xmx4G -jar rest-1.0.0-exec.jar`

To stop gerfully, send the empty body POST request to the management REST endpoint /manage/shutdown

`curl -X POST -H "client_erp_code: MANAGER" http://localhost:8081/manage/shutdown`

## MCDS clients and authentication
An X.509 client certificate must be used for authentication.
The certificate TSL handshake can be take place by the load balancer or by the MCDS application.
In the case of a load balancer, the load balancer must pass the client certificate in the HTTP header
(the header name is configurable).

Each MCDS application client (ERP) must have its own element of the ERP classifier in the database.
In the client X.590 certificate, the code of the classifier element must be correspond to the value 
of the Subject(CN) field of the certificate.




