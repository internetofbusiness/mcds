/*
    MCDS MANAGER
*/    
INSERT INTO mcds.classifier_item (id_class, id_clve, code, text, valid_from) VALUES
((select id from mcds.classifier where code = 'ERP'),(select cv.id from mcds.classifier c join mcds.classifier_version cv on cv.id_clas = c.id where c.code='ERP' and cv.code = 'ERP_V1'),'MANAGER','Mcds manager',current_date - 1);
COMMIT;
