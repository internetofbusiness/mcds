/* ==============
    FUNCTIONS
   ============== */
CREATE OR REPLACE FUNCTION mcds.fill_created_column()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
begin
    NEW.created = now();
    NEW.modified = now();
    RETURN NEW; 
END;
$function$
;

CREATE OR REPLACE FUNCTION mcds.update_modified_column()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
begin
	NEW.created = old.created;
    NEW.modified = now();
    RETURN NEW; 
END;
$function$
;

/* ==============
    TABLES
   ============== */
-- Table accounting_entry
CREATE TABLE "mcds"."accounting_entry" (
    "id"                    SERIAL                      NOT NULL,
    "entry_uuid"            TEXT                        NOT NULL,
    "id_pack"               INTEGER                     NOT NULL,
    "id_orga"               INTEGER                     NOT NULL,
    "entry_type"            INTEGER                     NOT NULL,
    "entry"                 TEXT,
    "valid"                 BOOLEAN                     NOT NULL,
    "rules_name"            TEXT,
    "source_journal_id"     TEXT,
    "target_application"    TEXT,
    "business_area"         VARCHAR,
    "created"               TIMESTAMP WITH TIME ZONE    NOT NULL,
    "modified"              TIMESTAMP WITH TIME ZONE    NOT NULL,
    CONSTRAINT "pk_accontingentry" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."accounting_entry" IS 'Accounting entries';
COMMENT ON COLUMN "mcds"."accounting_entry"."id" IS 'Unique id';
COMMENT ON COLUMN "mcds"."accounting_entry"."entry_uuid" IS 'Globally unique id of accounting entry';
COMMENT ON COLUMN "mcds"."accounting_entry"."id_pack" IS 'The refference to the package, that the entry arrived with';
COMMENT ON COLUMN "mcds"."accounting_entry"."id_orga" IS 'The refference to the entry owner';
COMMENT ON COLUMN "mcds"."accounting_entry"."entry_type" IS 'Type of entry (gl-cor_entriesType)';
COMMENT ON COLUMN "mcds"."accounting_entry"."entry" IS 'The content of the entry as XBRL GL';
COMMENT ON COLUMN "mcds"."accounting_entry"."valid" IS 'Is valid by taxonomy and other IoB business rules';
COMMENT ON COLUMN "mcds"."accounting_entry"."rules_name" IS 'The version of the taxonomy and rules that has been validated';
COMMENT ON COLUMN "mcds"."accounting_entry"."source_journal_id" IS 'The sourceJournalId from xbrl';
COMMENT ON COLUMN "mcds"."accounting_entry"."target_application" IS 'The targetApplication from xbrl';
COMMENT ON COLUMN "mcds"."accounting_entry"."business_area" IS 'Value from xbrl gl-cor:documentInfo/gl-bus:sourceApplication';
COMMENT ON COLUMN "mcds"."accounting_entry"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."accounting_entry"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "accounting_entry_created_idx" ON "mcds"."accounting_entry"("created");
CREATE INDEX "ix_accounting_entry_target_application" ON "mcds"."accounting_entry"("target_application");
CREATE INDEX "ixfk_accontingentry_organization" ON "mcds"."accounting_entry"("id_orga");
CREATE INDEX "ixfk_accontingentry_package" ON "mcds"."accounting_entry"("id_pack");
CREATE INDEX "ixfk_accounting_entry_classifier_item" ON "mcds"."accounting_entry"("entry_type");
-- Table Triggers
create trigger update_accounting_entry_modtime 
before update on mcds.accounting_entry 
for each row execute function mcds.update_modified_column();

create trigger insert_accounting_entry_cretime 
before insert on mcds.accounting_entry 
for each row execute function mcds.fill_created_column();


-- Table accounting_entry_detail
CREATE TABLE "mcds"."accounting_entry_detail" (
    "id"            BIGSERIAL                   NOT NULL,
    "id_acen"       INTEGER                     NOT NULL,
    "line_number"   TEXT,
    "posting_date"  date,
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL,
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL,
    
    CONSTRAINT "accounting_entry_detail_pkey" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."accounting_entry_detail" IS 'Aditional in formation of accounting entry, collected from gl-cor:entryDetail';
COMMENT ON COLUMN "mcds"."accounting_entry_detail"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."accounting_entry_detail"."id_acen" IS 'Foreign refference to accounting_entr';
COMMENT ON COLUMN "mcds"."accounting_entry_detail"."line_number" IS 'gl-cor:entryDetail/gl-cor:lineNumber value';
COMMENT ON COLUMN "mcds"."accounting_entry_detail"."posting_date" IS 'gl-cor:entryDetail/gl-cor:postingDate value';
COMMENT ON COLUMN "mcds"."accounting_entry_detail"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."accounting_entry_detail"."modified" IS 'Time of DB record last modified';
-- Table Triggers
create trigger insert_accounting_entry_detail 
before insert on mcds.accounting_entry_detail 
for each row execute function mcds.fill_created_column();

create trigger update_accounting_entry_detail 
before update on mcds.accounting_entry_detail 
for each row execute function mcds.update_modified_column();


-- Table classifier
CREATE TABLE "mcds"."classifier" (
    "id"            SERIAL                      NOT NULL,
    "code"          TEXT                        NOT NULL, 
    "name"          TEXT                        NOT NULL, 
    "description"   TEXT, 
    "id_orga"       VARCHAR, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_classifier" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."classifier" IS 'Classifiers';
COMMENT ON COLUMN "mcds"."classifier"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."classifier"."code" IS 'Classifier code';
COMMENT ON COLUMN "mcds"."classifier"."name" IS 'Meaningful human-readable name of the classifier';
COMMENT ON COLUMN "mcds"."classifier"."description" IS 'Description';
COMMENT ON COLUMN "mcds"."classifier"."id_orga" IS 'Registration number of the organization administering the classification';
COMMENT ON COLUMN "mcds"."classifier"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."classifier"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_classifier_organization" ON "mcds"."classifier"("id_orga");
-- Table Triggers
create trigger update_classifier_modtime 
before update on mcds.classifier 
for each row execute function mcds.update_modified_column();

create trigger insert_classifier_cretime 
before insert on mcds.classifier 
for each row execute function mcds.fill_created_column();


-- Table classifier_attribute
CREATE TABLE "mcds"."classifier_attribute" (
    "id"        SERIAL                      NOT NULL, 
    "code"      VARCHAR                     NOT NULL, 
    "id_clas"   INTEGER                     NOT NULL, 
    "name"      TEXT                        NOT NULL, 
    "format"    TEXT DEFAULT 'TEXT'         NOT NULL, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_classifierattribute" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."classifier_attribute" IS 'Classifier attribute definitions';
COMMENT ON COLUMN "mcds"."classifier_attribute"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."classifier_attribute"."code" IS 'The attribute code';
COMMENT ON COLUMN "mcds"."classifier_attribute"."id_clas" IS 'The reference to the attribute owner classifier';
COMMENT ON COLUMN "mcds"."classifier_attribute"."name" IS 'The attribute human-readable name';
COMMENT ON COLUMN "mcds"."classifier_attribute"."format" IS 'The attribute value datatype. Allowed types are:  "BOOLEAN" - allowed values are "0" or "1", "DATE" - allowed values must be on format of iso date YYYY-MM-DD, "INTEGER" - values must be integer numbers, 	- "TEXT" - any sequence od characters is allowed.';
COMMENT ON COLUMN "mcds"."classifier_attribute"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."classifier_attribute"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_classifierattribute_classifier" ON "mcds"."classifier_attribute"("id_clas");
-- Table Triggers
create trigger update_classifier_attribute_modtime 
before update on mcds.classifier_attribute 
for each row execute function mcds.update_modified_column();

create trigger insert_classifier_attribute_cretime 
before insert on mcds.classifier_attribute 
for each row execute function mcds.fill_created_column();


-- Table classifier_attribute_value
CREATE TABLE "mcds"."classifier_attribute_value" (
    "id"        SERIAL                      NOT NULL, 
    "id_clat"   INTEGER                     NOT NULL, 
    "id_clit"   INTEGER                     NOT NULL, 
    "value"     TEXT, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_classifierattributevalue" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."classifier_attribute_value" IS 'Classifier item atribute values';
COMMENT ON COLUMN "mcds"."classifier_attribute_value"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."classifier_attribute_value"."id_clat" IS 'Reference to attribute definition';
COMMENT ON COLUMN "mcds"."classifier_attribute_value"."id_clit" IS 'Reference to classifier item';
COMMENT ON COLUMN "mcds"."classifier_attribute_value"."value" IS 'attribute value';
COMMENT ON COLUMN "mcds"."classifier_attribute_value"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."classifier_attribute_value"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_classifierattributevalue_classifierattribute" ON "mcds"."classifier_attribute_value"("id_clat");
CREATE INDEX "ixfk_classifierattributevalue_classifieritem" ON "mcds"."classifier_attribute_value"("id_clit");
-- Table Triggers
create trigger update_classifier_attribute_value_modtime 
before update on mcds.classifier_attribute_value 
for each row execute function mcds.update_modified_column();

create trigger insert_classifier_attribute_value_cretime 
before insert on mcds.classifier_attribute_value 
for each row execute function mcds.fill_created_column();


-- Table classifier_item
CREATE TABLE "mcds"."classifier_item" (
    "id"            SERIAL                      NOT NULL, 
    "id_class"      INTEGER                     NOT NULL, 
    "code"          TEXT                        NOT NULL, 
    "text"          TEXT, 
    "valid_from"    date                        NOT NULL, 
    "valid_until"   date, 
    "seq_no"        INTEGER, 
    "id_upit"       INTEGER, 
    "id_clve"       INTEGER, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_classifieritem" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."classifier_item" IS 'Classifier items';
COMMENT ON COLUMN "mcds"."classifier_item"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."classifier_item"."id_class" IS 'Refference to classifier';
COMMENT ON COLUMN "mcds"."classifier_item"."code" IS 'Code of classifier item';
COMMENT ON COLUMN "mcds"."classifier_item"."text" IS 'Human-readable name of classifier item';
COMMENT ON COLUMN "mcds"."classifier_item"."valid_from" IS 'Start of validity of the classifier item';
COMMENT ON COLUMN "mcds"."classifier_item"."valid_until" IS 'End of validity of the classifier item';
COMMENT ON COLUMN "mcds"."classifier_item"."seq_no" IS 'Sequence nr oi item';
COMMENT ON COLUMN "mcds"."classifier_item"."id_upit" IS 'Reference to upper iten in hierarchi';
COMMENT ON COLUMN "mcds"."classifier_item"."id_clve" IS 'Reference to classifier version';
COMMENT ON COLUMN "mcds"."classifier_item"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."classifier_item"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_classifieritem_classifier" ON "mcds"."classifier_item"("id_class");
CREATE INDEX "ixfk_classifieritem_classifierversion" ON "mcds"."classifier_item"("id_clve");
CREATE INDEX "ix_classifier_item_code" ON "mcds"."classifier_item" ("code");

-- Table Triggers
create trigger update_classifier_item_modtime 
before update on mcds.classifier_item 
for each row execute function mcds.update_modified_column();

create trigger insert_classifier_item_cretime 
before insert on mcds.classifier_item 
for each row execute function mcds.fill_created_column();



-- Table classifier_item_link
CREATE TABLE "mcds"."classifier_item_link" (
    "id"            SERIAL                      NOT NULL, 
    "id_clit1"      INTEGER                     NOT NULL, 
    "id_clit2"      INTEGER                     NOT NULL, 
    "link_type"     TEXT, 
    "description"   TEXT, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_classifieritemlink" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."classifier_item_link" IS 'Links between classifier items';
COMMENT ON COLUMN "mcds"."classifier_item_link"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."classifier_item_link"."id_clit1" IS 'Viide klassifikaatori elemendile, mis on seotud teise elemendiga';
COMMENT ON COLUMN "mcds"."classifier_item_link"."id_clit2" IS 'Reference to the first linked element';
COMMENT ON COLUMN "mcds"."classifier_item_link"."link_type" IS 'Reference to second linked entity';
COMMENT ON COLUMN "mcds"."classifier_item_link"."description" IS 'description of link';
COMMENT ON COLUMN "mcds"."classifier_item_link"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."classifier_item_link"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_classifieritemlink_classifieritem" ON "mcds"."classifier_item_link"("id_clit1");
CREATE INDEX "ixfk_classifieritemlink_classifieritem_02" ON "mcds"."classifier_item_link"("id_clit2");
-- Table Triggers
create trigger update_classifier_item_link_modtime 
before update on mcds.classifier_item_link 
for each row execute function mcds.update_modified_column();

create trigger insert_classifier_item_link_cretime 
before insert on mcds.classifier_item_link 
for each row execute function mcds.fill_created_column();


-- Table classifier_version
CREATE TABLE "mcds"."classifier_version" (
    "id"            SERIAL                      NOT NULL, 
    "code"          TEXT                        NOT NULL, 
    "name"          TEXT                        NOT NULL, 
    "description"   TEXT, 
    "valid"         BOOLEAN DEFAULT TRUE        NOT NULL, 
    "valid_from"    date                        NOT NULL, 
    "valid_until"   date, "id_clas" INTEGER     NOT NULL, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_classifierversion" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."classifier_version" IS 'Classifier versions';
COMMENT ON COLUMN "mcds"."classifier_version"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."classifier_version"."code" IS 'Version code';
COMMENT ON COLUMN "mcds"."classifier_version"."name" IS 'human-readable name of version';
COMMENT ON COLUMN "mcds"."classifier_version"."description" IS 'description';
COMMENT ON COLUMN "mcds"."classifier_version"."valid" IS 'id version applicable';
COMMENT ON COLUMN "mcds"."classifier_version"."valid_from" IS 'applicable from';
COMMENT ON COLUMN "mcds"."classifier_version"."valid_until" IS 'applicable until';
COMMENT ON COLUMN "mcds"."classifier_version"."id_clas" IS 'reference to classifier';
COMMENT ON COLUMN "mcds"."classifier_version"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."classifier_version"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_classifierversion_classifier" ON "mcds"."classifier_version"("id_clas");
-- Table Triggers
create trigger update_classifier_version_modtime 
before update on mcds.classifier_version 
for each row execute function mcds.update_modified_column();

create trigger insert_classifier_version_cretime 
before insert on mcds.classifier_version 
for each row execute function mcds.fill_created_column();


-- Table event_log
CREATE TABLE "mcds"."event_log" (
    "id"                BIGSERIAL                   NOT NULL, 
    "event_type"        INTEGER                     NOT NULL, 
    "erp"               INTEGER, 
    "related_records"   TEXT, 
    "success"           BOOLEAN                     NOT NULL, 
    "message"           TEXT, 
    "created"           TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"          TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_table1" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."event_log" IS 'System event log';
COMMENT ON COLUMN "mcds"."event_log"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."event_log"."event_type" IS 'reference to event type classifier';
COMMENT ON COLUMN "mcds"."event_log"."erp" IS 'Reference to the ERP that initiated the event. Empty when the event is initiated by the system';
COMMENT ON COLUMN "mcds"."event_log"."related_records" IS 'References to event-related entries in the form "table: id"';
COMMENT ON COLUMN "mcds"."event_log"."success" IS 'Kas sĆ¼ndmus Ćµnnestus?';
COMMENT ON COLUMN "mcds"."event_log"."message" IS 'Possible message about the reasons for the event failure.';
COMMENT ON COLUMN "mcds"."event_log"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."event_log"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_eventlog_classifieritem" ON "mcds"."event_log"("event_type");
CREATE INDEX "ixfk_eventlog_classifieritem_02" ON "mcds"."event_log"("erp");
-- Table Triggers
create trigger update_event_log_modtime 
before update on mcds.event_log 
for each row execute function mcds.update_modified_column();

create trigger insert_event_log_cretime 
before insert on mcds.event_log 
for each row execute function mcds.fill_created_column();


-- Table invoice
CREATE TABLE "mcds"."invoice" (
    "id"                SERIAL                      NOT NULL, 
    "id_acen"           INTEGER                     NOT NULL, 
    "invoice"           TEXT                        NOT NULL, 
    "generation_time"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "created"           TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"          TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_einvoice" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."invoice" IS 'Invoices';
COMMENT ON COLUMN "mcds"."invoice"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."invoice"."id_acen" IS 'Reference to the entry from which the invoice was generated.';
COMMENT ON COLUMN "mcds"."invoice"."invoice" IS 'E-invoice in external format (UBL EN XML).';
COMMENT ON COLUMN "mcds"."invoice"."generation_time" IS 'Time of invoice compiled by e-invoice operator';
COMMENT ON COLUMN "mcds"."invoice"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."invoice"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_einvoice_accontingentry" ON "mcds"."invoice"("id_acen");
-- Table Triggers
create trigger update_invoice_modtime 
before update on mcds.invoice 
for each row execute function mcds.update_modified_column();

create trigger insert_invoice_cretime 
before insert on mcds.invoice 
for each row execute function mcds.fill_created_column();



-- Table invoice_status
CREATE TABLE "mcds"."invoice_status" (
    "id"        SERIAL                      NOT NULL, 
    "id_invo"   INTEGER                     NOT NULL, 
    "status"    TEXT                        NOT NULL, 
    "message"   TEXT, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_einvoicestatus" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."invoice_status" IS 'Invoice statuses';
COMMENT ON COLUMN "mcds"."invoice_status"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."invoice_status"."id_invo" IS 'Reference ti invoice';
COMMENT ON COLUMN "mcds"."invoice_status"."status" IS 'E-invoice status. Possible values are: "GENERATED", "FORWARDED TO OPERATOR", "FROM OPERATOR_NO VALID", "REJECTED".';
COMMENT ON COLUMN "mcds"."invoice_status"."message" IS 'Possible additional information or error message that was the basis for the status.';
COMMENT ON COLUMN "mcds"."invoice_status"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."invoice_status"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_einvoicestatus_einvoice" ON "mcds"."invoice_status"("id_invo");
-- Table Triggers
create trigger update_invoice_status_modtime 
before update on mcds.invoice_status 
for each row execute function mcds.update_modified_column();

create trigger insert_invoice_status_cretime 
before insert on mcds.invoice_status 
for each row execute function mcds.fill_created_column();



-- Table organization
CREATE TABLE "mcds"."organization" (
    "id"                SERIAL                                          NOT NULL, 
    "regnr"             TEXT                                            NOT NULL, 
    "name"              TEXT                                            NOT NULL, 
    "address"           TEXT, 
    "contact_details"   TEXT                                            NOT NULL, 
    "deactivated"       TIMESTAMP WITH TIME ZONE, 
    "reg_authority"     VARCHAR DEFAULT 'http://www.ariregister.rik.ee' NOT NULL, 
    "created"           TIMESTAMP WITH TIME ZONE                        NOT NULL, 
    "modified"          TIMESTAMP WITH TIME ZONE                        NOT NULL, 
    CONSTRAINT "pk_organization" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."organization" IS 'Organizations';
COMMENT ON COLUMN "mcds"."organization"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."organization"."regnr" IS 'Organization registration number or other unique identifier that identifies the organization.';
COMMENT ON COLUMN "mcds"."organization"."name" IS 'Name of organization';
COMMENT ON COLUMN "mcds"."organization"."address" IS 'Contact address of the organization.';
COMMENT ON COLUMN "mcds"."organization"."contact_details" IS 'Contact telephone, e-mail and other contact details of the organization.';
COMMENT ON COLUMN "mcds"."organization"."deactivated" IS 'The time from which the organization is not available in the system';
COMMENT ON COLUMN "mcds"."organization"."reg_authority" IS 'Issuer of registration code (default Estonia)';
COMMENT ON COLUMN "mcds"."organization"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."organization"."modified" IS 'Time of DB record last modified';

-- Table Triggers
create trigger update_organization_modtime 
before update on mcds.organization 
for each row execute function mcds.update_modified_column();

create trigger insert_organization_cretime 
before insert on mcds.organization 
for each row execute function mcds.fill_created_column();


-- Table organization_report_type
CREATE TABLE "mcds"."organization_report_type" (
    "id"            SERIAL                      NOT NULL, 
    "report_type"   INTEGER                     NOT NULL, 
    "id_orga"       INTEGER                     NOT NULL, 
    "deactivated"   TIMESTAMP WITH TIME ZONE, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_organization_report_type" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."organization_report_type" IS 'Reports that the organization must submit.';
COMMENT ON COLUMN "mcds"."organization_report_type"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."organization_report_type"."report_type" IS 'Reference to the report type classifier.';
COMMENT ON COLUMN "mcds"."organization_report_type"."id_orga" IS 'Reference to the organization with the reporting obligation.';
COMMENT ON COLUMN "mcds"."organization_report_type"."deactivated" IS 'The time from which the reporting obligation is deactivated.';
COMMENT ON COLUMN "mcds"."organization_report_type"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."organization_report_type"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_organizationreporttypes_classifieritem" ON "mcds"."organization_report_type"("report_type");
CREATE INDEX "ixfk_organizationreporttypes_organization" ON "mcds"."organization_report_type"("id_orga");
-- Table Triggers
create trigger update_organization_report_type_modtime 
before update on mcds.organization_report_type 
for each row execute function mcds.update_modified_column();

create trigger insert_organization_report_type_cretime 
before insert on mcds.organization_report_type 
for each row execute function mcds.fill_created_column();



-- Table package
CREATE TABLE "mcds"."package" (
    "id"                SERIAL                      NOT NULL, 
    "package_uuid"      UUID                        NOT NULL, 
    "erp"               INTEGER                     NOT NULL, 
    "xbrlgl"            TEXT, "valid" BOOLEAN, 
    "xbrl_gl_version"   VARCHAR, 
    "created"           TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"          TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_package" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."package" IS 'Package from an ERP system that contains XBRL GL content.';
COMMENT ON COLUMN "mcds"."package"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."package"."package_uuid" IS 'Global unique identifier of the package.';
COMMENT ON COLUMN "mcds"."package"."erp" IS 'Reference to package owner erp';
COMMENT ON COLUMN "mcds"."package"."xbrlgl" IS 'Package content as xbrl-gl';
COMMENT ON COLUMN "mcds"."package"."valid" IS 'Indicates whether the contents of the package are valid against XBRL GL xsd and IoB business rules.';
COMMENT ON COLUMN "mcds"."package"."xbrl_gl_version" IS 'xbrl gl taxonomy and IoB business rules version';
COMMENT ON COLUMN "mcds"."package"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."package"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_package_classifieritem" ON "mcds"."package"("erp");
-- Table Triggers
create trigger update_package_modtime 
before update on mcds."package" 
for each row execute function mcds.update_modified_column();

create trigger insert_package_cretime 
before insert on mcds.package 
for each row execute function mcds.fill_created_column();


-- Table package_status
CREATE TABLE "mcds"."package_status" (
    "id"        SERIAL                      NOT NULL, 
    "id_pack"   INTEGER                     NOT NULL, 
    "status"    TEXT                        NOT NULL, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_packagestatus" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."package_status" IS 'Package statuses';
COMMENT ON COLUMN "mcds"."package_status"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."package_status"."id_pack" IS 'Reference to package';
COMMENT ON COLUMN "mcds"."package_status"."status" IS 'Status';
COMMENT ON COLUMN "mcds"."package_status"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."package_status"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_packagestatus_package" ON "mcds"."package_status"("id_pack");
-- Table Triggers
create trigger update_package_status_modtime 
before update on mcds.package_status 
for each row execute function mcds.update_modified_column();

create trigger insert_package_status_cretime 
before insert on mcds.package_status 
for each row execute function mcds.fill_created_column();



-- Table permission
CREATE TABLE "mcds"."permission" (
    "id"            SERIAL                      NOT NULL, 
    "erp"           INTEGER                     NOT NULL, 
    "id_orga"       INTEGER, 
    "business_area" TEXT, 
    "report_type"   INTEGER, 
    "deactivated"   TIMESTAMP WITH TIME ZONE, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_permission" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."permission" IS 'Permissions';
COMMENT ON COLUMN "mcds"."permission"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."permission"."erp" IS 'Reference to erp';
COMMENT ON COLUMN "mcds"."permission"."id_orga" IS 'Reference to organization';
COMMENT ON COLUMN "mcds"."permission"."business_area" IS 'Which subsystem data can be processed by ERP.';
COMMENT ON COLUMN "mcds"."permission"."report_type" IS 'Reference to report type';
COMMENT ON COLUMN "mcds"."permission"."deactivated" IS 'The time from which the authorization is invalid';
COMMENT ON COLUMN "mcds"."permission"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."permission"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_permission_classifieritem" ON "mcds"."permission"("report_type");
CREATE INDEX "ixfk_permission_classifieritem_02" ON "mcds"."permission"("erp");
CREATE INDEX "ixfk_permission_organization" ON "mcds"."permission"("id_orga");
-- Table Triggers
create trigger update_permission_modtime 
before update on mcds.permission 
for each row execute function mcds.update_modified_column();

create trigger insert_permission_cretime 
before insert on mcds.permission 
for each row execute function mcds.fill_created_column();



-- Table report
CREATE TABLE "mcds"."report" (
    "id"            SERIAL                      NOT NULL, 
    "report_uuid"   VARCHAR                     NOT NULL, 
    "id_orga"       INTEGER                     NOT NULL, 
    "report_type"   INTEGER                     NOT NULL, 
    "deadline"      date                        NOT NULL, 
    "content"       TEXT                        NOT NULL, 
    "period_from"   date, 
    "period_until"  date, 
    "external_id"   VARCHAR, 
    "created"       TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"      TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_report" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."report" IS 'Reports';
COMMENT ON COLUMN "mcds"."report"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."report"."report_uuid" IS 'Public uniquie identifier of report';
COMMENT ON COLUMN "mcds"."report"."id_orga" IS 'Reference to organization';
COMMENT ON COLUMN "mcds"."report"."report_type" IS 'Reference to report type classifier';
COMMENT ON COLUMN "mcds"."report"."deadline" IS 'Deadline for submission of the report.';
COMMENT ON COLUMN "mcds"."report"."content" IS 'Content of report';
COMMENT ON COLUMN "mcds"."report"."period_from" IS 'Beginning of the reporting period';
COMMENT ON COLUMN "mcds"."report"."period_until" IS 'End of the reporting period';
COMMENT ON COLUMN "mcds"."report"."external_id" IS 'An external identifier obtained from the organization that received the report';
COMMENT ON COLUMN "mcds"."report"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."report"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_report_classifieritem" ON "mcds"."report"("report_type");
CREATE INDEX "ixfk_report_organization" ON "mcds"."report"("id_orga");
-- Table Triggers
create trigger update_report_modtime 
before update on mcds.report 
for each row execute function mcds.update_modified_column();

create trigger insert_report_cretime 
before insert on mcds.report 
for each row execute function mcds.fill_created_column();



-- Table report_entry
CREATE TABLE "mcds"."report_entry" (
    "id"        SERIAL                      NOT NULL, 
    "id_repo"   INTEGER                     NOT NULL, 
    "id_acen"   INTEGER                     NOT NULL, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_reportentry" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."report_entry" IS 'References to entries in the report';
COMMENT ON COLUMN "mcds"."report_entry"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."report_entry"."id_repo" IS 'Reference to report';
COMMENT ON COLUMN "mcds"."report_entry"."id_acen" IS 'Reference to accounting entry';
COMMENT ON COLUMN "mcds"."report_entry"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."report_entry"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_reportentry_report" ON "mcds"."report_entry"("id_repo");
-- Table Triggers
create trigger update_report_entry_modtime 
before update on mcds.report_entry 
for each row execute function mcds.update_modified_column();

create trigger insert_report_entry_cretime 
before insert on mcds.report_entry 
for each row execute function mcds.fill_created_column();



-- Table report_status
CREATE TABLE "mcds"."report_status" (
    "id"        SERIAL                      NOT NULL, 
    "id_repo"   INTEGER                     NOT NULL, 
    "status"    TEXT                        NOT NULL, 
    "info"      TEXT, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "pk_reportstatus" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."report_status" IS 'Report statuses';
COMMENT ON COLUMN "mcds"."report_status"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."report_status"."id_repo" IS 'Reference to report';
COMMENT ON COLUMN "mcds"."report_status"."status" IS 'Report status. Possible values are: "GENERATED", "CANCELED", "TRANSMITTED".';
COMMENT ON COLUMN "mcds"."report_status"."info" IS 'Possible additional information';
COMMENT ON COLUMN "mcds"."report_status"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."report_status"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_reportstatus_report" ON "mcds"."report_status"("id_repo");
-- Table Triggers
create trigger update_report_status_modtime 
before update on mcds.report_status 
for each row execute function mcds.update_modified_column();

create trigger insert_report_status_cretime 
before insert on mcds.report_status 
for each row execute function mcds.fill_created_column();



-- Table validation_result
CREATE TABLE "mcds"."validation_result" (
    "id"        BIGSERIAL                   NOT NULL, 
    "rule"      TEXT                        NOT NULL, 
    "message"   TEXT                        NOT NULL, 
    "id_acen"   INTEGER                     NOT NULL, 
    "created"   TIMESTAMP WITH TIME ZONE    NOT NULL, 
    "modified"  TIMESTAMP WITH TIME ZONE    NOT NULL, 
    CONSTRAINT "validation_result_pk" PRIMARY KEY ("id")
);
COMMENT ON TABLE "mcds"."validation_result" IS 'Entry validation errors and warnings';
COMMENT ON COLUMN "mcds"."validation_result"."rule" IS 'The code of the validation rule against which the given error occurred.';
COMMENT ON COLUMN "mcds"."validation_result"."message" IS 'Error message';
COMMENT ON COLUMN "mcds"."validation_result"."id_acen" IS 'Reference to accounting entry';
COMMENT ON COLUMN "mcds"."validation_result"."id" IS 'Primary key';
COMMENT ON COLUMN "mcds"."validation_result"."created" IS 'Time of DB record created';
COMMENT ON COLUMN "mcds"."validation_result"."modified" IS 'Time of DB record last modified';
-- Indexes
CREATE INDEX "ixfk_validationresult_accontingentry" ON "mcds"."validation_result"("id_acen");
-- Table Triggers
create trigger update_validation_result_modtime 
before update on mcds.validation_result 
for each row execute function mcds.update_modified_column();

create trigger insert_validation_result_cretime 
before insert on mcds.validation_result 
for each row execute function mcds.fill_created_column();




/* ==============
    CONSTRAINTS
   ============== */
ALTER TABLE "mcds"."accounting_entry_detail" ADD CONSTRAINT "accounting_entry_detail_id_acen_fkey" FOREIGN KEY ("id_acen") REFERENCES "mcds"."accounting_entry" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."accounting_entry" ADD CONSTRAINT "accounting_entry_un" UNIQUE ("entry_uuid");
ALTER TABLE "mcds"."accounting_entry" ADD CONSTRAINT "fk_accontingentry_organization" FOREIGN KEY ("id_orga") REFERENCES "mcds"."organization" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."accounting_entry" ADD CONSTRAINT "fk_accontingentry_package" FOREIGN KEY ("id_pack") REFERENCES "mcds"."package" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."accounting_entry" ADD CONSTRAINT "fk_accounting_entry_classifier_item" FOREIGN KEY ("entry_type") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."classifier" ADD CONSTRAINT "classifier_un" UNIQUE ("code");

ALTER TABLE "mcds"."classifier_attribute" ADD CONSTRAINT "un_code_class" UNIQUE ("code", "id_clas");
ALTER TABLE "mcds"."classifier_attribute" ADD CONSTRAINT "fk_classifierattribute_classifier" FOREIGN KEY ("id_clas") REFERENCES "mcds"."classifier" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."classifier_attribute_value" ADD CONSTRAINT "fk_classifierattributevalue_classifierattribute" FOREIGN KEY ("id_clat") REFERENCES "mcds"."classifier_attribute" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."classifier_attribute_value" ADD CONSTRAINT "fk_classifierattributevalue_classifieritem" FOREIGN KEY ("id_clit") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."classifier_item" ADD CONSTRAINT "un_class_item" UNIQUE ("id_class", "code", "id_clve");
ALTER TABLE "mcds"."classifier_item" ADD CONSTRAINT "fk_classifieritem_classifier" FOREIGN KEY ("id_class") REFERENCES "mcds"."classifier" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."classifier_item" ADD CONSTRAINT "fk_classifieritem_classifierversion" FOREIGN KEY ("id_clve") REFERENCES "mcds"."classifier_version" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."classifier_item_link" ADD CONSTRAINT "fk_classifieritemlink_classifieritem" FOREIGN KEY ("id_clit1") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."classifier_item_link" ADD CONSTRAINT "fk_classifieritemlink_classifieritem_02" FOREIGN KEY ("id_clit2") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."classifier_version" ADD CONSTRAINT "un_code" UNIQUE ("code", "id_clas");
ALTER TABLE "mcds"."classifier_version" ADD CONSTRAINT "fk_classifierversion_classifier" FOREIGN KEY ("id_clas") REFERENCES "mcds"."classifier" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."organization" ADD CONSTRAINT "un_org_regnr" UNIQUE ("regnr");

ALTER TABLE "mcds"."invoice" ADD CONSTRAINT "fk_einvoice_accontingentry" FOREIGN KEY ("id_acen") REFERENCES "mcds"."accounting_entry" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."invoice_status" ADD CONSTRAINT "fk_einvoicestatus_einvoice" FOREIGN KEY ("id_invo") REFERENCES "mcds"."invoice" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."event_log" ADD CONSTRAINT "fk_eventlog_classifieritem" FOREIGN KEY ("event_type") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."event_log" ADD CONSTRAINT "fk_eventlog_classifieritem_02" FOREIGN KEY ("erp") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."organization_report_type" ADD CONSTRAINT "un_org_repo_type" UNIQUE ("report_type", "id_orga");
ALTER TABLE "mcds"."organization_report_type" ADD CONSTRAINT "fk_organizationreporttypes_classifieritem" FOREIGN KEY ("report_type") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."organization_report_type" ADD CONSTRAINT "fk_organizationreporttypes_organization" FOREIGN KEY ("id_orga") REFERENCES "mcds"."organization" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."package" ADD CONSTRAINT "un_package_uuid" UNIQUE ("package_uuid");
ALTER TABLE "mcds"."package" ADD CONSTRAINT "fk_package_classifieritem" FOREIGN KEY ("erp") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."package_status" ADD CONSTRAINT "fk_packagestatus_package" FOREIGN KEY ("id_pack") REFERENCES "mcds"."package" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."permission" ADD CONSTRAINT "un_perm" UNIQUE ("erp", "id_orga", "report_type", "business_area");
ALTER TABLE "mcds"."permission" ADD CONSTRAINT "fk_permission_classifieritem" FOREIGN KEY ("report_type") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."permission" ADD CONSTRAINT "fk_permission_classifieritem_02" FOREIGN KEY ("erp") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."permission" ADD CONSTRAINT "fk_permission_organization" FOREIGN KEY ("id_orga") REFERENCES "mcds"."organization" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."report" ADD CONSTRAINT "fk_report_classifieritem" FOREIGN KEY ("report_type") REFERENCES "mcds"."classifier_item" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."report" ADD CONSTRAINT "fk_report_organization" FOREIGN KEY ("id_orga") REFERENCES "mcds"."organization" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."report_entry" ADD CONSTRAINT "fk_reportentry_accontingentry" FOREIGN KEY ("id_acen") REFERENCES "mcds"."accounting_entry" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mcds"."report_entry" ADD CONSTRAINT "fk_reportentry_report" FOREIGN KEY ("id_repo") REFERENCES "mcds"."report" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."report_status" ADD CONSTRAINT "fk_reportstatus_report" FOREIGN KEY ("id_repo") REFERENCES "mcds"."report" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "mcds"."validation_result" ADD CONSTRAINT "fk_validationresult_accontingentry" FOREIGN KEY ("id_acen") REFERENCES "mcds"."accounting_entry" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

