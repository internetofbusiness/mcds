/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.impl;

import ee.tieto.mcds.rest.repository.ParameterSetter;
import ee.tieto.mcds.rest.repository.PermissionRepositoryCustom;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.model.PermissionEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PermissionRepositoryImpl implements PermissionRepositoryCustom {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<PermissionEntity> findPermissionsIgnoreNulls(
      String erpCode, String orgRegNr, String businessArea, String reportTypeCode, boolean findOnlyActivePermissions) {
    String query = "SELECT p FROM PermissionEntity p WHERE 1=1";

    Map<String, Object> params = new HashMap<>();
    if (erpCode != null) {
      query += " AND p.erp.code = :erpCode";
      params.put("erpCode", erpCode);
    }
    if (orgRegNr != null){
      query += " AND p.organization.regnr = :orgRegNr";
      params.put("orgRegNr", orgRegNr);
    }
    if (businessArea != null){
      query += " AND p.businessArea = :businessArea";
      params.put("businessArea", businessArea);

    }
    if (reportTypeCode != null){
      query += " AND p.reportType.code = :reportTypeCode";
      params.put("reportTypeCode", reportTypeCode);
    }

    if (findOnlyActivePermissions) {
      query += " AND (p.deactivated IS NULL OR p.deactivated > :currentTime)";
      params.put("currentTime", OffsetDateTime.now());
    }

    TypedQuery<PermissionEntity> qu = entityManager.createQuery(query, PermissionEntity.class);
    qu = ParameterSetter.setParameters(qu, params);
    return qu.getResultList();
  }

  @Override
  public List<PermissionEntity> findPermissionsUseNulls (
      String erpCode, String orgRegNr, String businessArea, String reportTypeCode) {

    String query = "SELECT p FROM PermissionEntity p " +
        "left join p.erp e left join p.organization o left join p.reportType rt " +
        "WHERE " +
        "((:erpCode is null and e.code is null) or e.code = :erpCode) " +
        "AND ((:orgRegNr is null and o.regnr is null) or o.regnr = :orgRegNr) " +
        "AND ((:businessArea is null and p.businessArea is null) or p.businessArea = :businessArea) " +
        "AND ((:reportType is null and rt.code is null) or rt.code = :reportType)";

    return entityManager.createQuery(query, PermissionEntity.class)
        .setParameter("erpCode", erpCode)
        .setParameter("orgRegNr", orgRegNr)
        .setParameter("businessArea", businessArea)
        .setParameter("reportType", reportTypeCode)
        .getResultList();
  }

  @Override
  public List<PermissionEntity> findByErpCodeAndDate(String erpCode, OffsetDateTime dateTime) {
    String query =
        "SELECT p " +
        "FROM " +
        "  ClassifierEntity c " +
        "  join ClassifierItemEntity ci on ci.classifier = c " +
        "  join PermissionEntity p on p.erp = ci " +
        "  join fetch OrganizationEntity o on o = p.organization " +
        "WHERE " +
        "  c.code = :erpClassifierCode " +
        "  and ci.code = :erpCode " +
        "  and (p.deactivated is null or p.deactivated > :dateTime) ";

    TypedQuery<PermissionEntity> qu = entityManager.createQuery(query, PermissionEntity.class);
    qu.setParameter("erpClassifierCode", Classifier.ERP.name());
    qu.setParameter("erpCode", erpCode);
    qu.setParameter("dateTime", dateTime);
    return qu.getResultList();

  }

  @Override
  public boolean isPermittedForErp(String erpCode, String regNr, String businessArea, OffsetDateTime time, boolean ignoreBusinessArea) {
    String select =
        "select count(p) " +
            "from " +
            "ClassifierEntity c " +
            "join ClassifierItemEntity erp on erp.classifier = c " +
            "join PermissionEntity p on p.erp = erp " +
            "join OrganizationEntity o on o = p.organization " +
            "where " +
            "c.code = 'ERP' " +
            "and erp.code = :erp " +
            "and (erp.validFrom is null or erp.validFrom <= :localDate) " +
            "and (erp.validUntil is null or erp.validUntil >= :localDate) " +
            "and (p.deactivated is null or p.deactivated <= :time) " +
            "and o.regnr = :regNr ";

    if (!ignoreBusinessArea) {
      if (businessArea == null) {
        select += "and p.businessArea is null ";
      } else {
        select += "and p.businessArea = :businessArea ";
      }
    }

    LocalDate localDate = time.toLocalDate();
    TypedQuery<Long> qu = entityManager.createQuery(select, Long.class)
        .setParameter("erp", erpCode)
        .setParameter("localDate", localDate)
        .setParameter("time", time)
        .setParameter("regNr", regNr);
    if (!ignoreBusinessArea && businessArea != null) {
      qu.setParameter("erp", erpCode);
    }
    return qu.getSingleResult() > 0;
  }
}
