/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Table(name = "organization_report_type", schema = "mcds", catalog = "mcds")
public class OrganizationReportTypeEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "report_type", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ClassifierItemEntity reportType;

    @JoinColumn(name = "id_orga", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private OrganizationEntity organization;

    @Column(name = "deactivated")
    private OffsetDateTime deactivated;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private List<ClassifierItemEntity> classifierItems;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClassifierItemEntity getReportType() {
        return reportType;
    }

    public void setReportType(ClassifierItemEntity reportType) {
        this.reportType = reportType;
    }

    public OrganizationEntity getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationEntity idOrga) {
        this.organization = idOrga;
    }

    public OffsetDateTime getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(OffsetDateTime deactivated) {
        this.deactivated = deactivated;
    }

    public List<ClassifierItemEntity> getClassifierItems() {
        return classifierItems;
    }

    public void setClassifierItems(List<ClassifierItemEntity> classifierItems) {
        this.classifierItems = classifierItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof OrganizationReportTypeEntity))
            return false;

        OrganizationReportTypeEntity other = (OrganizationReportTypeEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
