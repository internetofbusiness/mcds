/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.Organization;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
public class RestToEntityOrganization extends Converter<Organization, OrganizationEntity> {

  @Override
  public OrganizationEntity convert(Organization source, OrganizationEntity destination, boolean iterate) {
    destination.setRegnr(source.getRegNr());
    destination.setRegAuthority(source.getRegAuthority());
    destination.setName(source.getName());
    destination.setAddress(source.getAddress());
    destination.setContactDetails(source.getContact());
    if (source.isActive()) {
      destination.setDeactivated(null);
    } else {
      if (destination.getDeactivated() == null) {
        destination.setDeactivated(OffsetDateTime.now());
      }
    }
    return destination;
  }
}
