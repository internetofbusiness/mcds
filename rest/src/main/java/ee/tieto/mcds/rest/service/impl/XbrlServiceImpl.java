/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.Pair;
import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.model.EntryInfo;
import ee.tieto.mcds.rest.model.InvoiceInfo;
import ee.tieto.mcds.rest.model.InvoiceStatus;
import ee.tieto.mcds.rest.repository.*;
import ee.tieto.mcds.rest.repository.constants.AccountingEntryStatus;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.constants.PackageStatus;
import ee.tieto.mcds.rest.repository.model.*;
import ee.tieto.mcds.rest.service.AbstractService;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.service.XbrlService;
import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.Validator;
import ee.tieto.mcds.validator.XbrlValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class XbrlServiceImpl extends AbstractService implements XbrlService {
  private static final Logger log = LoggerFactory.getLogger(XbrlServiceImpl.class);

  private final PackageRepository packageRepo;
  private final AccountingEntryRepository entryRepo;
  private final ClassifierItemRepository classItRepo;
  private final PackageStatusRepository packageStatusRepo;
  private final AccountingEntryRepository accountingEntryRepository;
  private final EventLogService eventService;
  private final XbrlValidatorFactory validatorFactory;
  private final XbrlProcessor xbrlProcessor;

  @Value("${mcds.target-application.invoice-operator:e-invoicing_operator}")
  private String targetApplicationInvoice;


  @Autowired
  public XbrlServiceImpl(
      PackageRepository packageRepo,
      AccountingEntryRepository entryRepo, ClassifierItemRepository classItRepo,
      PackageStatusRepository packageStatusRepo,
      AccountingEntryRepository accountingEntryRepository,
      EventLogService eventService,
      XbrlValidatorFactory validatorFactory,
      XbrlProcessor xbrlProcessor) {
    this.packageRepo = packageRepo;
    this.entryRepo = entryRepo;
    this.classItRepo = classItRepo;
    this.packageStatusRepo = packageStatusRepo;
    this.accountingEntryRepository = accountingEntryRepository;
    this.eventService = eventService;
    this.validatorFactory = validatorFactory;
    this.xbrlProcessor = xbrlProcessor;
  }

  @Override
  @Transactional
  public String saveXbrl(String xbrl, String erpCode, String xbrlVersion) {
    PackageEntity entity = null;
    try {
      entity = new PackageEntity();
      entity.setErp(classItRepo.findByClassifierCodeAndItemCode(Classifier.ERP.name(), erpCode)
          .orElseThrow(
              () -> new McdsValidationException("Unknown erp code " + erpCode)
          ));
      entity.setPackageUuid(UUID.randomUUID());
      entity.setXbrlgl(xbrl);
      entity.setXbrlGlVersion(xbrlVersion);
      entity = packageRepo.save(entity);
      addPackageStatus(entity, PackageStatus.SAVED);
      packageRepo.flush();
      eventService.newEvent(
          EventType.XBRL_SAVE, erpCode, "package:" + entity.getId(), true, null);
      return entity.getPackageUuid().toString();
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          EventType.XBRL_SAVE, erpCode,
          "package:" + (entity == null ? "null" : entity.getId()),
          false, e.getMessage());
      throw e;
    }
  }

  private void addPackageStatus(PackageEntity pack, PackageStatus status) {
    List<PackageStatusEntity> statuses = pack.getPackageStatuses();
    if (statuses == null) {
      statuses = new ArrayList<>(1);
      pack.setPackageStatuses(statuses);
    }
    PackageStatusEntity statusEntry = new PackageStatusEntity();
    statusEntry.setPackage(pack);
    statusEntry.setStatus(status);
    packageStatusRepo.save(statusEntry);
  }

  @Override
  @Transactional
  public ValidationResult validate(String xbrl, String packageId, String version) {
    try {
      Validator validator = validatorFactory.getSchemaValidator(version);
      if (validator == null) {
        throw new McdsValidationException("Unsupported xbrl-gl version - " + version);
      }
      ValidationResult result = validator.validate(xbrl);
      PackageEntity pack = packageRepo.findPackageEntityByPackageUuid(UUID.fromString(packageId));
      eventService.newEvent(
          EventType.XBRL_VALIDATE, getUser(),
          EventType.XBRL_VALIDATE.getTable() + ":" + pack.getId(),
          result.isValid(),
          result.getMessages() == null || result.getMessages().isEmpty() ? null : result.getMessages().toString());
      pack.setValid(result.isValid());
      addPackageStatus(pack, result.isValid() ? PackageStatus.SCHEMA_VALID : PackageStatus.SCHEMA_NOT_VALID);
      return result;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      //TODO handle validation errors separatelly
      throw new McdsValidationException(e.getMessage());
    }
  }

  @Override
  @Transactional
  public ValidationResult process(String packageUuid) {
    PackageEntity packEntry = null;
    ValidationResult result = null;
    try {
      packEntry = packageRepo.findPackageEntityByPackageUuid(UUID.fromString(packageUuid));
      result = xbrlProcessor.process(packEntry);
      eventService.newEvent(
          EventType.ENTRY_ADD, getUser(),
          "package:" + packEntry.getId(),
          true, null);
      return result;
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          EventType.ENTRY_ADD, getUser(),
          "package:" + (packEntry == null ? "null" : packEntry.getId().toString()),
          false, e.getMessage());
      throw e;
    }
  }


  @Override
  @Transactional(readOnly = true)
  public List<ee.tieto.mcds.rest.model.PackageStatus> getEntriesStatus(
      String erpCode, UUID sessionId, String entryId,
      LocalDate receivedFrom, LocalDate receivedUntil) {

    Pair<OffsetDateTime, OffsetDateTime> fromUntil = convertToOffsetDateTime(receivedFrom, receivedUntil,
        sessionId == null && entryId == null);
    List<AccountingEntryEntityLightReadOnly> entries = accountingEntryRepository.findEntries(erpCode, sessionId, entryId, fromUntil.getFirst(), fromUntil.getSecond(), OffsetDateTime.now());

    Map<String, ee.tieto.mcds.rest.model.PackageStatus> result = new HashMap<>();
    for (AccountingEntryEntityLightReadOnly entry : entries) {
      ee.tieto.mcds.rest.model.PackageStatus packageStatus = result.get(entry.getPackage().getPackageUuid().toString());
      if (packageStatus == null) {
        packageStatus = new ee.tieto.mcds.rest.model.PackageStatus();
        packageStatus.setSessionId(entry.getPackage().getPackageUuid().toString());
        packageStatus.setTimeOfReceiving(entry.getPackage().getCreated());
        packageStatus.setXbrlGlVersion(entry.getPackage().getXbrlGlVersion());
        packageStatus.setStatus(entry.getPackage().getCurrentStatus());
        packageStatus.setEntryInfo(new ArrayList<>());
      }
      EntryInfo info = new EntryInfo();
      info.setStatus(entry.getValid() ? AccountingEntryStatus.VALID : AccountingEntryStatus.NOT_VALID);
      info.setEntryIdentifier(entry.getEntryUuid());
      info.setErrors(new ArrayList<>());
      for (ValidationResultEntity val : entry.getValidationResults()) {
        info.addError(val.getRule(), val.getMessage());
      }
      info.setInvoiceInfo(getInvoiceInfos(entry));

      packageStatus.getEntryInfo().add(info);
      result.put(packageStatus.getSessionId(), packageStatus);
    }
    return new ArrayList<>(result.values());
  }

  @Override
  @Transactional
  public List<ee.tieto.mcds.rest.model.PackageStatus> getInvoiceEntriesStatus(
      UUID sessionId, String entryId,
      LocalDate receivedFrom, LocalDate receivedUntil){

    Pair<OffsetDateTime, OffsetDateTime> fromUntil = convertToOffsetDateTime(receivedFrom, receivedUntil,
        sessionId == null && entryId == null);
    List<AccountingEntryEntityLightReadOnly> entries = accountingEntryRepository.findInvoiceEntries(sessionId, entryId, fromUntil.getFirst(), fromUntil.getSecond());

    Map<String, ee.tieto.mcds.rest.model.PackageStatus> result = new HashMap<>();
    for (AccountingEntryEntityLightReadOnly entry : entries) {
      ee.tieto.mcds.rest.model.PackageStatus packageStatus = result.get(entry.getPackage().getPackageUuid().toString());
      if (packageStatus == null) {
        packageStatus = new ee.tieto.mcds.rest.model.PackageStatus();
        packageStatus.setSessionId(entry.getPackage().getPackageUuid().toString());
        packageStatus.setTimeOfReceiving(entry.getPackage().getCreated());
        packageStatus.setXbrlGlVersion(entry.getPackage().getXbrlGlVersion());
        packageStatus.setStatus(entry.getPackage().getCurrentStatus());
        packageStatus.setEntryInfo(new ArrayList<>());
      }
      EntryInfo info = new EntryInfo();
      info.setStatus(entry.getValid() ? AccountingEntryStatus.VALID : AccountingEntryStatus.NOT_VALID);
      info.setEntryIdentifier(entry.getEntryUuid());
      info.setErrors(new ArrayList<>());
      for (ValidationResultEntity val : entry.getValidationResults()) {
        info.addError(val.getRule(), val.getMessage());
      }
      info.setInvoiceInfo(getInvoiceInfos(entry));

      packageStatus.getEntryInfo().add(info);
      result.put(packageStatus.getSessionId(), packageStatus);
    }
    return new ArrayList<>(result.values());

  }

  private List<InvoiceInfo> getInvoiceInfos(AccountingEntryEntityLightReadOnly entry) {
    if (!targetApplicationInvoice.equals(entry.getTargetApplication())) {
      return null;
    }
    InvoiceEntity invoice = entry.getInvoice();
    if (invoice == null) {
      return null;
    }
    List<InvoiceInfo> invoiceInfos = new ArrayList<>(1);
    InvoiceInfo invoiceInfo = new InvoiceInfo();
    invoiceInfos.add(invoiceInfo);
    invoiceInfo.setContent(invoice.getInvoice());
    invoiceInfo.setCompilationTime(invoice.getGenerationTime().format(DateTimeFormatter.ISO_DATE_TIME));
    List<InvoiceStatusEntity> invStatuses = invoice.getInvoiceStatuses();
    if (invStatuses != null && !invStatuses.isEmpty()) {
      invoiceInfo.setStatus(
          invStatuses.stream().map(s -> {
            InvoiceStatus status = new InvoiceStatus();
            status.setStatus(s.getStatus().name());
            status.setMessage(s.getMessage());
            status.setTime(s.getCreated());
            return status;
          }).collect(Collectors.toList()));
    }
    return invoiceInfos;
  }

  private static final class EntriesResult {
    public static EntriesResult getEmpty() {
      EntriesResult ret = new EntriesResult();
      ret.entries = new ArrayList<>(0);
      ret.idList = new ArrayList<>(0);
      return ret;
    }
    private EntriesResult() {
    }
    private EntriesResult(List<String> entries, List<String> idList) {
      this.entries = entries;
      this.idList = idList;
    }
    private List<String> entries;
    private List<String> idList;
  }

  @Override
  @Transactional
  public List<String> getEntries(String regNr, String returnType, LocalDate from, LocalDate until,
                                 String entryType, Set<String> entryIdentifiers) {
    try {
      EntriesResult result = getEntriesInternal(regNr, from, until, entryType, entryIdentifiers, false);
      eventService.newEvent(
          EventType.ENTRY_VIEW, getUser(),
          EventType.ENTRY_VIEW.getTable() + ":" + result.idList,
          true, null);
      return result.entries;
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          EventType.ENTRY_VIEW, getUser(),
          EventType.ENTRY_VIEW.getTable() + ":",
          false, e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public List<String> getInvoiceEntryes(String regNr, String returnType, LocalDate from, LocalDate until,
                                 String entryType, Set<String> entryIdentifiers) {
    try {
      EntriesResult result = getEntriesInternal(regNr, from, until, entryType, entryIdentifiers, true);
      eventService.newEvent(
          EventType.ENTRY_VIEW, getUser(),
          EventType.ENTRY_VIEW.getTable() + ":" + result.idList,
          true, "Invoices");
      return result.entries;
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          EventType.ENTRY_VIEW, getUser(),
          EventType.ENTRY_VIEW.getTable() + ":",
          false, "Invoices - " + e.getMessage());
      throw e;
    }
  }

  public EntriesResult getEntriesInternal(String regNr, LocalDate from, LocalDate until,
                                         String entryType, Set<String> entryIdentifiers, boolean invoices) {
    Pair<OffsetDateTime, OffsetDateTime> fromUntil = convertToOffsetDateTime(from, until,
        entryIdentifiers == null || entryIdentifiers.isEmpty());
    List<AccountingEntryEntity> entries;
    if (invoices) {
      entries = entryRepo.findInvoiceEntries(
          regNr, fromUntil.getFirst(), fromUntil.getSecond(), entryType, entryIdentifiers, OffsetDateTime.now());
    } else {
      String erpCode = getUser();
      entries = entryRepo.findEntries(
          erpCode, regNr, fromUntil.getFirst(), fromUntil.getSecond(), entryType, entryIdentifiers, OffsetDateTime.now());
    }
    if (entries == null || entries.isEmpty()) {
      return EntriesResult.getEmpty();
    }
    final List<String> entriesList = new ArrayList<>(entries.size());
    final List<String> idList = new ArrayList<>(entries.size());
    entries
        .forEach(e -> {
          entriesList.add(Base64.getEncoder().encodeToString(e.getEntry().getBytes(StandardCharsets.UTF_8)));
          idList.add(e.getId().toString());
        });
    return new EntriesResult(entriesList, idList);
  }

  private Pair<OffsetDateTime, OffsetDateTime> convertToOffsetDateTime(LocalDate from, LocalDate until, boolean setIfNull) {
    OffsetDateTime fromDate = null;
    OffsetDateTime untilDate = null;

    if (from != null) {
      fromDate = from.atStartOfDay(ZoneId.systemDefault()).toOffsetDateTime();
    }
    if (until != null) {
      untilDate = until.atStartOfDay(ZoneId.systemDefault()).toOffsetDateTime().plusDays(1).minusNanos(1);
    }

    if (!setIfNull) {
      return Pair.of(fromDate, untilDate);
    }

    if (fromDate == null && untilDate == null) {
      fromDate = OffsetDateTime.now().truncatedTo(ChronoUnit.DAYS)
          .with(TemporalAdjusters.firstDayOfMonth());
      untilDate = OffsetDateTime.now().truncatedTo(ChronoUnit.DAYS)
          .with(TemporalAdjusters.firstDayOfNextMonth()).minusNanos(1);
    } else if (fromDate == null) {
      fromDate = untilDate.with(TemporalAdjusters.firstDayOfMonth());
    } else if (untilDate == null) {
      untilDate = fromDate.with(TemporalAdjusters.firstDayOfNextMonth()).minusNanos(1);
    }

    return Pair.of(fromDate, untilDate);
  }

  //DEBUG
  @Override
  @Transactional
  public void saveSourceJournal() {
    Stream<AccountingEntryEntity> entries = entryRepo.findAllCustom();
    entries.forEach(e-> {
      prepareJId(e);
      entryRepo.flush();
      entryRepo.detach(e);
    });
  }

  private boolean prepareJId(AccountingEntryEntity entry) {
    try {
      String sji = xbrlProcessor.prepareSourceJournalId(entry.getEntry());
      if (sji != null) {
        entry.setSourceJournalId(sji);
      }
      return true;
    } catch (Exception e) {
      log.error("Entry id={}", entry.getId());
      log.error(e.getMessage(), e);
      return false;
    }
  }
}
