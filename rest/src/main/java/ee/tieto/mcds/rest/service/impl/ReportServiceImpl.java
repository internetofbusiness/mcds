/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.McdsDataIntegrityException;
import ee.tieto.mcds.rest.McdsNotFoundException;
import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.converter.EntityToRestReport;
import ee.tieto.mcds.rest.model.Report;
import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.PermissionRepository;
import ee.tieto.mcds.rest.repository.ReportRepository;
import ee.tieto.mcds.rest.repository.ReportStatusRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.constants.ReportStatus;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.PermissionEntity;
import ee.tieto.mcds.rest.repository.model.ReportEntity;
import ee.tieto.mcds.rest.repository.model.ReportStatusEntity;
import ee.tieto.mcds.rest.service.*;
import ee.tieto.mcds.rest.service.report.McdsReportReceivingException;
import ee.tieto.mcds.rest.service.report.McdsReportSenderException;
import ee.tieto.mcds.rest.service.report.ReportSender;
import ee.tieto.mcds.rest.service.report.ReportSenderFactory;
import ee.tieto.mcds.rest.service.report.model.SenderResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class ReportServiceImpl extends AbstractService implements ReportService {
  private static final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);

  private final ClassifierItemRepository classItemRepo;
  private final PermissionRepository permRepo;
  private final ReportRepository reportRepo;
  private final ReportStatusRepository reportStatusRepo;
  private final EntityToRestReport reportConverter;
  private final ReportSenderFactory senderFactory;

  @Autowired
  public ReportServiceImpl(ClassifierItemRepository classItemRepo,
                           PermissionRepository permRepo,
                           ReportRepository reportRepo,
                           ReportStatusRepository reportStatusRepo, EntityToRestReport reportConverter,
                           ReportSenderFactory senderFactory) {
    this.classItemRepo = classItemRepo;
    this.permRepo = permRepo;
    this.reportRepo = reportRepo;
    this.reportStatusRepo = reportStatusRepo;
    this.reportConverter = reportConverter;
    this.senderFactory = senderFactory;
  }

  @Override
  @Transactional
  public List<Report> getReports(String erpCode, List<String> regnrList, String status, String reportType, LocalDate startDate, LocalDate endDate) {
    validate(erpCode, regnrList, reportType, status);
    ReportStatus rStatus = null;
    if (status != null) {
      rStatus = ReportStatus.valueOf(status);
    }
    List<ReportEntity> reports = reportRepo.findReportsIgnoreNulls(regnrList, rStatus, reportType, startDate, endDate);
    return reportConverter.convert(reports);
  }

  @Override
  @Transactional
  public void sendEntry(String id) {
    ReportEntity entity = null;
    try {
      entity = findReportUsePermissions(id, getUser(), false);
      if (entity == null) {
        throw new McdsNotFoundException("Report id=" + id + " not found");
      }
      ReportSender sender = senderFactory.getSender(ReportType.valueOf(entity.getReportType().getCode()));
      SenderResponse response = sender.sendReport(entity);
      if (response.isError()) {
        log.error(response.getErrorMessage());
        if (response.getFault() != null) {
          throw new McdsReportSenderException(response.getErrorMessage());
        }
        throw new McdsReportReceivingException(response.getErrorMessage());
      }

      log.debug(response.getMessage());
      entity.setExternalId(response.getSessionId());
      saveNewStatus(entity, ReportStatus.SUBMITED, EventType.REPORT_SUBMIT);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      eventService.newEventNewTransaction(
          EventType.REPORT_SUBMIT, getUser(),
          EventType.REPORT_SUBMIT.getTable() + ":" + (entity == null ? "" : entity.getId()),
          false, e.getMessage());
      if (e instanceof McdsException) {
        throw e;
      }
      throw new McdsSystemException("Error sending report");
    }
  }

  @Override
  public String estatReturnData(String id) {
    throw new McdsSystemException("Not implemented");
  }

  @Override
  public String estatReturnError(String id) {
    throw new McdsSystemException("Not implemented");
  }

  @Override
  @Transactional
  public String getReportContent(String erpCode, String id) {
    ReportEntity report = findReportUsePermissions(id, erpCode, false);
    return report.getContent();
  }

  @Override
  @Transactional
  public Report cancelReport(String erpCode, String id) {
    ReportEntity report = null;
    try {
      report = findReportUsePermissions(id, erpCode, true);
      saveNewStatus(report, ReportStatus.CANCELED, EventType.REPORT_CANCEL);
      return reportConverter.convert(report);
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          EventType.REPORT_CANCEL, getUser(),
          EventType.REPORT_CANCEL.getTable() + ":" + (report != null ? report.getId() : "null"),
          false, (report != null ? "Report:" + id + " ERROR: " : "" ) + e.getMessage());
      throw e;
    }
  }

  private void saveNewStatus(ReportEntity report, ReportStatus reportStatus, EventType event) {
    try {
      ReportStatusEntity status = new ReportStatusEntity();
      status.setReport(report);
      status.setStatus(reportStatus);
      report.getReportStatuses().add(status);
      reportStatusRepo.save(status);
      //reportStatusRepo.refresh(status);
      //reportRepo.refresh(report);
      eventService.newEvent(
          event, getUser(),
          event.getTable() + ":" + report.getId(),
          true, null);
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          event, getUser(),
          event.getTable() + ":" + (report != null ? report.getId() : "null"),
          false, (report != null ? "Report:" + report.getId() + " ERROR: " : "" ) + e.getMessage());
      throw e;
    }
  }


  private ReportEntity findReportUsePermissions(String id, String erpCode, boolean supressEvent) {
    ReportEntity report = null;
    try {
      report = reportRepo.findByReportUuid(id);
      if (report == null) {
        throw new McdsNotFoundException("Report not found");
      }

      List<PermissionEntity> permissions = permRepo.findPermissionsIgnoreNulls(
          erpCode, report.getOrganization().getRegnr(), null,
          report.getReportType().getCode(), true);
      if (permissions.isEmpty()) {
        throw new McdsRestrictedException("Report " + id + " restricted for erp " + erpCode);
      }
      if (!supressEvent) {
        eventService.newEvent(
            EventType.REPORT_PREVIEW_VIEW, getUser(),
            EventType.REPORT_PREVIEW_VIEW.getTable() + ":" + report.getId(),
            true, null);
      }
      return report;
    } catch (Exception e) {
      if (!supressEvent) {
        eventService.newEventNewTransaction(
            EventType.REPORT_PREVIEW_VIEW, getUser(),
            EventType.REPORT_PREVIEW_VIEW.getTable() + ":" + (report == null ? "null" : report.getId()),
            false,
            (report == null ? id + " - " : "") + e.getMessage());
      }
      throw e;
    }
  }

  private void validate(String erpCode, List<String> regnrList, String reportType, String status) {
    // Check status
    if (status != null && !ReportStatus.contains(status)) {
      throw new McdsValidationException("Invalid status - " + status + ", expected " + Arrays.toString(ReportStatus.values()));
    }
    // Check reportType
    List<ClassifierItemEntity> type = classItemRepo.findClassifierItemByCodes(Classifier.REPORT_TYPE.name(), reportType);
    if (type.size() == 0) {
      throw new McdsValidationException("Unsupported report type - " + reportType);
    }
    if (type.size() > 1) {
      throw new McdsDataIntegrityException("Report type by code - expected one, found " + type.size());
    }
    // Check permissions
    for (String regnr : regnrList) {
      List<PermissionEntity> perm = permRepo.findPermissionsIgnoreNulls(erpCode, regnr, null, reportType, true);
      if (perm.isEmpty()) {
        throw new McdsRestrictedException("Not permitted for " + regnr);
      }
    }
  }

}
