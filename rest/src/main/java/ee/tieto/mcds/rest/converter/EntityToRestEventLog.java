/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.EventLog;
import ee.tieto.mcds.rest.repository.model.EventLogEntity;
import org.springframework.stereotype.Component;

@Component
public class EntityToRestEventLog extends Converter<EventLogEntity, EventLog> {

    @Override
    public EventLog convert(EventLogEntity source, EventLog destination, boolean iterate) {
        if (source == null) {
            return null;
        }

        destination.setId(source.getId());
        destination.setEventType(source.getEventType().getCode());
        destination.setEventTime(source.getCreated());
        destination.setErpCode(source.getErp().getCode());
        destination.setSuccess(source.getSuccess());
        destination.setMessage(source.getMessage());

        return destination;
    }
}
