/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.xroad;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import javax.activation.DataHandler;
import javax.xml.soap.*;
import javax.xml.transform.sax.SAXSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.ConnectException;
import java.net.URL;

public class SoapClient {
    private static final Logger logger = LoggerFactory.getLogger(SoapClient.class);

    public SOAPMessage callSoapWebService(String soapEndpointUrl, RequestHandler requestHandler) {
        try {
            // Create SOAP Connection
            logger.debug("Creating SOAP connection");
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Create SOAP Message
            logger.debug("Creating SOAP message");
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage message = messageFactory.createMessage();

            // set message content from String
            logger.debug("Setting message content from String");
            message.getSOAPPart().setContent(new SAXSource(new InputSource(new StringReader(requestHandler.getRequest()))));

            // Add attachment if the request is a SubmitDataRequest
            if (requestHandler.getDataFile() != null) {
                logger.debug("Attaching file to Submit Data request");
                DataHandler handler = new DataHandler(requestHandler.getDataFile(), "text/xml");
                AttachmentPart attachPart = message.createAttachmentPart(handler);
                attachPart.setContentId("<" + requestHandler.getDataFileName() + ">"); // the brackets are necessary
                attachPart.setMimeHeader("Content-Transfer-Encoding", "8bit");
                message.addAttachmentPart(attachPart);
                message.saveChanges();
            }
            logger.debug("Sending SOAP message");
            logger.trace(soapMessageToString(message));
            SOAPMessage response = soapConnection.call(message, new URL(soapEndpointUrl));
            logger.debug("Response SOAP message");
            logger.trace("{}", response == null ? "NULL" : soapMessageToString(response));

            soapConnection.close();
            logger.debug("SOAP message sent and response received");
            return response;
        } catch (Exception e) {
            Throwable root = e;
            while (root.getCause() != null && root.getCause() != root) {
                root = root.getCause();
            }
            String message = "Error occurred while sending SOAP Request to Server! Make sure you have the correct endpoint URL and SOAPAction!";
            if (root instanceof ConnectException) {
                message += " URL - " + soapEndpointUrl;
            }
            logger.error(message, root);
            requestHandler.setErrorMessage(root.getMessage() + ", endpointUrl - " + soapEndpointUrl);
        }
        logger.debug("Failed to send SOAP message");
        return null;
    }

    public String soapMessageToString(SOAPMessage message) {
        logger.debug("Converting SOAP message to String");
        if (message != null) {
            ByteArrayOutputStream baos = null;
            try {
                baos = new ByteArrayOutputStream();
                message.writeTo(baos);
                return baos.toString();
            } catch (Exception e) {
                logger.debug("Error: could not convert SOAP message to String", e);
            } finally {
                if (baos != null) {
                    try {
                        baos.close();
                    } catch (IOException ioe) {
                        logger.debug("Error: could not convert SOAP message to String", ioe);
                    }
                }
            }
        }
        return null;
    }
}
