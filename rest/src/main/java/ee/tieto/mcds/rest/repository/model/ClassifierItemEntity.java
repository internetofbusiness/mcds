/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "classifier_item", schema = "mcds", catalog = "mcds")
public class ClassifierItemEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_class", nullable = false)
    private ClassifierEntity classifier;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "text")
    private String text;

    @Column(name = "valid_from", nullable = false)
    private LocalDate validFrom;

    @Column(name = "valid_until")
    private LocalDate validUntil;

    @Column(name = "seq_no")
    private Integer seqNo;

    @JoinColumn(name = "id_upit")
    @ManyToOne(fetch = FetchType.LAZY)
    private ClassifierItemEntity parentItem;

    @JoinColumn(name = "id_clve")
    @ManyToOne(fetch = FetchType.LAZY)
    private ClassifierVersionEntity classifierVersion;

    @OneToMany(mappedBy = "classifierItem")
    private List<ClassifierAttributeValueEntity> classifierAttributeValues;

    @OneToMany(mappedBy = "classifierItem1", fetch = FetchType.LAZY)
    private List<ClassifierItemLinkEntity> classifierItemLinks1;

    @OneToMany(mappedBy = "classifierItem2", fetch = FetchType.LAZY)
    private List<ClassifierItemLinkEntity> classifierItemLinks2;

    @OneToMany(mappedBy = "parentItem", fetch = FetchType.LAZY)
    private List<ClassifierItemEntity> childItems;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClassifierEntity getClassifier() {
        return classifier;
    }

    public void setClassifier(ClassifierEntity classifier) {
        this.classifier = classifier;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDate validUntil) {
        this.validUntil = validUntil;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public ClassifierItemEntity getParentItem() {
        return parentItem;
    }

    public void setParentItem(ClassifierItemEntity parentItem) {
        this.parentItem = parentItem;
    }

    public ClassifierVersionEntity getClassifierVersion() {
        return classifierVersion;
    }

    public void setClassifierVersion(ClassifierVersionEntity classifierVersion) {
        this.classifierVersion = classifierVersion;
    }

    public List<ClassifierItemLinkEntity> getClassifierItemLinks1() {
        return classifierItemLinks1;
    }

    public void setClassifierItemLinks1(List<ClassifierItemLinkEntity> classifierItemLinks1) {
        this.classifierItemLinks1 = classifierItemLinks1;
    }

    public List<ClassifierItemLinkEntity> getClassifierItemLinks2() {
        return classifierItemLinks2;
    }

    public void setClassifierItemLinks2(List<ClassifierItemLinkEntity> classifierItemLinks2) {
        this.classifierItemLinks2 = classifierItemLinks2;
    }

    public List<ClassifierAttributeValueEntity> getClassifierAttributeValues() {
        return classifierAttributeValues;
    }

    public void setClassifierAttributeValues(List<ClassifierAttributeValueEntity> classifierItemValues) {
        this.classifierAttributeValues = classifierItemValues;
    }

    public List<ClassifierItemEntity> getChildItems() {
        return childItems;
    }

    public void setChildItems(List<ClassifierItemEntity> childItems) {
        this.childItems = childItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ClassifierItemEntity))
            return false;

        ClassifierItemEntity other = (ClassifierItemEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
