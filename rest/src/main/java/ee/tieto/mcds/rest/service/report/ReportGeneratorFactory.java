/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.service.report.model.ReportMetadata;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ReportGeneratorFactory {

  private final ApplicationContext context;

  public ReportGeneratorFactory(ApplicationContext context) {
    this.context = context;
  }

  public ReportGenerator getGenerator(ReportMetadata metadata) {
    if (metadata.getReportType() == ReportType.STAT_SALARY) {
      ReportGenerator generator = context.getBean(StatSalaryReportGenerator.class);
      return generator;
    }
    throw new McdsSystemException("Unsupported reportType " + metadata.getReportType() + " for report generator");
  }
}
