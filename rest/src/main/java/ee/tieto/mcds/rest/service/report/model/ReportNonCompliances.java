/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportNonCompliances {
  public enum Severity {WARNING, ERROR}

  Map<Severity, List<String>> nonCompliances = new HashMap<>();

  public void add(Severity severity, String problem) {
    List<String> messages = nonCompliances.computeIfAbsent(severity, k -> new ArrayList<>());
    messages.add(problem);
  }

  public boolean haveNonCompliance() {
    return !nonCompliances.isEmpty();
  }

  public boolean haveErrors() {
    return nonCompliances.containsKey(Severity.ERROR);
  }

  public boolean haveWarnings() {
    return this.nonCompliances.containsKey(Severity.WARNING);
  }

  @Override
  public String toString() {
    return this.nonCompliances.toString();
  }
}
