/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;

import java.util.ArrayList;
import java.util.List;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConf extends WebSecurityConfigurerAdapter {
  private static final String AUTHENTICATION_METHOD_DN_HEADER = "DN_HEADER";
  private static final String AUTHENTICATION_METHOD_X509 = "X509";
  private static final String DN_PRINCIPAL_REGEX = "CN=(.*?)(?:,|$)";


  @Value("${mcds.debug:false}")
  private boolean debugEnabled;
  @Value("${mcds.authentication.method}") // DN_HEADER or X509
  private String authenticationMethod;

  @Value("${mcds.authentication.header}")
  private String dnAuthetnticationHeaderName;
  @Value("${mcds.authentication.debug-header}")
  private String debugAuthenticationHeaderName;


  @Autowired
  private AuthorizationUserDetailsService userDetailService;


  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .formLogin().disable()
        .httpBasic().disable()
        .logout().disable()
        .authorizeRequests().anyRequest().authenticated()
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and();
    if (AUTHENTICATION_METHOD_DN_HEADER.equals(authenticationMethod)) {
      http.addFilterBefore(getRequestHeaderAuthenticationDnHeaderFilter(), X509AuthenticationFilter.class);
    } else if (AUTHENTICATION_METHOD_X509.equals(authenticationMethod)) {
      http.x509()
          .subjectPrincipalRegex(DN_PRINCIPAL_REGEX)
          .authenticationUserDetailsService(userDetailService);
    }
    if (debugEnabled && debugAuthenticationHeaderName != null) {
      if (AUTHENTICATION_METHOD_DN_HEADER.equals(authenticationMethod)) {
        http.addFilterBefore(getRequestHeaderAuthenticationFilter(), RequestHeaderAuthenticationDnHeaderFilter.class);
      } else {
        http.addFilterBefore(getRequestHeaderAuthenticationFilter(), X509AuthenticationFilter.class);
      }
    }
  }

  @Bean
  @ConditionalOnProperty(value = "mcds.authentication.method", havingValue = AUTHENTICATION_METHOD_DN_HEADER)
  public RequestHeaderAuthenticationDnHeaderFilter getRequestHeaderAuthenticationDnHeaderFilter() {
    RequestHeaderAuthenticationDnHeaderFilter filter = new RequestHeaderAuthenticationDnHeaderFilter();
    filter.setPrincipalRequestHeader(this.dnAuthetnticationHeaderName);
    filter.setSubjectDnRegex(DN_PRINCIPAL_REGEX);
    filter.setExceptionIfHeaderMissing(false);
    filter.setAuthenticationManager(authenticationManager());
    return filter;
  }

  @Bean
  @ConditionalOnProperty("mcds.authentication.debug-header")
  public RequestHeaderAuthenticationFilter getRequestHeaderAuthenticationFilter() {
    RequestHeaderAuthenticationFilter filter = new RequestHeaderAuthenticationFilter();
    filter.setPrincipalRequestHeader(debugAuthenticationHeaderName);
    filter.setExceptionIfHeaderMissing(false);
    filter.setAuthenticationManager(authenticationManager());
    return filter;
  }

  @Bean
  @Override
  protected AuthenticationManager authenticationManager() {
    final List<AuthenticationProvider> providers = new ArrayList<>(1);
    providers.add(preAuthAuthProvider());
    return new ProviderManager(providers);
  }

  @Bean(name = "preAuthProvider")
  protected PreAuthenticatedAuthenticationProvider preAuthAuthProvider() {
    PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    provider.setPreAuthenticatedUserDetailsService(userDetailService);
    return provider;
  }

}
