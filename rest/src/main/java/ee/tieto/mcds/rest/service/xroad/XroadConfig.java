/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.xroad;

public class XroadConfig {

//MEMBER : ee-dev : COM : 10137025 ja SUBSYSTEM : ee-dev : COM : 10137025 : MCDS
  private String userId = "EE11111111111";
  private String securityServerUrl = "http://localhost:30888";
  private String protocolVersion = "4.0";
  private String memberClass = "COM"; //ServicememberClass=GOV
  private String membermemberCode = "10137025"; // =70000332
  private String subsystem = "MCDS"; //MembersubsystemCode=sa-client
  private String xroadInstance = "ee-dev"; //ee-test
}
