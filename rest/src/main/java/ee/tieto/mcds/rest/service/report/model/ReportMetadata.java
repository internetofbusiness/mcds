/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report.model;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.repository.model.ClassifierAttributeValueEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.service.report.ReportPeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Optional;


public abstract class ReportMetadata {
  private static final Logger log = LoggerFactory.getLogger(ReportMetadata.class);

  //Rrport period - enum ReportPeriod
  private static final String ATTRIBUTE_CODE_PERIOD = "REP_PERIOD";
  //Report start shift in days by calendar - Integer
  private static final String ATTRIBUTE_CODE_STAR_SHIFT = "REP_PERIOD_START_DAY";
  // Report submission day
  private static final String ATTRIBUTE_SUBMISSION_DAY = "SUBMISSION_DAY";
  // Report generationn day in next period - shift after next period start
  private static final String ATTRIBUTE_GEN_LAG = "REP_GEN_LAG";

  private ReportType reportType;
  private String itemVersion;

  private ReportPeriod period;
  private Integer periodStartShift;
  // 11.	DAY_NUMBER_IN_PERIOD in UC-019
  private Integer submissionDay;
  private Integer generationLag;

  private LocalDate periodStart;
  private LocalDate periodEnd;
  private LocalDate generationDate;
  private LocalDate submissionDate;


  public ReportMetadata(ClassifierItemEntity clItem) {
    setReportType(ReportType.valueOf(clItem.getCode()));
    setItemVersion(clItem.getClassifierVersion().getCode());
    clItem.getClassifierAttributeValues().forEach(this::initByAttributes);

    LocalDate now = OffsetDateTime.now().toLocalDate();
    periodStart = period.getStartOfPreviousPeriod(now);
    periodEnd = period.getEndOfPreviousPeriod(now);
    generationDate = period.getStartOfCurrentPeriod(now).plusDays(generationLag);
    submissionDate = period.getStartOfCurrentPeriod(now).plusDays(submissionDay);
  }
  public void setOtherPeriod(LocalDate dateInPeriod, LocalDate generateAndSubmit) {
    if (isInCurrenPeriod(dateInPeriod)) {
      return;
    }
    initTimes(dateInPeriod, true);
  }

  private boolean isInCurrenPeriod(LocalDate date) {
    return this.periodStart.equals(date)
        || this.periodEnd.equals(date)
        || this.periodStart.isBefore(date) && this.periodEnd.isAfter(date);
  }

  private void initTimes(LocalDate dateInPeriod, boolean setAll) {
    periodStart = period.getStartOfCurrentPeriod(dateInPeriod);
    periodEnd = period.getEndOfCurrentPeriod(dateInPeriod);
    if (setAll) {
      generationDate = period.getStartOfCurrentPeriod(dateInPeriod).plusDays(generationLag);
      submissionDate = period.getStartOfCurrentPeriod(dateInPeriod).plusDays(submissionDay);
    }
  }

  public static Optional<ReportMetadata> fromEntity(ClassifierItemEntity clItem) {
    try {
      ReportType repType = ReportType.valueOf(clItem.getCode());
      if (repType == ReportType.STAT_SALARY) {
        return StatSalaryReportMetadata.fromEntity(clItem);
      } else {
        log.error("{} not implemented", repType.name());
        return Optional.empty();
      }
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  private void initByAttributes(ClassifierAttributeValueEntity attributeValue) {
    String code = attributeValue.getClassifierAttribute().getCode();
    String value = attributeValue.getValue();
    if (ATTRIBUTE_CODE_PERIOD.equals(code)) {
      setPeriod(ReportPeriod.valueOf(value));
    } else if (ATTRIBUTE_CODE_STAR_SHIFT.equals(code)) {
      setPeriodStartShift(Integer.valueOf(value));
    } else if (ATTRIBUTE_GEN_LAG.equals(code)) {
      setGenerationLag(Integer.valueOf(value));
    } else if (ATTRIBUTE_SUBMISSION_DAY.equals(code)) {
      setSubmissionDay(Integer.valueOf(value));
    }
  }

  public boolean isMustGenerate() {
    LocalDate now = OffsetDateTime.now().toLocalDate();
    LocalDate start = period.getStartOfCurrentPeriod(now);
    start = start.plusDays(this.generationLag);
    if (now.isBefore(start)) {
      return false;
    }
    return true;
  }

  public LocalDate getFromDate() {
    return this.periodStart;
  }

  public LocalDate getUntilDate() {
    return this.periodEnd;
  }

  public ReportType getReportType() {
    return reportType;
  }

  public void setReportType(ReportType reportType) {
    this.reportType = reportType;
  }

  public String getItemVersion() {
    return itemVersion;
  }

  public void setItemVersion(String itemVersion) {
    this.itemVersion = itemVersion;
  }

  public ReportPeriod getPeriod() {
    return period;
  }

  public void setPeriod(ReportPeriod period) {
    this.period = period;
  }

  public Integer getPeriodStartShift() {
    return periodStartShift;
  }

  public void setPeriodStartShift(Integer periodStartShift) {
    this.periodStartShift = periodStartShift;
  }

  public Integer getSubmissionDay() {
    return submissionDay;
  }

  public void setSubmissionDay(Integer submissionDay) {
    this.submissionDay = submissionDay;
  }

  public LocalDate getSubmissionDate() {
    return this.submissionDate;
  }

  public Integer getGenerationLag() {
    return generationLag;
  }

  public void setGenerationLag(Integer generationLag) {
    this.generationLag = generationLag;
  }

  public LocalDate getPeriodStart() {
    return periodStart;
  }

  public void setPeriodStart(LocalDate periodStart) {
    this.periodStart = periodStart;
  }

  public LocalDate getPeriodEnd() {
    return periodEnd;
  }

  public void setPeriodEnd(LocalDate periodEnd) {
    this.periodEnd = periodEnd;
  }

  public LocalDate getGenerationDate() {
    return generationDate;
  }

  public void setGenerationDate(LocalDate generationDate) {
    this.generationDate = generationDate;
  }

}
