/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.ClassifierAttribute;
import ee.tieto.mcds.rest.model.ClassifierItem;
import ee.tieto.mcds.rest.model.ClassifierItemLink;
import ee.tieto.mcds.rest.repository.model.ClassifierAttributeValueEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemLinkEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EntityToRestClassifierItem extends Converter<ClassifierItemEntity, ClassifierItem> {

  private final Converter<ClassifierAttributeValueEntity, ClassifierAttribute> attributeConverter;

  private final Converter<ClassifierItemLinkEntity, ClassifierItemLink> linkConverter;

  @Autowired
  public EntityToRestClassifierItem(
      Converter<ClassifierAttributeValueEntity, ClassifierAttribute> attributeConverter,
      Converter<ClassifierItemLinkEntity, ClassifierItemLink> linkConverter) {
    this.attributeConverter = attributeConverter;
    this.linkConverter = linkConverter;
  }

  @Override
  public ClassifierItem convert(ClassifierItemEntity source, ClassifierItem destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setCode(source.getCode());
    destination.setName(source.getText());
    destination.setSeqNr(source.getSeqNo());
    destination.setValidFrom(source.getValidFrom());
    destination.setValidUntil(source.getValidUntil());
    ClassifierItemEntity upper = source.getParentItem();
    String upperCode = upper == null ? null : upper.getCode();
    destination.setUpperClassifierItemCode(upperCode);

    if (iterate) {
      destination.setClassifierAttributes(attributeConverter.convert(source.getClassifierAttributeValues(), true));
      List<ClassifierItemLinkEntity> entityLinks = new ArrayList<>();
      entityLinks.addAll(source.getClassifierItemLinks1());
      entityLinks.addAll(source.getClassifierItemLinks2());
      List<ClassifierItemLink> links = linkConverter.convert(entityLinks, true);
      if (links != null) {
        destination.setLinks(links);
      }
    }

    return destination;
  }

}
