/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.ClassifierAttribute;
import ee.tieto.mcds.rest.repository.model.ClassifierAttributeValueEntity;
import org.springframework.stereotype.Component;

@Component
public class EntityToRestClassifierAttribute extends Converter<ClassifierAttributeValueEntity, ClassifierAttribute> {

  @Override
  public ClassifierAttribute convert(ClassifierAttributeValueEntity source,
                                     ClassifierAttribute destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setCode(source.getClassifierAttribute().getCode());
    destination.setName(source.getClassifierAttribute().getName());
    destination.setFormat(source.getClassifierAttribute().getFormat());
    destination.setValue(source.getValue());

    return destination;
  }

}
