/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequestHeaderAuthenticationDnHeaderFilter extends RequestHeaderAuthenticationFilter{
  private static final Logger log = LoggerFactory.getLogger(RequestHeaderAuthenticationDnHeaderFilter.class);

  private Pattern subjectDnPattern;
  private String principalRequestHeader = "ssl_client_s_dn";
  private boolean exceptionIfHeaderMissing = true;


  protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
    String dn = request.getHeader(principalRequestHeader);

    if (dn == null) {
      if (exceptionIfHeaderMissing) {
        throw new PreAuthenticatedCredentialsNotFoundException(principalRequestHeader + " header not found in request.");
      }
      return null;
    }

    return extractPrincipal(dn);
  }


  /**
   * Sets the regular expression which will by used to extract the user name from the
   * certificate's Subject DN.
   * <p>
   * It should contain a single group; for example the default expression
   * "CN=(.*?)(?:,|$)" matches the common name field. So "CN=Jimi Hendrix, OU=..." will
   * give a user name of "Jimi Hendrix".
   * <p>
   * The matches are case insensitive. So "emailAddress=(.?)," will match
   * "EMAILADDRESS=jimi@hendrix.org, CN=..." giving a user name "jimi@hendrix.org"
   *
   * @param subjectDnRegex the regular expression to find in the subject
   */
  public void setSubjectDnRegex(String subjectDnRegex) {
    Assert.hasText(subjectDnRegex, "Regular expression may not be null or empty");
    subjectDnPattern = Pattern.compile(subjectDnRegex, Pattern.CASE_INSENSITIVE);
  }

  public void setPrincipalRequestHeader(String name) {
    this.principalRequestHeader = name;
  }

  public void setExceptionIfHeaderMissing(boolean exceptionOnMissing) {
    this.exceptionIfHeaderMissing = exceptionOnMissing;
  }

  private Object extractPrincipal(String subjectDN) {
    log.debug("Subject DN is '{}'", subjectDN);

    Matcher matcher = subjectDnPattern.matcher(subjectDN);

    if (!matcher.find()) {
      throw new BadCredentialsException(
          "RequestHeaderAuthenticationDnHeaderFilter.noMatching" + subjectDN +
          "No matching pattern was found in subject DN: {0}");
    }

    if (matcher.groupCount() != 1) {
      throw new IllegalArgumentException(
          "Regular expression must contain a single group ");
    }

    String username = matcher.group(1);

    log.debug("Extracted Principal name is '{}'", username);

    return username;
  }



}
