/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report.model;

public class SenderResponse {

  private String sessionId;
  private String message;
  private String errorMessage;
  private Fault fault;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getErrorMessage() {
    if (this.fault != null) {
      StringBuilder sb = new StringBuilder();
      if (this.fault.getCode() != null) {
        sb.append("Code:").append(this.fault.getCode()).append(" ");
      }
      sb.append("message: ").append(fault.getDescription());
      return sb.toString();
    }
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public Fault getFault() {
    return fault;
  }

  public void setFault(Fault fault) {
    this.fault = fault;
  }

  public void setFault(String code, String message) {
    this.fault = new Fault();
    this.fault.setCode(code);
    this.fault.setDescription(message);
  }

  public boolean isError() {
    return this.fault != null || this.errorMessage != null;
  }

  @Override
  public String toString() {
    return "SenderResponse{" +
        "sessionId='" + sessionId + '\'' +
        ", message='" + message + '\'' +
        ", errorMessage='" + errorMessage + '\'' +
        ", fault=" + fault +
        '}';
  }

  public static class Fault {
    private String code;
    private String description;

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    @Override
    public String toString() {
      return "Fault{" +
          "code='" + code + '\'' +
          ", description='" + description + '\'' +
          '}';
    }
  }
}
