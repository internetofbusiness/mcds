/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

public class ClassifierItemLink {

  private String linkedClassifierCode;
  private String linkedClassifierItemCode;
  private String linkType;
  private String description;

  public String getLinkedClassifierCode() {
    return linkedClassifierCode;
  }

  public void setLinkedClassifierCode(String linkedClassifierCode) {
    this.linkedClassifierCode = linkedClassifierCode;
  }

  public String getLinkedClassifierItemCode() {
    return linkedClassifierItemCode;
  }

  public void setLinkedClassifierItemCode(String linkedClassifierItemCode) {
    this.linkedClassifierItemCode = linkedClassifierItemCode;
  }

  public String getLinkType() {
    return linkType;
  }

  public void setLinkType(String linkType) {
    this.linkType = linkType;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
