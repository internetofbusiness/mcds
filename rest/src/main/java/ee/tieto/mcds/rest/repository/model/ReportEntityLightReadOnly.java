/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import org.springframework.data.annotation.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@Immutable
@Entity
@Table(name = "report", schema = "mcds", catalog = "mcds")
public class ReportEntityLightReadOnly {

  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "report_uuid", nullable = false)
  private String reportUuid;

  @JoinColumn(name = "id_orga", nullable = false)
  @ManyToOne(fetch = FetchType.EAGER)
  private OrganizationEntity organization;

  @JoinColumn(name = "report_type", nullable = false)
  @ManyToOne(fetch = FetchType.EAGER)
  private ClassifierItemEntity reportType;

  @Column(name = "deadline", nullable = false)
  private LocalDate deadline;

  @Column(name = "period_from")
  private LocalDate periodFrom;

  @Column(name = "period_until")
  private LocalDate periodUntil;

  @Column(name = "external_id")
  private String externalId;

  @Column(name = "created", nullable = false)
  private OffsetDateTime created;

  @OneToMany(mappedBy = "report", fetch = FetchType.LAZY )
  private List<ReportEntryEntity> reportEntryes;

  @OneToMany(mappedBy = "report", fetch = FetchType.LAZY)
  @OrderBy("created DESC")
  private List<ReportStatusEntity> reportStatuses;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getReportUuid() {
    return reportUuid;
  }

  public void setReportUuid(String reportUuid) {
    this.reportUuid = reportUuid;
  }

  public OrganizationEntity getOrganization() {
    return organization;
  }

  public void setOrganization(OrganizationEntity organization) {
    this.organization = organization;
  }

  public ClassifierItemEntity getReportType() {
    return reportType;
  }

  public void setReportType(ClassifierItemEntity reportType) {
    this.reportType = reportType;
  }

  public LocalDate getDeadline() {
    return deadline;
  }

  public LocalDate getPeriodFrom() {
    return periodFrom;
  }

  public void setPeriodFrom(LocalDate periodFrom) {
    this.periodFrom = periodFrom;
  }

  public LocalDate getPeriodUntil() {
    return periodUntil;
  }

  public void setPeriodUntil(LocalDate periodUntil) {
    this.periodUntil = periodUntil;
  }

  public void setDeadline(LocalDate deadline) {
    this.deadline = deadline;
  }

  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  public OffsetDateTime getCreated() {
    return created;
  }

  public void setCreated(OffsetDateTime created) {
    this.created = created;
  }

  public List<ReportEntryEntity> getReportEntryes() {
    return reportEntryes;
  }

  public void setReportEntryes(List<ReportEntryEntity> reportEntryes) {
    this.reportEntryes = reportEntryes;
  }

  public List<ReportStatusEntity> getReportStatuses() {
    return reportStatuses;
  }

  public void setReportStatuses(List<ReportStatusEntity> reportStatuses) {
    this.reportStatuses = reportStatuses;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ReportEntity)) {
      return false;
    }

    ReportEntity other = (ReportEntity) o;
    return id != null && id.equals(other.getId());
  }

  @Override
  public int hashCode() {
    return 31;
  }

}
