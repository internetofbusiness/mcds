/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.constants;

public enum EventType {
  ORGANIZATION_SAVE("organization"),    // Add organization
  AUTHORIZATION_ADD("permission"),      // Add permission
  AUTHORIZATION_DELETE("permission"),   // Disable permission
  REPORT_PREVIEW_VIEW("report"),        // View report content
  REPORT_GENERATE("report"),            // Report generate
  REPORT_CANCEL("report"),              // Cancel report
  REPORT_SUBMIT("report"),              // Send report to receiver
  ENTRY_VIEW("accounting_entry"),       // Get entries
  ENTRY_ADD("accounting_entry"),        // Prepare entries from package xbrl
  XBRL_SAVE("package"),                 // XBRL package save
  XBRL_VALIDATE("package"),             // XBRL validation
  INVOICE_FEEDBACK_SAVE("invoice"),     // Save invoice feedback
  SCHEDULER_EXECUTED("");               // System scheduled task executed

  private String table;

  EventType(String table) {
    this.table = table;
  }

  public String getTable() {
    return this.table;
  }
}
