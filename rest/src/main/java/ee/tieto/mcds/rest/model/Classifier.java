/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

import java.util.List;

public class Classifier {
  private String code;
  private String name;
  private String description;
  private String ownerRegNr;
  private List<ClassifierVersion> versions;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getOwnerRegNr() {
    return ownerRegNr;
  }

  public void setOwnerRegNr(String ownerRegNr) {
    this.ownerRegNr = ownerRegNr;
  }

  public List<ClassifierVersion> getVersions() {
    return versions;
  }

  public void setVersions(List<ClassifierVersion> versions) {
    this.versions = versions;
  }
}
