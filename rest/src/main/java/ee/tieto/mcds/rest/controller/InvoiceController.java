/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.InvoiceFeedback;
import ee.tieto.mcds.rest.model.PackageStatus;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.service.InvoiceService;
import ee.tieto.mcds.rest.service.XbrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/invoice")
@Validated
public class InvoiceController {

  private final InvoiceService invoiceService;
  private final XbrlService xbrlService;
  private final EventLogService eventService;

  @Autowired
  public InvoiceController(InvoiceService invoiceService, XbrlService xbrlService, EventLogService eventService) {
    this.invoiceService = invoiceService;
    this.xbrlService = xbrlService;
    this.eventService = eventService;
  }

  // Kirjendi ja selle valideerimise tulemuste väljastamine UC-003
  @GetMapping(value = "/entries/status", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_INVOICE_OPERATOR')")
  public List<PackageStatus> invoiceEntryStatus(
      @RequestParam(name = "sessionId", required = false) UUID sessionId,
      @RequestParam(name = "receivedFrom", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate receivedFrom,
      @RequestParam(name = "receivedUntil", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate receivedUntil,
      @RequestParam(name = "entryIdentifier", required = false) String entryIdentifier) {
    try {
      return xbrlService.getInvoiceEntriesStatus(sessionId, entryIdentifier, receivedFrom, receivedUntil);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @GetMapping(value = "/entries", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_INVOICE_OPERATOR')")
  public List<String> getInvoiceEntry(@RequestParam(value = "regNr", required = false) String organizationIdentifier,
                                 @RequestParam(value = "fromDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                 @RequestParam(value = "untilDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate untilDate,
                                 @RequestParam(value = "entriesType", required = false) String entriesType,
                                 @RequestParam(value = "entryIdentifier", required = false) Set<String> entryIdentifiers) {

    try {
      List<String> entries = xbrlService.getInvoiceEntryes(organizationIdentifier, null, fromDate,
          untilDate, entriesType, entryIdentifiers);
      return entries;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }


  @PostMapping(value = "/feedback")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_INVOICE_OPERATOR')")
  public ResponseEntity<Void> saveInvoiceFeedback(
          @Valid @RequestBody List<InvoiceFeedback> fb) {
    try {
      invoiceService.saveFeedback(fb);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }
}
