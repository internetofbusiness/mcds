/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.model.InvoiceFeedback;
import ee.tieto.mcds.rest.repository.*;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.model.*;
import ee.tieto.mcds.rest.service.AbstractService;
import ee.tieto.mcds.rest.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class InvoiceServiceImpl extends AbstractService implements InvoiceService {

  private final InvoiceRepository invRepo;
  private final PermissionRepository permRepo;
  private final ClassifierItemRepository clItemRepo;
  private final AccountingEntryRepositoryLight accountingEntryRepository;

  @Autowired
  public InvoiceServiceImpl(InvoiceRepository invRepo,
                            PermissionRepository permRepo,
                            ClassifierItemRepository clItemRepo,
                            AccountingEntryRepositoryLight accountingEntryRepository) {
    this.invRepo = invRepo;
    this.permRepo = permRepo;
    this.clItemRepo = clItemRepo;
    this.accountingEntryRepository = accountingEntryRepository;
  }

  @Override
  @Transactional
  public void saveFeedback(List<InvoiceFeedback> fb) {
    List<String> invoiceIdList = new ArrayList<>(fb.size());
    try {
      for (InvoiceFeedback i : fb) {
        AccountingEntryEntityLightReadOnly entry = accountingEntryRepository.findByEntryUuid(i.getEntryIdentifier());
        if (entry == null) {
          throw new McdsValidationException(String.format("Accounting entry with identifier %s not found", i.getEntryIdentifier()));
        }
        InvoiceEntity invoice = entry.getInvoice();
        if (invoice == null) {
          invoice = new InvoiceEntity();
          invoice.setAccountingEntry(entry);
          invoice.setInvoice(i.getXml());
          invoice.setGenerationTime(i.getGenerationTime());
          invoice.setInvoiceStatuses(new ArrayList<>());
          invRepo.saveAndFlush(invoice);
        }
        invoiceIdList.add(invoice.getId().toString());
        InvoiceStatusEntity invoiceStatus = new InvoiceStatusEntity();
        invoiceStatus.setInvoice(invoice);
        invoiceStatus.setStatus(i.getStatus());
        invoiceStatus.setMessage(i.getComment());
        invoice.getInvoiceStatuses().add(invoiceStatus);
        invRepo.save(invoice);
      }
      eventService.newEvent(
          EventType.INVOICE_FEEDBACK_SAVE,
          getUser(),
          EventType.INVOICE_FEEDBACK_SAVE.getTable() + ":" + invoiceIdList.toString(),
          true, null);
    } catch (Exception e) {
      eventService.newEventNewTransaction(
          EventType.INVOICE_FEEDBACK_SAVE,
          getUser(),
          EventType.INVOICE_FEEDBACK_SAVE.getTable() + ":null",
          false, e.getMessage());
      throw e;
    }
  }
}
