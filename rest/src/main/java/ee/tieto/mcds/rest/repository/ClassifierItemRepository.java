/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository;

import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ClassifierItemRepository extends JpaRepository<ClassifierItemEntity, Long> {


  //TODO Version not handled. The versioned classifier may contain more than one item with same code
  @Query("select i from ClassifierItemEntity i where " +
      "i.code = :itemCode and i.classifier.code = :classifierCode")
  Optional<ClassifierItemEntity> findByClassifierCodeAndItemCode(
      @Param("classifierCode") String classifierCode,
      @Param("itemCode") String itemCode);

  @Query("select i from ClassifierItemEntity i where " +
      "i.code = :itemCode and i.classifier.code = :classifierCode\n" +
      "and (i.validFrom is null or i.validFrom <= :date) " +
      "and (i.validUntil is null or i.validUntil > :date)")
  Optional<ClassifierItemEntity> findByClassifierCodeAndItemCode(
      @Param("classifierCode") String classifierCode,
      @Param("itemCode") String itemCode,
      @Param("date") LocalDate date);

  @Query("select i from ClassifierEntity c join c.classifierItems i where c.code = :cCode")
  List<ClassifierItemEntity> findClassifierItemsByClassifierCode(
      @Param("cCode") String code);

  @Query("select i from ClassifierEntity c join c.classifierItems i where c.code = :classifierCode and i.code = :classifierItemCode")
  List<ClassifierItemEntity> findClassifierItemByCodes(
      @Param("classifierCode") String classifierCode,
      @Param("classifierItemCode") String classifierItemCode);

  @Query("select i from " +
      "ClassifierEntity c " +
      "inner join c.classifierVersions v " +
      "inner join c.classifierItems i " +
      "where " +
      "c.code=:classifierCode and v.code=:versionCode " +
      "and v.valid=true " +
      "and ((v.validFrom is null or v.validFrom <= :date) and (v.validUntil is null or v.validUntil > :date)) " +
      "and ((i.validFrom is null or i.validFrom <= :date) and (i.validUntil is null or i.validUntil > :date))")
  List<ClassifierItemEntity> findByClassifierCodeAndVersionCodeAndDate(
      @Param("versionCode") String versionCode,
      @Param("classifierCode") String classifierCode,
      @Param("date") LocalDate date);
}
