/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.config;

import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.ErpClAttributes;
import ee.tieto.mcds.rest.repository.model.ClassifierAttributeValueEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class AuthorizationUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

  @Value("${mcds.manager-name}")
  private String managerName;

  private final ClassifierItemRepository clRepo;

  public AuthorizationUserDetailsService(ClassifierItemRepository clRepo) {
    this.clRepo = clRepo;
  }

  @Override
  @Transactional
  public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
    final String principal = (String)token.getPrincipal();

    ClassifierItemEntity erp =
        clRepo.findByClassifierCodeAndItemCode(Classifier.ERP.name(), principal, LocalDate.now()).orElse(null);
    if (erp == null) {
      throw new UsernameNotFoundException("User " + principal + " not found");
    }

    List<String> roles = new ArrayList<>();
    roles.add("ROLE_USER");


    if (principal.equals(managerName)) {
      roles.add("ROLE_MANAGER");
    }

    //Is user e-invoice operator
    ClassifierAttributeValueEntity attrValue = erp.getClassifierAttributeValues().stream()
        .filter(av -> av.getClassifierAttribute().getCode().equals(ErpClAttributes.E_INVOICE_OPERATOR.name())).findFirst().orElse(null);
    if (attrValue != null && ("TRUE".equals(attrValue.getValue()) || "true".equals(attrValue.getValue()))) {
      roles.add("ROLE_INVOICE_OPERATOR");
    }

    String[] rolesaArray = new String[roles.size()];
    roles.toArray(rolesaArray);
    return new User(principal, "", AuthorityUtils.createAuthorityList(rolesaArray));
  }
}
