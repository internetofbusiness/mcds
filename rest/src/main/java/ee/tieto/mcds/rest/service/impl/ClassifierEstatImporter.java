/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.StatClassifier;
import ee.tieto.mcds.rest.model.StatClassifierElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class ClassifierEstatImporter {

  @Value("${mcds.estat-classifiers.classifiers-url}")
  private String classifiersUrl;

  @Value("${mcds.estat-classifiers.elements-url}")
  private String elementsUrl;

  public List<StatClassifier> importClassifiers() {
    try {
      String content = requestData(classifiersUrl);
      return new ObjectMapper().readValue(content, new TypeReference<>(){});
    } catch (Exception e) {
      throw new McdsSystemException(e.getMessage(), e);
    }
  }

  public List<StatClassifierElement> importClassifierElement(String classifier, LocalDate date) {
    try {
      String url = elementsUrl + classifier + "/";
      if (date != null) {
        url += date.format(DateTimeFormatter.ISO_DATE);
      }
      String data = requestData(url);
      return new ObjectMapper().readValue(data, new TypeReference<>() {});
    } catch (Exception e) {
      throw new McdsSystemException(e.getMessage(), e);
    }
  }

  private String requestData(String urlString) {
    try {
      URL url = new URL(urlString);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");

      BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
      String line;
      StringBuilder sb = new StringBuilder();
      do {
        line = reader.readLine();
        sb.append(line);
      } while (line != null);

      reader.close();
      con.disconnect();
      return sb.toString();
    } catch (Exception e) {
      throw new McdsSystemException(e.getMessage(), e);
    }
  }

}
