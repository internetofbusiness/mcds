/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.utils.DateTimeTools;

import java.time.LocalDate;

public enum ReportPeriod {
  CONTINUOUS, DAY, WEEK, MONTH, QUARTER, HALF_YEAR, YEAR;

  public LocalDate getStartOfCurrentPeriod(LocalDate current) {
    switch(this) {
      case CONTINUOUS:
      case DAY:
        return DateTimeTools.startOfDay(current, 0);
      case WEEK:
        return DateTimeTools.startOfWeek(current, 0);
      case MONTH:
        return DateTimeTools.startOfMonth(current, 0);
      case QUARTER:
        return DateTimeTools.startOfQuarter(current, 0);
      case HALF_YEAR:
        return DateTimeTools.startOfHalfYear(current, 0);
      case YEAR:
        return DateTimeTools.startOfYear(current, 0);
    }
    return null;
  }

  public LocalDate getEndOfCurrentPeriod(LocalDate current) {
    switch(this) {
      case CONTINUOUS:
      case DAY:
        return DateTimeTools.endOfDay(current, 0);
      case WEEK:
        return DateTimeTools.endOfWeek(current, 0);
      case MONTH:
        return DateTimeTools.endOfMonth(current, 0);
      case QUARTER:
        return DateTimeTools.endOfQuarter(current, 0);
      case HALF_YEAR:
        return DateTimeTools.endOfHalfYear(current, 0);
      case YEAR:
        return DateTimeTools.endOfYear(current, 0);
    }
    return null;
  }

  public LocalDate getStartOfPreviousPeriod(LocalDate current) {
    switch(this) {
      case CONTINUOUS:
      case DAY:
        return DateTimeTools.startOfDay(current, -1);
      case WEEK:
        return DateTimeTools.startOfWeek(current, -1);
      case MONTH:
        return DateTimeTools.startOfMonth(current, -1);
      case QUARTER:
        return DateTimeTools.startOfQuarter(current, -1);
      case HALF_YEAR:
        return DateTimeTools.startOfHalfYear(current, -1);
      case YEAR:
        return DateTimeTools.startOfYear(current, -1);
    }
    return null;
  }

  public LocalDate getEndOfPreviousPeriod(LocalDate current) {
    switch(this) {
      case CONTINUOUS:
      case DAY:
        return DateTimeTools.endOfDay(current, -1);
      case WEEK:
        return DateTimeTools.endOfWeek(current, -1);
      case MONTH:
        return DateTimeTools.endOfMonth(current, -1);
      case QUARTER:
        return DateTimeTools.endOfQuarter(current, -1);
      case HALF_YEAR:
        return DateTimeTools.endOfHalfYear(current, -1);
      case YEAR:
        return DateTimeTools.endOfYear(current, -1);
    }
    return null;
  }

}
