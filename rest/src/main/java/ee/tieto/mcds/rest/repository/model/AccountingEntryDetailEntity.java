/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Objects;

@Entity
@Table(name = "accounting_entry_detail", schema = "mcds", catalog = "mcds")
public class AccountingEntryDetailEntity {

  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Basic
  @Column(name = "line_number", nullable = true, length = -1)
  private String lineNumber;

  @Basic
  @Column(name = "posting_date", nullable = true)
  private LocalDate postingDate;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="id_acen")
  private AccountingEntryEntity accountingEntry;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLineNumber() {
    return lineNumber;
  }

  public void setLineNumber(String lineNumber) {
    this.lineNumber = lineNumber;
  }

  public LocalDate getPostingDate() {
    return postingDate;
  }

  public void setPostingDate(LocalDate postingDate) {
    this.postingDate = postingDate;
  }

  public AccountingEntryEntity getAccountingEntry() {
    return accountingEntry;
  }

  public void setAccountingEntry(AccountingEntryEntity accountingEntry) {
    this.accountingEntry = accountingEntry;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountingEntryDetailEntity that = (AccountingEntryDetailEntity) o;
    return Objects.equals(id, that.id) && Objects.equals(lineNumber, that.lineNumber) && Objects.equals(postingDate, that.postingDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, lineNumber, postingDate);
  }
}
