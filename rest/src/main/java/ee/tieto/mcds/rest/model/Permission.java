/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Permission {

  @NotBlank(message = "erpCode required")
  private String erpCode;
  private String regNr;
  private String businessArea;
  private String reportType;
  @NotNull(message = "active required")
  private Boolean active;

  public String getErpCode() {
    return erpCode;
  }

  public void setErpCode(String erpCode) {
    this.erpCode = erpCode;
  }

  public String getRegNr() {
    return regNr;
  }

  public void setRegNr(String regNr) {
    this.regNr = regNr;
  }

  public String getBusinessArea() {
    return businessArea;
  }

  public void setBusinessArea(String businessArea) {
    this.businessArea = businessArea;
  }

  public String getReportType() {
    return reportType;
  }

  public void setReportType(String reportType) {
    this.reportType = reportType;
  }

  public Boolean isActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }
}
