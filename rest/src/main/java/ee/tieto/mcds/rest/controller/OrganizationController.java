/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.rest.McdsNotFoundException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.Organization;
import ee.tieto.mcds.rest.service.OrganizationSerice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/manage")
@Validated
public class OrganizationController {

  private final OrganizationSerice service;

  @Autowired
  public OrganizationController(OrganizationSerice service) {
    this.service = service;
  }

  @PostMapping("/clients")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public ResponseEntity<Void> putClient(@Valid @RequestBody List<Organization> organizations) {
    try {
      service.save(organizations);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @GetMapping(value = "/client/{regNr}", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public Organization getClient(@PathVariable("regNr") String regNr) {
    try {

      List<Organization> organizations = service.findClients(regNr);
      if (organizations.isEmpty()) {
        throw new McdsNotFoundException("Organization '" + regNr + "' not found");
      }
      return organizations.get(0);

    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  @GetMapping(value = "/clients", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public List<Organization> getClients() {
    try {
      return service.findClients(null);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

}
