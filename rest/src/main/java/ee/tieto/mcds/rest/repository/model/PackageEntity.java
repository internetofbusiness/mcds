/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import ee.tieto.mcds.rest.repository.constants.PackageStatus;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "package", schema = "mcds", catalog = "mcds")
public class PackageEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "package_uuid", nullable = false)
    @Type(type="pg-uuid")
    private UUID packageUuid;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "erp")
    private ClassifierItemEntity erp;

    @Column(name = "xbrlgl")
    private String xbrlgl;

    @Column(name = "valid", nullable = false)
    private Boolean valid;

    @Column(name = "created", nullable = false)
    private OffsetDateTime created;

    @Column(name = "xbrl_gl_version")
    private String xbrlGlVersion;

    @OneToMany(mappedBy = "pacage", fetch = FetchType.LAZY)
    private List<AccountingEntryEntity> accountingEntryes;

    @OneToMany(mappedBy = "pacage", fetch = FetchType.LAZY)
    @OrderBy("id DESC")
    private List<PackageStatusEntity> packageStatuses;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getPackageUuid() {
        return packageUuid;
    }

    public void setPackageUuid(UUID packageUuid) {
        this.packageUuid = packageUuid;
    }

    public ClassifierItemEntity getErp() {
        return erp;
    }

    public void setErp(ClassifierItemEntity erp) {
        this.erp = erp;
    }

    public String getXbrlgl() {
        return xbrlgl;
    }

    public void setXbrlgl(String xbrlgl) {
        this.xbrlgl = xbrlgl;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getXbrlGlVersion() {
        return xbrlGlVersion;
    }

    public void setXbrlGlVersion(String xbrlGlVersion) {
        this.xbrlGlVersion = xbrlGlVersion;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public List<AccountingEntryEntity> getAccountingEntryes() {
        return accountingEntryes;
    }

    public void setAccountingEntryes(List<AccountingEntryEntity> accountingEntryes) {
        this.accountingEntryes = accountingEntryes;
    }

    public List<PackageStatusEntity> getPackageStatuses() {
        return packageStatuses;
    }

    public void setPackageStatuses(List<PackageStatusEntity> packageStatuses) {
        this.packageStatuses = packageStatuses;
    }

    @Transient
    public PackageStatus getCurrentStatus() {
        if (this.getPackageStatuses() == null) {
            return null;
        }
        return getPackageStatuses().get(0).getStatus();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PackageEntity))
            return false;

        PackageEntity other = (PackageEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
