/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties("mcds.reports-config")
public class ReportConfig {
  public enum ServiceType {SUBMIT_REPORT, FEEDBACK_SERTVICE}
  public enum ReceiverType {X_ROAD}

  private String path;
  private Map<ReportType, Report> reports;

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  private Map<ReportType, Report> getReports() {
    return this.reports;
  }

  public void setReports(List<Report> reports) {
    if (reports == null) {
      this.reports = new HashMap<>(0);
      return;
    }
    this.reports = new HashMap<>(reports.size());
    for (Report r : reports) {
      this.reports.put(r.getType(), r);
    }
  }

  public String getTemplate(ReportType reportType) {
    Report r = this.reports.get(reportType);
    if (r == null) {
      throw new McdsSystemException("Report template for for report type " + reportType + " not found");
    }
    return r.getTemplate();
  }

  public Report getReportConf(ReportType reportType) {
    return this.reports.get(reportType);
  }

  public static class Report {
    private ReportType type;
    private String template;
    private Receiver receiver;

    public ReportType getType() {
      return type;
    }

    public void setType(ReportType type) {
      this.type = type;
    }

    public String getTemplate() {
      return template;
    }

    public void setTemplate(String template) {
      this.template = template;
    }

    public Receiver getReceiver() {
      return receiver;
    }

    public void setReceiver(Receiver receiver) {
      this.receiver = receiver;
    }

    public String getRequestTemplatePath(ReceiverType receiverType, ServiceType serviceType) {
      if (receiverType == ReceiverType.X_ROAD) {
        Xroad xroad = this.receiver.getXroad();
        if (xroad == null) {
          throw new McdsSystemException(
              "Unconfigured receiver type " + ReceiverType.X_ROAD + " for repoprt " + this.type);
        }
        Service service = xroad.getServices().get(serviceType);
        if (service == null) {
          throw new McdsSystemException(
              "Unconfigured service type " + serviceType + " for report " +
                  this.type + " receiver " + ReceiverType.X_ROAD);
        }
        return service.getTemplate();
      }
      throw new McdsSystemException("Unsupported receiver type " + receiverType);
    }

  }

  public static class Receiver {
    private ReceiverType type;
    private Xroad xroad;

    public ReceiverType getType() {
      return type;
    }

    public void setType(ReceiverType type) {
      this.type = type;
    }

    public Xroad getXroad() {
      return xroad;
    }

    public void setXroad(Xroad xroad) {
      this.xroad = xroad;
    }
  }

  public static class Xroad {
    private String userId;
    private String securityServerUrl;
    private String protocolVersion;
    private String xroadInstance;
    private String membermemberClass;
    private String membermemberCode;
    private String memberSubsystemCode;
    private String serviceMemberClass;
    private String serviceMemberCode;
    private String serviceSubsystemCode;
    private Map<ServiceType, Service> services;

    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public String getSecurityServerUrl() {
      return securityServerUrl;
    }

    public void setSecurityServerUrl(String securityServerUrl) {
      this.securityServerUrl = securityServerUrl;
    }

    public String getProtocolVersion() {
      return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
      this.protocolVersion = protocolVersion;
    }

    public String getXroadInstance() {
      return xroadInstance;
    }

    public void setXroadInstance(String xroadInstance) {
      this.xroadInstance = xroadInstance;
    }

    public String getMembermemberClass() {
      return membermemberClass;
    }

    public void setMembermemberClass(String membermemberClass) {
      this.membermemberClass = membermemberClass;
    }

    public String getMembermemberCode() {
      return membermemberCode;
    }

    public void setMembermemberCode(String membermemberCode) {
      this.membermemberCode = membermemberCode;
    }

    public String getMemberSubsystemCode() {
      return memberSubsystemCode;
    }

    public void setMemberSubsystemCode(String memberSubsystemCode) {
      this.memberSubsystemCode = memberSubsystemCode;
    }

    public String getServiceMemberClass() {
      return serviceMemberClass;
    }

    public void setServiceMemberClass(String serviceMemberClass) {
      this.serviceMemberClass = serviceMemberClass;
    }

    public String getServiceMemberCode() {
      return serviceMemberCode;
    }

    public void setServiceMemberCode(String serviceMemberCode) {
      this.serviceMemberCode = serviceMemberCode;
    }

    public String getServiceSubsystemCode() {
      return serviceSubsystemCode;
    }

    public void setServiceSubsystemCode(String serviceSubsystemCode) {
      this.serviceSubsystemCode = serviceSubsystemCode;
    }

    public Map<ServiceType, Service> getServices() {
      return services;
    }

    public void setServices(List<Service> services) {
      if (services == null) {
        this.services = new HashMap<>(0);
        return;
      }
      this.services = new HashMap<>(services.size());
      for (Service s : services) {
        this.services.put(s.getType(), s);
      }
    }
  }

  public static class Service {
    private ServiceType type;
    private String serviceCode;
    private String serviceVersion;
    private String template;

    public ServiceType getType() {
      return type;
    }

    public void setType(ServiceType type) {
      this.type = type;
    }

    public String getServiceCode() {
      return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
      this.serviceCode = serviceCode;
    }

    public String getServiceVersion() {
      return serviceVersion;
    }

    public void setServiceVersion(String serviceVersion) {
      this.serviceVersion = serviceVersion;
    }

    public String getTemplate() {
      return template;
    }

    public void setTemplate(String template) {
      this.template = template;
    }
  }

}
