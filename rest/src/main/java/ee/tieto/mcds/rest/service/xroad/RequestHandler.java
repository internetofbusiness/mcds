/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.xroad;

import ee.tieto.mcds.rest.service.report.ReportConfig.Report;
import ee.tieto.mcds.rest.service.report.ReportConfig.Xroad;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.UUID;

public class RequestHandler {
    private static final Logger logger = LoggerFactory.getLogger(RequestHandler.class);

    private String dataFileName;
    private String dataFile;
    private String request;
    private final Properties requestProperties;
    private final Properties serverProperties;
    private String errorMessage;

    public RequestHandler(String request) {
        logger.debug("Instantiating RequestHandler");
        requestProperties = new Properties();
        serverProperties = new Properties();
        this.request = request;
    }


    public void init(Report config) {
        Xroad xroad = config.getReceiver().getXroad();
        this.requestProperties.setProperty("protocolVersion", xroad.getProtocolVersion());
        this.requestProperties.setProperty("id", UUID.randomUUID().toString());
        this.requestProperties.setProperty("ServicexRoadInstance", xroad.getXroadInstance());
        this.requestProperties.setProperty("ServicememberClass", xroad.getServiceMemberClass());
        this.requestProperties.setProperty("ServicememberCode", xroad.getServiceMemberCode());
        this.requestProperties.setProperty("ServicesubsystemCode", xroad.getServiceSubsystemCode());

        this.requestProperties.setProperty("MemberxRoadInstance", xroad.getXroadInstance());
        this.requestProperties.setProperty("MembermemberClass", xroad.getMembermemberClass());
        this.requestProperties.setProperty("MembermemberCode", xroad.getMembermemberCode());
        this.requestProperties.setProperty("MembersubsystemCode", xroad.getMemberSubsystemCode());
    }

    public void setProperties(Properties properties) {
        if (properties != null) {
            properties.forEach((key, value) -> this.requestProperties.setProperty((String) key, (String) value));
        }
    }

    public void setReady() {
        insertParametersIntoRequestString();
    }

    public void insertParametersIntoRequestString() { // DO: error handling if params not valid?
        logger.debug("Injecting specified request parameters into request String");
        // header parameters
        if (requestProperties.getProperty("userId") != null) {
            request = request.replace("Placeholder:userId", requestProperties.getProperty("userId"));
        }
        if (requestProperties.getProperty("protocolVersion") != null) {
            request = request.replace("Placeholder:protocolVersion", requestProperties.getProperty("protocolVersion"));
        }
        if (requestProperties.getProperty("id") != null) {
            request = request.replace("Placeholder:id", requestProperties.getProperty("id"));
        }
        // service parameters
        if (requestProperties.getProperty("ServicexRoadInstance") != null) {
            request = request.replace("ServicePlaceholder:xRoadInstance", requestProperties.getProperty("ServicexRoadInstance"));
        }
        if (requestProperties.getProperty("ServicememberClass") != null) {
            request = request.replace("ServicePlaceholder:memberClass", requestProperties.getProperty("ServicememberClass"));
        }
        if (requestProperties.getProperty("ServicememberCode") != null) {
            request = request.replace("ServicePlaceholder:memberCode", requestProperties.getProperty("ServicememberCode"));
        }
        if (requestProperties.getProperty("ServicesubsystemCode") != null) {
            request = request.replace("ServicePlaceholder:subsystemCode", requestProperties.getProperty("ServicesubsystemCode"));
        }
        if (requestProperties.getProperty("ServiceserviceCode") != null) {
            request = request.replace("ServicePlaceholder:serviceCode", requestProperties.getProperty("ServiceserviceCode"));
        }
        if (requestProperties.getProperty("ServiceserviceVersion") != null) {
            request = request.replace("ServicePlaceholder:serviceVersion", requestProperties.getProperty("ServiceserviceVersion"));
        }
        // client/member parameters
        if (requestProperties.getProperty("MemberxRoadInstance") != null) {
            request = request.replace("MemberPlaceholder:xRoadInstance", requestProperties.getProperty("MemberxRoadInstance"));
        }
        if (requestProperties.getProperty("MembermemberClass") != null) {
            request = request.replace("MemberPlaceholder:memberClass", requestProperties.getProperty("MembermemberClass"));
        }
        if (requestProperties.getProperty("MembermemberCode") != null) {
            request = request.replace("MemberPlaceholder:memberCode", requestProperties.getProperty("MembermemberCode"));
        }
        if (requestProperties.getProperty("MembersubsystemCode") != null) {
            request = request.replace("MemberPlaceholder:subsystemCode", requestProperties.getProperty("MembersubsystemCode"));
        }
        // body parameters
        if (requestProperties.getProperty("ServiceserviceCode").compareTo("SubmitData") == 0 && dataFile != null) {
            request = request.replace("Placeholder:DataFile", "cid:" + dataFileName);
        }
        if (requestProperties.getProperty("XSDValidationOnly") != null) {
            request = request.replace("Placeholder:XSDValidationOnly", requestProperties.getProperty("XSDValidationOnly"));
        }
        if (requestProperties.getProperty("SubmitId") != null) {
            request = request.replace("Placeholder:SubmitId", requestProperties.getProperty("SubmitId"));
        }
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getDataFile() {
        return dataFile;
    }

    public void setDataFile(String dataFile) {
        this.dataFile = dataFile;
    }

    public String getDataFileName() {
        return this.dataFileName;
    }

    public void setDataFileName(String name) {
        this.dataFileName = name;
    }

    public Properties getRequestProperties() {
        return requestProperties;
    }

    public Properties getServerProperties() {
        return serverProperties;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
