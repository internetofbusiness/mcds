/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository;

import ee.tieto.mcds.rest.repository.constants.ReportStatus;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.repository.model.ReportEntity;
import ee.tieto.mcds.rest.service.report.model.ReportDataCollector;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

public interface ReportRepositoryCustom {

  List<ReportEntity> findReportsIgnoreNulls(List<String> regnrList, ReportStatus status, String reportType, LocalDate startDate, LocalDate endDate);
  OffsetDateTime findExistsGeneratedReportTime(String regnr, ReportType reportType, LocalDate periodFrom, LocalDate periodUntil);
  boolean isEntriesChangedInPeriod(String regNr, LocalDate periodFrom, LocalDate periodUntil, OffsetDateTime lastGenerationTime);
  void saveReport(Integer id_orga, Integer report_type, LocalDate deadline,
                  LocalDate periodFrom, LocalDate periodUntil,
                  String content, ReportDataCollector collector);

  void refresh(ReportEntity entity);
}
