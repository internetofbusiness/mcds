/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "organization", schema = "mcds", catalog = "mcds")
public class OrganizationEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "regnr", nullable = false)
    private String regnr;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "contact_details", nullable = false)
    private String contactDetails;

    @Column(name = "deactivated")
    private OffsetDateTime deactivated;

    @Column(name = "reg_authority")
    private String regAuthority;

    @OneToMany(mappedBy = "organization")
    private List<ClassifierEntity> classifiers;

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    private List<PermissionEntity> permissions;

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    private Set<OrganizationReportTypeEntity> organizationReportTypes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegnr() {
        return regnr;
    }

    public void setRegnr(String regnr) {
        this.regnr = regnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(String contactDetails) {
        this.contactDetails = contactDetails;
    }

    public OffsetDateTime getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(OffsetDateTime deactivated) {
        this.deactivated = deactivated;
    }

    public String getRegAuthority() {
        return regAuthority;
    }

    public void setRegAuthority(String regAuthority) {
        this.regAuthority = regAuthority;
    }

    public List<ClassifierEntity> getClassifiers() {
        return this.classifiers;
    }

    public void setClassifiers(List<ClassifierEntity> classifiers) {
        this.classifiers = classifiers;
    }

    public List<PermissionEntity> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionEntity> permissions) {
        this.permissions = permissions;
    }

    public Set<OrganizationReportTypeEntity> getOrganizationReportTypes() {
        return organizationReportTypes;
    }

    public void setOrganizationReportTypes(Set<OrganizationReportTypeEntity> organizationReportTypes) {
        this.organizationReportTypes = organizationReportTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof OrganizationEntity))
            return false;

        OrganizationEntity other = (OrganizationEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
