/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.repository.AccountingEntryRepository;
import ee.tieto.mcds.rest.repository.ClassifierRepository;
import ee.tieto.mcds.rest.repository.model.AccountingEntryEntity;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.service.report.model.ReportDataCollector;
import ee.tieto.mcds.rest.service.report.model.ReportMetadata;
import ee.tieto.mcds.rest.service.report.model.ReportNonCompliances;
import ee.tieto.mcds.rest.service.report.model.ReportNonCompliances.Severity;
import ee.tieto.mcds.rest.service.report.model.StatSalaryReportMetadata;
import org.dom4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;

public class StatSalaryReportGeneratorHelper {
  private static final Logger log = LoggerFactory.getLogger(StatSalaryReportGeneratorHelper.class);

  private static final Map<String, String> pathNamespaces = new HashMap<>();
  static {
    pathNamespaces.put("xbrli", "http://www.xbrl.org/2003/instance");
    pathNamespaces.put("gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
    pathNamespaces.put("gl-bus", "http://www.xbrl.org/int/gl/bus/2015-03-25");
  }


  private static final QName ENTRY_HEADER_QNAME = DocumentFactory.getInstance().createQName(
      "entryHeader", "gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");

  private static final String XPATH_SOURCEJOURNAL_ID =
      "gl-cor:accountingEntries/gl-cor:entryHeader/gl-cor:sourceJournalID";
  private static final String XPATH_ACCOUNT_SUB =
      "gl-cor:accountingEntries/gl-cor:entryHeader/gl-cor:entryDetail/gl-cor:account/gl-cor:accountSub";
  private static final String XPATH_ACCOUNT_SUBTYPE = "gl-cor:accountSubType";
  private static final String XPATH_ACCOUNT_SUBID = "gl-cor:accountSubID";
  private static final String XPATH_IDENTIFIER_SCHEME = "xbrli:context/xbrli:entity/xbrli:identifier/@scheme";
  private static final String XPATH_IDENTIFIER = "xbrli:context/xbrli:entity/xbrli:identifier";


  private final AccountingEntryRepository entryRepository;
  private final ClassifierRepository classifierRepository;
  private final StatSalaryReportMetadata meta;
  private final Map<String, Set<String>> classifierItems;

  public StatSalaryReportGeneratorHelper(AccountingEntryRepository entryRepository, ClassifierRepository classifierRepository, ReportMetadata meta) {
    this.classifierRepository = classifierRepository;
    if (!(meta instanceof StatSalaryReportMetadata)) {
      throw new IllegalArgumentException("meta must be type of " + StatSalaryReportMetadata.class.getTypeName());
    }
    this.meta = (StatSalaryReportMetadata)meta;
    this.entryRepository = entryRepository;
    this.classifierItems = new HashMap<>();
  }

  public ReportDataCollector generateReport(OrganizationEntity org, final Document template, ReportDataCollector collector) {
    Stream<AccountingEntryEntity> entries =
        entryRepository.findOrganizationEntriesInPostingPeriod(org.getRegnr(), meta.getFromDate(), meta.getUntilDate());
    entries.forEach(e-> {
      ReportNonCompliances nonCompliance = addToReport(e, org, template);
      if (!nonCompliance.haveErrors()) {
        if (nonCompliance.haveWarnings()) {
          collector.addNonCompliances(e.getEntryUuid(), nonCompliance);
        }
        collector.addEntryId(e.getId());
      }
    });
    return collector;
  }

  private ReportNonCompliances validateEntry(final Element entry) {
    ReportNonCompliances nonCompliances = new ReportNonCompliances();

    // gl-cor:sourceJournalID
    String tmp = findElementValue(entry, XPATH_SOURCEJOURNAL_ID);
    if (tmp == null || !meta.getSourceJournalId().contains(tmp)) {
      nonCompliances.add(Severity.ERROR, "Unsupported gl-cor:sourceJournalID value - " + tmp);
      return nonCompliances;
    }

    List<Node> subs = findNodelist(entry, XPATH_ACCOUNT_SUB);
    if (subs == null || subs.isEmpty()) {
      nonCompliances.add(Severity.ERROR, "gl-cor:accountSub elements not found");
      return nonCompliances;
    }

    for (Node n : subs) {
      String subType = findElementValue ((Element)n, XPATH_ACCOUNT_SUBTYPE);
      if (!meta.getAccountSubType().contains(subType)) {
        nonCompliances.add(Severity.ERROR, "Unsupported gl-cor:accountSubType value - " + subType);
      }

      String subId = findElementValue ((Element)n, XPATH_ACCOUNT_SUBID);
      if (!checkClassifierItem(subType, subId)) {
        nonCompliances.add(Severity.WARNING,
            "gl-cor:accountSubID value " + subId + " not found in classifier " + subType);
      }
    }

    return nonCompliances;
  }

  private boolean checkClassifierItem(String subType, String subId) {
    Set<String> items = classifierItems.get(subType);
    if (items == null) {
      items = classifierRepository.getItemCodesAsSet(subType);
      classifierItems.put(subType, items);
    }
    return items.isEmpty() || items.contains(subId);
  }

  private ReportNonCompliances addToReport(AccountingEntryEntity entry, OrganizationEntity org, final Document template) {
    ReportNonCompliances nonCompliances;
    boolean add = true;
    try {
      Element doc = DocumentHelper.parseText(entry.getEntry()).getRootElement();
      nonCompliances = validateEntry(doc);
      if (nonCompliances.haveErrors()) {
        add = false;
        return nonCompliances;
      }
      // add to report
      setEntryToReport(template.getRootElement(), doc);
      return nonCompliances;
    } catch (Exception e) {
      add = false;
      log.error("Unparseble accounting_ebtry, ID={}", entry.getId());
      throw new McdsSystemException("Unparseble accounting entry, ID=" + entry.getId(), e);
    } finally {
      entryRepository.detach(entry);
    }
  }

  private void setEntryToReport(Element report, Element entry) {
    Element entries = (Element)findNodelist(report, "gl-cor:accountingEntries").get(0);
    List<Node> headers = findNodelist(entry, "gl-cor:accountingEntries/gl-cor:entryHeader");
    if (headers == null) {
      return;
    }
    for (Node node : headers) {
      Element header = (Element)node.clone();
      header.detach();
      Attribute id = header.attribute("id");
      if (id != null) {
        header.remove(id);
      }
      entries.add(header);
    }
  }

  private String findElementValue(Element element, String xpath) {
    XPath path = DocumentHelper.createXPath(xpath);
    path.setNamespaceURIs(pathNamespaces);
    Node node = path.selectSingleNode(element);
    return node == null ? null : node.getText();
  }

  private List<Node> findNodelist(Element element, String xpath) {
    XPath path = DocumentHelper.createXPath(xpath);
    path.setNamespaceURIs(pathNamespaces);
    List<Node> nodeList = path.selectNodes(element);
    if (nodeList == null) {
      return new ArrayList<>(0);
    }
    return nodeList;
  }

}
