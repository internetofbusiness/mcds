/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.rest.McdsNotFoundException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.Classifier;
import ee.tieto.mcds.rest.service.ClassifierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/manage")
public class ClassifiersController {

  private final ClassifierService service;

  @Autowired
  public ClassifiersController(ClassifierService service) {
    this.service = service;
  }

  // Teenus: Klassifikaatorite pärimine MCDS-ist
  @GetMapping(value = {"/classifier/items/{classifierCode}", "/classifier/items/{classifierCode}/{versionCode}"}, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public Classifier getClassifier(
      @PathVariable(value = "classifierCode") String classifierCode,
      @PathVariable(name = "versionCode", required = false) String versionCode) {
    try {
      return service.getClassifier(classifierCode, versionCode);
    } catch (Exception e) {
      throw new McdsNotFoundException();
    }
  }

  // Teenus: Klassifikaatorite pärimine välisest allikast
  @GetMapping(value = "/classifier/externalClassifierStat/{classifierCode}", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public void getExternalClassifierStat(@PathVariable("classifierCode") String classifierCode,
                                        @RequestParam(name = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate date) {
    try {
      service.getExternalClassifierStat(classifierCode, date);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @GetMapping(value = "/classifier/list/all", produces = "application/json;charset=UTF-8")
  public List<Classifier> getClassifierList() {
    return service.getClassifierList();
  }

  @GetMapping(value = "/classifier/updateEstatClassifiers", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public void getExternalClassifierStat () {
    service.getExternalClassifierStat();
  }
}
