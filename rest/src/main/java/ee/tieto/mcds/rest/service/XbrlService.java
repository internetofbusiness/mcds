/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service;

import ee.tieto.mcds.rest.model.PackageStatus;
import ee.tieto.mcds.validator.ValidationResult;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface XbrlService {

  String saveXbrl(String xbrl, String erpCode, String xbrlVersion);
  ValidationResult validate(String xbrl, String packageId, String version);
  ValidationResult process(String packageUuid);

  List<PackageStatus> getEntriesStatus(
      String erpCode, UUID sessionId, String entryId,
      LocalDate receivedFrom, LocalDate receivedUntil);

  List<PackageStatus> getInvoiceEntriesStatus(
    UUID sessionId, String entryId,
    LocalDate receivedFrom, LocalDate receivedUntil);

  List<String> getEntries(String regNr, String returnType, LocalDate from, LocalDate until,
                          String entryType, Set<String> entryIdentifiers);

  List<String> getInvoiceEntryes(String regNr, String returnType, LocalDate from, LocalDate until,
                          String entryType, Set<String> entryIdentifiers);

  //DEBUG
  void saveSourceJournal();
}
