/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.repository.model.PermissionEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionsByOrganization {
  private static final String NULL_BUSINESS_ARES = "NULL_BUSINESS_AREA";

  private final Map<String, Map<String, OrganizationEntity>> organizations;


  public PermissionsByOrganization(List<PermissionEntity> perm, boolean ignoreReportType) {
    this.organizations = new HashMap<>();
    if (perm == null || perm.isEmpty()) {
      return;
    }
    perm.forEach(p -> {
      if (ignoreReportType && p.getReportType() != null) {
        return;
      }
      OrganizationEntity org = p.getOrganization();
      Map<String, OrganizationEntity> areas = organizations.get(org.getRegnr());
      if (areas == null) {
        areas = new HashMap<>();
        organizations.put(org.getRegnr(), areas);
      }
      areas.put(p.getBusinessArea() == null ? NULL_BUSINESS_ARES : p.getBusinessArea(), org);
    });
  }

  public OrganizationEntity get(String regNr, String businessArea) {
    Map<String, OrganizationEntity> areas = organizations.get(regNr);
    if (areas == null) {
      return null;
    }
    String area = businessArea == null ? NULL_BUSINESS_ARES : businessArea;
    OrganizationEntity org = areas.get(area);
    if (org == null && businessArea != null) {
      return areas.get(NULL_BUSINESS_ARES);
    }
    return org;
  }

  public boolean isEmpty() {
    return this.organizations.isEmpty();
  }
}
