/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report.model;

import ee.tieto.mcds.rest.repository.constants.ReportStatus;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ReportDataCollector {

  private String reportId;
  private Integer reportTypeId;
  private final Set<Integer> entryIds = new HashSet<>();
  private Map<String, ReportNonCompliances> noncompliances = new HashMap<>();

  public String getReportId() {
    return reportId;
  }

  public void setReportId(String reportId) {
    this.reportId = reportId;
  }

  public Integer getReportTypeId() {
    return reportTypeId;
  }

  public void setReportTypeId(Integer reportTypeId) {
    this.reportTypeId = reportTypeId;
  }

  public Map<String, ReportNonCompliances> getNoncompliances() {
    return noncompliances;
  }

  public void setNoncompliances(Map<String, ReportNonCompliances> noncompliances) {
    this.noncompliances = noncompliances;
  }

  public void addNonCompliances(String uuid, ReportNonCompliances nonCompliance) {
    this.noncompliances.put(uuid, nonCompliance);
  }

  public Set<Integer> getEntryIds() {
    return entryIds;
  }

  public void addEntryId(Integer id) {
    this.entryIds.add(id);
  }

  public ReportStatus getReportStatus() {
    if (!noncompliances.values().stream().filter(ReportNonCompliances::haveErrors).findFirst().isEmpty()) {
      return ReportStatus.ERROR;
    }
    return ReportStatus.GENERATED;
  }

  public String getInformation() {
    if (noncompliances.isEmpty()) {
      return null;
    }
    return noncompliances.toString();
  }
}
