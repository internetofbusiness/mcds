/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "classifier_attribute_value", schema = "mcds", catalog = "mcds")
public class ClassifierAttributeValueEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_clat")
    private ClassifierAttributeEntity classifierAttribute;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_clit")
    private ClassifierItemEntity classifierItem;

    @Column(name = "value")
    private String value;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClassifierAttributeEntity getClassifierAttribute() {
        return classifierAttribute;
    }

    public void setClassifierAttribute(ClassifierAttributeEntity classifierAttribute) {
        this.classifierAttribute = classifierAttribute;
    }

    public ClassifierItemEntity getClassifierItem() {
        return classifierItem;
    }

    public void setClassifierItem(ClassifierItemEntity classifierItem) {
        this.classifierItem = classifierItem;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ClassifierAttributeValueEntity))
            return false;

        ClassifierAttributeValueEntity other = (ClassifierAttributeValueEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
