/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.impl;

import ee.tieto.mcds.rest.repository.ClassifierRepositoryCustom;
import ee.tieto.mcds.rest.repository.model.ClassifierEntity;

import javax.persistence.*;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class ClassifierRepositoryImpl implements ClassifierRepositoryCustom {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<ClassifierEntity> findClassifier(String classifierCode) {
    String query = "SELECT c FROM  ClassifierEntity c WHERE c.code = :cCode";
    TypedQuery<ClassifierEntity> qu = entityManager.createQuery(query, ClassifierEntity.class)
        .setParameter("cCode", classifierCode);

    return qu.getResultList();
  }

  @Override
  public Set<String> getItemCodesAsSet(String classifierCode) {
    String select =
        "select ci.code from ClassifierEntity c join ClassifierItemEntity ci on ci.classifier = c where c.code = :classifierCode";
    //noinspection unchecked
    return (Set<String>)entityManager.createQuery(select)
        .setParameter("classifierCode", classifierCode)
        .getResultStream().collect(Collectors.toSet());
  }

  public void detach(Object entity) {
    this.entityManager.detach(entity);
  }
}
