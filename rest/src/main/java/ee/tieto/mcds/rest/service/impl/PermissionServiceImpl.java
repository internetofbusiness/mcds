/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.converter.EntityToRestPermission;
import ee.tieto.mcds.rest.model.Permission;
import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.OrganizationRepository;
import ee.tieto.mcds.rest.repository.PermissionRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.repository.model.PermissionEntity;
import ee.tieto.mcds.rest.McdsDataIntegrityException;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

  private final PermissionRepository permRepo;
  private final OrganizationRepository orgRepo;
  private final ClassifierItemRepository classItemRepo;
  private final EntityToRestPermission permConverter;
  private final EventLogService elogService;

  @Autowired
  public PermissionServiceImpl(
      PermissionRepository permRepo,
      OrganizationRepository orgRepo,
      ClassifierItemRepository classItemRepo,
      EntityToRestPermission permConverter,
      EventLogService elogService) {
    this.permRepo = permRepo;
    this.orgRepo = orgRepo;
    this.classItemRepo = classItemRepo;
    this.permConverter = permConverter;
    this.elogService = elogService;
  }

  @Override
  @Transactional
  public void savePermissions(List<Permission> permissions) {
    for (Permission p : permissions) {
      List<PermissionEntity> permissionEntities = permRepo.findPermissionsUseNulls(
          p.getErpCode(), p.getRegNr(), p.getBusinessArea(), p.getReportType());

      // Permission must be unique by erpCode, regNr, businessArea, reportType
      if (permissionEntities.size() > 1) {
        String message = String.format(
            "More than one permission was found in the database, parameters [erpCode=%s, regNr=%s, businessArea=%s, reportType=%s]",
            p.getErpCode(), p.getRegNr(), p.getBusinessArea(), p.getReportType());
        throw new McdsDataIntegrityException(message);
      }

      // Restricted to save new inactivated permission
      if (permissionEntities.isEmpty() && !p.isActive()) {
        throw new McdsValidationException(String.format(
            "Can't find permission to deactivate - parameters [erpCode=%s, regNr=%s, businessArea=%s, reportType=%s]",
            p.getErpCode(), p.getRegNr(), p.getBusinessArea(), p.getReportType()));
      }

      PermissionEntity saved = savePermission(p, permissionEntities.isEmpty() ? null : permissionEntities.get(0));
      elogService.newEvent(
          p.isActive() ? EventType.AUTHORIZATION_ADD : EventType.AUTHORIZATION_DELETE,
          p.getErpCode(), "permission:" + saved.getId(), true, null);
    }
  }

  private PermissionEntity savePermission(Permission permission, final PermissionEntity permissionEntity) {
    PermissionEntity entity = permissionEntity;
    if (entity == null) {
      entity = new PermissionEntity();
    }

    // Organization
    if (entity.getOrganization() == null && permission.getRegNr() != null) {
      List<OrganizationEntity> orgs = orgRepo.findOrganization(permission.getRegNr());
      if (orgs.isEmpty()) {
        throw new McdsValidationException(
            String.format("Organization with reg code %s not found", permission.getRegNr()));
      }
      entity.setOrganization(orgs.get(0));
    }

    // Erp
    if (entity.getErp() == null && permission.getErpCode() != null) {
      entity.setErp(
          classItemRepo.findByClassifierCodeAndItemCode(Classifier.ERP.name(), permission.getErpCode())
              .orElseThrow(() -> new McdsValidationException(
                  String.format("Erp with code %s not found", permission.getErpCode()))));
    }

    // reportType
    if (entity.getReportType() == null && permission.getReportType() != null) {
      entity.setReportType(
          classItemRepo.findByClassifierCodeAndItemCode(
              Classifier.REPORT_TYPE.name(), permission.getReportType())
              .orElseThrow(() -> new McdsValidationException(
                  String.format("Report type with code %s not found", permission.getReportType()))
          )
      );
    }

    //Business area
    entity.setBusinessArea(permission.getBusinessArea());

    // deactivated
    entity.setDeactivated(permission.isActive() ?  null : OffsetDateTime.now());

    // Persist if new
    permRepo.save(entity);
    return entity;
  }

  @Override
  @Transactional
  public List<Permission> findPermissions (
      String erpCode, String orgRegNr, String businessArea, String reportTypeCode) {
    //TODO throw McdsSystem exception
    return permConverter.convert(
        permRepo.findPermissionsIgnoreNulls(erpCode, orgRegNr, businessArea, reportTypeCode, false)
    );
  }
}
