/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.rest.repository.DbHealthRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class SystemController implements HealthIndicator {
  private static final Logger log = LoggerFactory.getLogger(SystemController.class);

  private final DbHealthRepository dbHealthRepo;

  public SystemController(DbHealthRepository dbHealthRepo) {
    this.dbHealthRepo = dbHealthRepo;
  }

  @Override
  @GetMapping(value = "/health", produces = "application/json;charset=UTF-8")
  public Health health() {
    try {
      dbHealthRepo.isDbHealt();
      return Health.up().build();
    } catch (Exception e) {
      log.error(e.getMessage());
      return Health.down().build();
    }
  }
}
