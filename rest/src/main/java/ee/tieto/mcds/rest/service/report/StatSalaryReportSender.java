/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.repository.ReportRepository;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.repository.model.ReportEntity;
import ee.tieto.mcds.rest.service.report.ReportConfig.ReceiverType;
import ee.tieto.mcds.rest.service.report.ReportConfig.Report;
import ee.tieto.mcds.rest.service.report.ReportConfig.ServiceType;
import ee.tieto.mcds.rest.service.report.model.SenderResponse;
import ee.tieto.mcds.rest.service.xroad.RequestHandler;
import ee.tieto.mcds.rest.service.xroad.SoapClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;

@Component
public class StatSalaryReportSender implements ReportSender {
  private static final Logger log = LoggerFactory.getLogger(StatSalaryReportSender.class);

  private static final ReceiverType RECEIVER_TYPE = ReceiverType.X_ROAD;
  private static final ServiceType SERVICE_TYPE_SUBMIT = ServiceType.SUBMIT_REPORT;
  private static final String ATTACHMENT_FILE_NAME = "reportDataAsXbrlGl";

  private final Report config;

  public StatSalaryReportSender(ReportConfig fullConfig, ReportRepository reportRepo) {
    this.config = fullConfig.getReportConf(ReportType.STAT_SALARY);
  }

  @Override
  public SenderResponse sendReport(ReportEntity reportEntity) {
    try {
      RequestHandler handler = getHandler();
      Properties properties = new Properties();
      properties.setProperty("userId", config.getReceiver().getXroad().getUserId());
      properties.setProperty("ServiceserviceCode",
          config.getReceiver().getXroad().getServices().get(SERVICE_TYPE_SUBMIT).getServiceCode());
      properties.setProperty("ServiceserviceVersion",
          config.getReceiver().getXroad().getServices().get(SERVICE_TYPE_SUBMIT).getServiceVersion());
      properties.setProperty("XSDValidationOnly", "1");
      handler.init(this.config);
      handler.setProperties(properties);
      handler.setDataFile(reportEntity.getContent());
      handler.setDataFileName(ATTACHMENT_FILE_NAME);
      handler.setReady();
      SoapClient client = new SoapClient();
      SOAPMessage resp = client.callSoapWebService(config.getReceiver().getXroad().getSecurityServerUrl(), handler);
      SenderResponse response = prepareSubmitResponse(resp, handler.getErrorMessage());
      log.trace("SenderResponse: {}", response);
      return response;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  @Override
  public void getFeedback() {
  }

  private SenderResponse prepareSubmitResponse(SOAPMessage message, String errorMessage) throws Exception {
    SenderResponse response = new SenderResponse();
    if (message == null) {
      response.setErrorMessage(Objects.requireNonNullElse(errorMessage, "Sender response is NULL"));
      return response;
    }
    SOAPBody body = message.getSOAPBody();
    if (body.hasFault()) {
      SOAPFault fault = body.getFault();
      response.setFault(fault.getFaultCode(), fault.getFaultString());
      return response;
    }

    NodeList nodes = body.getElementsByTagName("XSDValidationErrorMessage");
    if (nodes != null && nodes.getLength() > 0) {
      String error = nodes.item(0).getTextContent();
      response.setErrorMessage(error);
      return response;
    }

    String submitId = null;
    nodes = body.getElementsByTagName("SubmitId");
    if (nodes != null && nodes.getLength() > 0) {
      submitId = nodes.item(0).getTextContent();
    }

    if (submitId != null && !submitId.isBlank()) {
      response.setSessionId(submitId);
    } else {
      response.setErrorMessage("SubmitId not foind in SOAP response");
    }
    return response;
  }

  private RequestHandler getHandler() {
    try {
      Path templateFile = Path.of(config.getRequestTemplatePath(RECEIVER_TYPE, StatSalaryReportSender.SERVICE_TYPE_SUBMIT));
      return new RequestHandler(Files.readString(templateFile, StandardCharsets.UTF_8));
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }
}
