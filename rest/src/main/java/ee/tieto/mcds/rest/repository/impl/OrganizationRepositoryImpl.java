/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.impl;

import ee.tieto.mcds.rest.repository.OrganizationRepositoryCustom;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Repository
public class OrganizationRepositoryImpl implements OrganizationRepositoryCustom {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<OrganizationEntity> findOrganization(String regnr) {
    String query = "SELECT o FROM OrganizationEntity o";
    TypedQuery<OrganizationEntity> qu;
    if (regnr != null) {
      query += " WHERE o.regnr = :regnr";
      qu = entityManager.createQuery(query, OrganizationEntity.class).setParameter("regnr", regnr);
    } else {
      qu = entityManager.createQuery(query, OrganizationEntity.class);
    }
    return qu.getResultList();
  }

  @Override
  public List<OrganizationEntity> findByPermittedErp(String erpCode, OffsetDateTime date) {
    String query = "select o from OrganizationEntity  o " +
        "inner join o.permissions p " +
        "inner join p.erp e " +
        "where " +
        "e.code = :erpCode " +
        "and (p.reportType is null and  (p.deactivated is null or p.deactivated > :date)) " +
        "and ((e.validFrom is null or e.validFrom <= :date) and (e.validUntil is null or e.validUntil > :date))";
    return entityManager.createQuery(query, OrganizationEntity.class)
        .setParameter("erpCode", erpCode)
        .setParameter("date", date)
        .getResultList();
  }

  @Override
  public List<OrganizationEntity> findOrganizationsForActiveReportType(String reportTypeCode, OffsetDateTime time) {
    String query = "select o from " +
        "OrganizationEntity o " +
        "   join OrganizationReportTypeEntity ort on ort.organization = o " +
        "   join ClassifierItemEntity repType on repType = ort.reportType " +
        "   join ClassifierEntity cl on cl = repType.classifier " +
        "where " +
        "    (ort.deactivated is null or ort.deactivated > :time) " +
        "    and repType.code = :reportTypeCod " +
        "    and (repType.validFrom is null or repType.validFrom <= :time) " +
        "    and (repType.validUntil is null or repType.validUntil > :time) " +
        "    and cl.code = '" + Classifier.REPORT_TYPE.name() + "'";

    TypedQuery<OrganizationEntity> qu = entityManager.createQuery(query, OrganizationEntity.class);
    qu.setParameter("time", time);
    qu.setParameter("reportTypeCod", reportTypeCode);
    return qu.getResultList();
  }


}
