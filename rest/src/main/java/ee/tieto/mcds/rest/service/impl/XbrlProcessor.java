/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.helper.Assert;
import ee.tieto.mcds.rest.repository.*;
import ee.tieto.mcds.rest.repository.constants.PackageStatus;
import ee.tieto.mcds.rest.repository.model.*;
import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.ValidatorConfig;
import ee.tieto.mcds.validator.ValidatorConfig.ContextType;
import ee.tieto.mcds.validator.XbrlValidatorFactory;
import ee.tieto.mcds.validator.BusinessValidator;
import ee.tieto.mcds.validator.BusinessValidator.ValidationContext;
import org.dom4j.*;
import org.postgresql.util.PSQLException;
import org.postgresql.util.ServerErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Component
@Transactional(propagation = Propagation.MANDATORY)
public class XbrlProcessor {
  private static final Logger log = LoggerFactory.getLogger(XbrlProcessor.class);

  private static final QName ENTRY_HEADER_QNAME = DocumentFactory.getInstance().createQName(
      "entryHeader", "gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
  private static final QName ACCOUNTING_ENTRIES_QNAME = DocumentFactory.getInstance().createQName(
      "accountingEntries", "gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
  private static final Map<String, String> pathNamespaces = new HashMap<>();

  private static final String ENTRY_UUID_PATH = "gl-cor:accountingEntries/gl-cor:entryHeader/@id";
  private static final String ENTRY_DETAIL_PATH = "gl-cor:entryDetail";
  private static final String POSTING_DATE_PATH = "gl-cor:postingDate";
  private static final String DETAIL_LINE_NR_PATH = "gl-cor:lineNumber";
  private static final String DOCUMENT_INFO_TARGET_APPLICATION_PATH = "gl-cor:documentInfo/gl-bus:targetApplication";
  private static final String DOCUMENT_INFO_SOURCE_APPLICATION_PATH = "gl-cor:documentInfo/gl-bus:sourceApplication";
  private static final String ENTRY_HEADER_SOURCE_JOURNAL_ID_PATH = "gl-cor:sourceJournalID";

  static {
    pathNamespaces.put("xbrli", "http://www.xbrl.org/2003/instance");
    pathNamespaces.put("gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
    pathNamespaces.put("gl-bus", "http://www.xbrl.org/int/gl/bus/2015-03-25");
  }

  private static final String ENTRY_TYPE_CLASSIFIER_CODE = "ENTRY_TYPE";

  private final DocumentFactory factory = DocumentFactory.getInstance();
  private final XbrlValidatorFactory validatorFactory;
  private final PackageStatusRepository packageStatusRepository;
  private final AccountingEntryRepository entryRepo;
  private final AccountingEntryDetailRepository entryDetailRepo;
  private final ValidationResultRepository valResultRepo;
  private final PermissionRepository permRepo;
  private final ClassifierItemRepository clitemRepo;
  private final ValidatorConfig config;

  @Autowired
  public XbrlProcessor(
      XbrlValidatorFactory validatorFactory,
      PackageRepository packageRepo,
      PackageStatusRepository packageStatusRepository, AccountingEntryRepository entryRepo, AccountingEntryDetailRepository entryDetailRepo, ValidationResultRepository valResultRepo, OrganizationRepository orgRepo,
      PermissionRepository permRepo, ClassifierItemRepository clitemRepo, ValidatorConfig config) {
    this.validatorFactory = validatorFactory;
    this.packageStatusRepository = packageStatusRepository;
    this.entryRepo = entryRepo;
    this.entryDetailRepo = entryDetailRepo;
    this.valResultRepo = valResultRepo;
    this.permRepo = permRepo;
    this.clitemRepo = clitemRepo;
    this.config = config;
  }

  public ValidationResult process(PackageEntity pack) {
    try {
      String version = pack.getXbrlGlVersion();

      PermissionsByOrganization perms = new PermissionsByOrganization(permRepo.findByErpCodeAndDate(pack.getErp().getCode(), OffsetDateTime.now()), true);
      if (perms.isEmpty()) {
        log.warn("ERP {} don't have right to process package {} on {}",
            pack.getErp().getCode(), pack.getPackageUuid(), pack.getCreated());
        updatePackage(pack, PackageStatus.ENTRIES_CREATING_ERROR);
      }
      Map<String, ClassifierItemEntity> entryTypes = loadEntryTypes(version, pack.getCreated());

      String xbrlString = pack.getXbrlgl();

      Document xbrl = DocumentHelper.parseText(xbrlString);
      Document template = createTemplate(xbrl, ACCOUNTING_ENTRIES_QNAME);

      //XBRL
      ValidationResult result = validate(template, version, ValidationContext.XBRL, null);
      String regNr = getElementValue(
          template.getRootElement(), config.getregNrXpath(version, ContextType.GLOBAL));
      String entryType = getElementValue(
          template.getRootElement(), config.getEntryTypeXpath(version, ContextType.GLOBAL));

      //ENTRIES
      List<Node> accountingEntriesList = findNodelist(
          xbrl.getRootElement(), "gl-cor:accountingEntries");
      boolean valid = result.isValid();

      for (Node accountingEntries : accountingEntriesList) {
        Set<String> usedIds = new HashSet<>(accountingEntriesList.size());
        Document accountingEntryesTemplate = (Document) template.clone();
        addNew(accountingEntryesTemplate.getRootElement(), (Element) accountingEntries, ENTRY_HEADER_QNAME);
        ValidationResult entriesResult = validate(
            accountingEntryesTemplate, version, ValidationContext.ENTRIES, result.clone());
        valid = entriesResult.isValid() && valid;
        String entriesRegNr = regNr;
        if (entriesRegNr == null) {
          entriesRegNr = getElementValue(
              (Element) accountingEntries, config.getregNrXpath(version, ContextType.ENTRIES));
        }
        String entriesEntryType = entryType;
        if (entriesEntryType == null) {
          entriesEntryType = getElementValue(
              (Element) accountingEntries, config.getEntryTypeXpath(version, ContextType.ENTRIES));
        }

        String sourceApplication = getElementValue(
            (Element) accountingEntries,DOCUMENT_INFO_SOURCE_APPLICATION_PATH);

        String targetApplication = getElementValue(
            (Element) accountingEntries,DOCUMENT_INFO_TARGET_APPLICATION_PATH);

        OrganizationEntity org = perms.get(entriesRegNr, sourceApplication);
        if (org == null) {
          throw new McdsValidationException("Unusable Organization reg nr - " + entriesRegNr);
        }

        //ENTRY
        List<Node> headers = findNodelist((Element) accountingEntries, "./gl-cor:entryHeader");
        for (Node entryHeader : headers) {
          Document entry = createEntry(entryHeader, accountingEntryesTemplate);
          ValidationResult entryResult = validate(entry, version, ValidationContext.ENTRY, entriesResult.clone());
          valid = entryResult.isValid() && valid;
          String entryRegNr = entriesRegNr;
          String entryEntryType = entriesEntryType;
          String sourceJournalId = getElementValue(
              (Element)entryHeader, ENTRY_HEADER_SOURCE_JOURNAL_ID_PATH);

          log.trace("RESULT - {}", entryResult);
          log.trace("ENTRY:\n{}", entry.asXML());
          String entryId = getHeaderId(usedIds, entry);
          List<Node> entryDetails = findNodelist((Element)entryHeader, ENTRY_DETAIL_PATH);
          if (entryDetails == null || entryDetails.isEmpty()) {
            throw new McdsValidationException(ENTRY_DETAIL_PATH + " required");
          }
          List<AccountingEntryDetailEntity> entryDetailEntities =
              entryDetails.stream()
                  .map(this::createEntryDetailEntity)
                  .collect(Collectors.toList());
          saveEntry(entry, entryId, pack, entryDetailEntities, org, entryTypes.get(entryEntryType), sourceJournalId, targetApplication, sourceApplication, version, entryResult);
        }
      }
      updatePackage(pack, PackageStatus.ENTRIES_CREATED);
      return result;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  private AccountingEntryDetailEntity createEntryDetailEntity(Node detailNode) {
    Element elem = (Element)detailNode;
    AccountingEntryDetailEntity entry = new AccountingEntryDetailEntity();
    List<Node> nodeList = findNodelist(elem, DETAIL_LINE_NR_PATH);
    if (nodeList != null && !nodeList.isEmpty()) {
      entry.setLineNumber(nodeList.get(0).getText());
    }

    nodeList = findNodelist(elem, POSTING_DATE_PATH);
    LocalDate postingDate = null;
    if (nodeList != null && !nodeList.isEmpty()) {
      String postingDateString = nodeList.get(0).getText();
      if (postingDateString.length() > 10) {
        postingDateString = postingDateString.substring(0, 10);
      }
      postingDate = LocalDate.parse(postingDateString);
    }
    entry.setPostingDate(postingDate);
    return entry;
  }

  private String getHeaderId(Set<String> oldIds, Document xbrl) {
    String id = getElementValue(xbrl.getRootElement(), ENTRY_UUID_PATH);
    if (id == null) {
      throw new McdsValidationException("entryHeader id required");
    }
    if (oldIds.contains(id)) {
      throw new McdsValidationException("entryHeader id mast be unique ower xbrl-gl - " + id);
    }
    return id;
  }

  private void saveEntry(
      Document entry,
      String entryId,
      PackageEntity pack,
      List<AccountingEntryDetailEntity> details,
      OrganizationEntity org,
      ClassifierItemEntity entryType,
      String sourceJournalId,
      String targetApplication,
      String sourceApplication,
      String version,
      ValidationResult validationResult) {
    Assert.notNull(entry, pack, org, entryType, version);

    AccountingEntryEntity entity = new AccountingEntryEntity();
    entity.setEntryUuid(entryId);
    entity.setPackage(pack);
    entity.setOrganization(org);
    entity.setEntryType(entryType);
    entity.setTargetApplication(targetApplication);
    entity.setSourceJournalId(sourceJournalId);
    entity.setEntry(entry.asXML());
    entity.setValid(validationResult.isValid());
    entity.setRulesName(version);
    entity.setBusinessArea(sourceApplication);
    try {
      entryRepo.save(entity);
      if (!validationResult.isValid()) {
        ValidationResultEntity vrEntity = new ValidationResultEntity();
        vrEntity.setRule(version);
        vrEntity.setMessage(validationResult.getMessages().toString());
        vrEntity.setAccountingEntry(entity);
        if (entity.getValidationResults() == null) {
          entity.setValidationResults(new ArrayList<>());
        }
        entity.getValidationResults().add(vrEntity);
        valResultRepo.save(vrEntity);
      }
      details.forEach(d -> {
        d.setAccountingEntry(entity);
        entryDetailRepo.save(d);
      });
      entryRepo.flush();
    } catch (DataIntegrityViolationException e) {
      Throwable root = e.getMostSpecificCause();
      //TODO The database specific implementation must be separated
      if (root instanceof PSQLException) {
        PSQLException pe = (PSQLException)root;
        ServerErrorMessage servMess = pe.getServerErrorMessage();
        if ("23505".equals(servMess.getSQLState())) {
          throw new McdsValidationException(pe.getServerErrorMessage().getDetail());
        }
      }
      throw new McdsSystemException("Database exception");
    }

  }

  private void updatePackage(PackageEntity pack, PackageStatus status) {
    boolean valid = status == PackageStatus.ENTRIES_CREATED;
    pack.setValid(valid);
    PackageStatusEntity entity = new PackageStatusEntity();
    entity.setStatus(status);
    entity.setPackage(pack);
    packageStatusRepository.save(entity);
    pack.getPackageStatuses().add(entity);
  }

  private Map<String, ClassifierItemEntity> loadEntryTypes(String version, OffsetDateTime date) {
    return this.clitemRepo.findByClassifierCodeAndVersionCodeAndDate(version, ENTRY_TYPE_CLASSIFIER_CODE, date.toLocalDate())
        .stream().collect(Collectors.toMap(ClassifierItemEntity::getCode, i->i));
  }

  private List<Node> findNodelist(Element elem, String expression) {
    XPath path = DocumentHelper.createXPath(expression);
    path.setNamespaceURIs(pathNamespaces);
    return path.selectNodes(elem);
  }

  private String getElementValue(Element elem, String expression) {
    if (expression == null) {
      return null;
    }
    XPath path = DocumentHelper.createXPath(expression);
    path.setNamespaceURIs(pathNamespaces);
    Node node = path.selectSingleNode(elem);
    if (node == null) {
      return null;
    }
    return node.getText();
  }

  protected ValidationResult validate(
      Node node, String version, ValidationContext context, final ValidationResult result) {
    List<BusinessValidator> validators = validatorFactory.getBusinessValidators(version, context);
    if (validators == null || validators.isEmpty()) {
      return ValidationResult.newOK();
    }
    ValidationResult _result = result == null ? new ValidationResult() : result;
    for (BusinessValidator validator : validators) {
      _result = validator.validate(node, _result);
    }
    return _result;
  }

  private Document createEntry(Node n, Document template) {
    XPath accountingEntriesPath = DocumentHelper.createXPath("xbrli:xbrl/gl-cor:accountingEntries");
    Map<String, String> pathNamespaces = new HashMap<>();
    pathNamespaces.put("xbrli", "http://www.xbrl.org/2003/instance");
    pathNamespaces.put("gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
    accountingEntriesPath.setNamespaceURIs(pathNamespaces);

    Document result = (Document)template.clone();
    Element parent = (Element)accountingEntriesPath.selectSingleNode(result);

    parent.add(((Element)n.clone()).detach());
    return result;
  }

  private Document createTemplate(Document doc, QName exclude) {
    Element elem = doc.getRootElement();
    QName rootName = elem.getQName();
    Element rootElem = factory.createElement(rootName);

    Document template = factory.createDocument(rootElem);
    Element tElem = template.getRootElement();
    tElem.addNamespace(elem.getNamespace().getPrefix(), elem.getNamespace().getURI());
    transformNamespaces(elem, tElem);
    transformAttributes(elem, tElem);
    transformContent(elem, tElem, exclude);
    return template;
  }

  private void addNew(Element template, Element oldElem, QName exclude) {
    Element ne = factory.createElement(oldElem.getQName());
    transformElement(oldElem, ne);
    transformContent(oldElem, ne, exclude);
    template.add(ne);
  }

  private boolean transformContent(Element oldElem, Element newElem, QName exclude) {
    for (Element oe : oldElem.elements()) {
      if (exclude != null && oe.getQName().equals(exclude)) {
        continue;
      }
      Element ne = factory.createElement(oe.getQName());
      transformElement(oe, ne);
      newElem.add(ne);
      if (oe.isTextOnly()) {
        ne.addText(oe.getText());
      } else {
        if (transformContent(oe, ne, null)) {
          return true;
        }
      }
    }
    return false;
  }

  private void transformElement(Element oldElem, Element newElem) {
    transformNamespaces(oldElem, newElem);
    transformAttributes(oldElem, newElem);
  }

  private void transformNamespaces(final Element oldElem, final Element newElem) {
    for (Namespace ns : oldElem.additionalNamespaces()) {
      newElem.addNamespace(ns.getPrefix(), ns.getURI());
    }
  }

  private void transformAttributes(final Element oldElem, final Element newElem) {
    for (Attribute atr : oldElem.attributes()) {
      newElem.addAttribute(atr.getQName(), atr.getValue());
    }
  }

  public String prepareSourceJournalId(String xml) throws Exception {
    if (xml == null) {
      return null;
    }

    Document doc =  DocumentHelper.parseText(xml);
    Element root = doc.getRootElement();
    String sjId = getElementValue(root, "gl-cor:accountingEntries/gl-cor:entryHeader/gl-cor:sourceJournalID");
    return sjId;
  }

}
