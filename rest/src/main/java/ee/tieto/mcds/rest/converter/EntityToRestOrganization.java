/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.Organization;
import ee.tieto.mcds.rest.repository.OrganizationRepository;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.stream.Collectors;

@Component
public class EntityToRestOrganization extends Converter<OrganizationEntity, Organization> {

  @Override
  public Organization convert(OrganizationEntity source, Organization destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setRegNr(source.getRegnr());
    destination.setRegAuthority(source.getRegAuthority());
    destination.setName(source.getName());
    destination.setAddress(source.getAddress());
    destination.setContact(source.getContactDetails());
    destination.setActive(source.getDeactivated() == null
        || source.getDeactivated().compareTo(OffsetDateTime.now()) > 0);
    destination.setReportingRequirements(source.getOrganizationReportTypes().stream()
        .map(t -> t.getReportType().getCode()).collect(Collectors.toSet()));

    return destination;
  }
}
