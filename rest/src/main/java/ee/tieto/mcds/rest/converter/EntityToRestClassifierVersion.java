/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.ClassifierItem;
import ee.tieto.mcds.rest.model.ClassifierVersion;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierVersionEntity;
import org.springframework.stereotype.Component;

@Component
public class EntityToRestClassifierVersion extends Converter<ClassifierVersionEntity, ClassifierVersion> {

  private final Converter<ClassifierItemEntity, ClassifierItem> itemConverter;

  public EntityToRestClassifierVersion(Converter<ClassifierItemEntity, ClassifierItem> itemConverter) {
    this.itemConverter = itemConverter;
  }

  @Override
  public ClassifierVersion convert(ClassifierVersionEntity source, ClassifierVersion destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setCode(source.getCode());
    destination.setDescription(source.getDescription());
    destination.setName(source.getName());
    destination.setValid(source.getValid());
    destination.setValidFrom(source.getValidFrom());
    destination.setValidUntil(source.getValidUntil());

    if (iterate) {
      destination.setItems(itemConverter.convert(source.getClassifierItems(), true));
    }

    return destination;
  }

}
