/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.EventLog;
import ee.tieto.mcds.rest.service.EventLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.time.OffsetDateTime;
import java.util.List;

@RestController
@RequestMapping("/manage")
public class EventLogController {

  private final EventLogService elogService;

  @Autowired
  public EventLogController(EventLogService elogService) {
    this.elogService = elogService;
  }

  @GetMapping(value = "/eventLog", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public List<EventLog> getEvents(
          @RequestParam(name = "startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime startTime,
          @RequestParam(name = "endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)OffsetDateTime endTime,
          @RequestParam(name = "success", required = false) Boolean success,
          @RequestParam(name = "eventType", required = false) List<String> eventType) {
    try {
      List<EventLog> events = elogService.findEvents(startTime, endTime, success, eventType);
      return events;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }
}
