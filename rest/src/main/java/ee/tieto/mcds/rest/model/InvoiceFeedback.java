/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

import ee.tieto.mcds.rest.repository.constants.InvoiceStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

public class InvoiceFeedback {
  @NotBlank
  private String entryIdentifier;
  @NotBlank
  private String xml;
  @NotNull
  private InvoiceStatus status;
  @NotNull
  private OffsetDateTime generationTime;
  private String comment;

  public String getEntryIdentifier() {
    return entryIdentifier;
  }

  public void setEntryIdentifier(String uuid) {
    this.entryIdentifier = uuid;
  }

  public String getXml() {
    return xml;
  }

  public void setXml(String xml) {
    this.xml = xml;
  }

  public InvoiceStatus getStatus() {
    return status;
  }

  public void setStatus(InvoiceStatus status) {
    this.status = status;
  }

  public OffsetDateTime getGenerationTime() {
    return generationTime;
  }

  public void setGenerationTime(OffsetDateTime generationTime) {
    this.generationTime = generationTime;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }
}
