/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

import ee.tieto.mcds.validator.ValidationResult.Severity;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class PackageStatus {

  private String sessionId;
  private OffsetDateTime timeOfReceiving;
  private String xbrlGlVersion;
  private ee.tieto.mcds.rest.repository.constants.PackageStatus status;
  private List<Error> errors;
  private List<EntryInfo> entryInfo;

  public void addError(Severity severity, String message) {
    if (this.errors == null) {
      this.errors = new ArrayList<>();
    }
    this.errors.add(new Error(severity, message));
  }

  public void addEntryInfo(EntryInfo info) {
    if (this.entryInfo == null) {
      this.entryInfo = new ArrayList<>();
    }
    this.entryInfo.add(info);
  }

  public String getSessionId() {
    return this.sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public OffsetDateTime getTimeOfReceiving() {
    return timeOfReceiving;
  }

  public void setTimeOfReceiving(OffsetDateTime timeOfReceiving) {
    this.timeOfReceiving = timeOfReceiving;
  }

  public String getXbrlGlVersion() {
    return xbrlGlVersion;
  }

  public void setXbrlGlVersion(String xbrlGlVersion) {
    this.xbrlGlVersion = xbrlGlVersion;
  }

  public ee.tieto.mcds.rest.repository.constants.PackageStatus getStatus() {
    return status;
  }

  public void setStatus(ee.tieto.mcds.rest.repository.constants.PackageStatus status) {
    this.status = status;
  }

  public List<Error> getErrors() {
    return errors;
  }

  public void setErrors(List<Error> errors) {
    this.errors = errors;
  }

  public List<EntryInfo> getEntryInfo() {
    return entryInfo;
  }

  public void setEntryInfo(List<EntryInfo> entryInfo) {
    this.entryInfo = entryInfo;
  }

  private static final class Error {
    private Severity severity;
    private String message;

    private Error(Severity severity, String message) {
      this.severity = severity;
      this.message = message;
    }

    public Severity getSeverity() {
      return severity;
    }

    public void setSeverity(Severity severity) {
      this.severity = severity;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
