/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.rest.McdsDataIntegrityException;
import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.converter.Converter;
import ee.tieto.mcds.rest.model.Organization;
import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.OrganizationReportTypeRepository;
import ee.tieto.mcds.rest.repository.OrganizationRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.repository.model.OrganizationReportTypeEntity;
import ee.tieto.mcds.rest.service.AbstractService;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.service.OrganizationSerice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrganizationServiceImpl extends AbstractService implements OrganizationSerice {

  private final OrganizationRepository repo;
  private final OrganizationReportTypeRepository repTypeRepo;
  private final ClassifierItemRepository clItemRepo;
  private final Converter<OrganizationEntity, Organization> orgConverter;
  private final Converter<Organization, OrganizationEntity> orgToEntConverter;
  private final EventLogService eventService;

  @Autowired
  public OrganizationServiceImpl(OrganizationRepository repo,
                                 OrganizationReportTypeRepository repTypeRepo,
                                 ClassifierItemRepository clItemRepo,
                                 Converter<OrganizationEntity, Organization> orgConverter,
                                 Converter<Organization, OrganizationEntity> orgToEntConverter,
                                 EventLogService eventService) {
    this.repo = repo;
    this.repTypeRepo = repTypeRepo;
    this.clItemRepo = clItemRepo;
    this.orgConverter = orgConverter;
    this.orgToEntConverter = orgToEntConverter;
    this.eventService = eventService;
  }

  @Override
  @Transactional
  public List<Organization> findClients(String regNr) {
    //TODO If refNr != null && entities more than 1, throw error
    List<OrganizationEntity> entities = repo.findOrganization(regNr);
    return orgConverter.convert(entities);
  }

  @Override
  @Transactional
  public void save(List<Organization> organizations) {
    if (organizations == null || organizations.isEmpty()) {
      return;
    }

    organizations.forEach(this::save);
  }

  private void save(Organization organization) {

    final Set<String> newRepTypes = organization.getReportingRequirements() == null ?
        new HashSet<>(0)
        : organization.getReportingRequirements();

    final Map<String, ClassifierItemEntity> reportTypes =
            clItemRepo.findClassifierItemsByClassifierCode(Classifier.REPORT_TYPE.name())
            .stream()
            .filter(t -> newRepTypes.contains(t.getCode()))
            .collect(Collectors.toMap(ClassifierItemEntity::getCode, item -> item));

    //Load or create OrganizationEntity
    List<OrganizationEntity> orgEntities = repo.findOrganization(organization.getRegNr());
    if (orgEntities.size() > 1) {
      throw new McdsDataIntegrityException("Organization by regNr - expected one, found " + orgEntities.size());
    }
    OrganizationEntity org;
    if (orgEntities.isEmpty()) {
      org = new OrganizationEntity();
      org.setOrganizationReportTypes(new HashSet<>());
    } else {
      org = orgEntities.get(0);
    }

    final OrganizationEntity orgEntity = repo.save(orgToEntConverter.convert(organization, org, false));

    final Map<String, OrganizationReportTypeEntity> actualRepTypes = orgEntity.getOrganizationReportTypes()
        .stream().collect(Collectors.toMap(c -> c.getReportType().getCode(), c -> c));

    actualRepTypes.forEach((key, value) -> {
      if (organization.isActive() && newRepTypes.contains(key)) {
        value.setDeactivated(null);
      } else {
        if (value.getDeactivated() == null) {
          value.setDeactivated(OffsetDateTime.now());
        }
      }
    });

    for (String t : newRepTypes) {
      ClassifierItemEntity newTypeRef = reportTypes.get(t);
      if (newTypeRef == null) {
        throw new McdsValidationException("Unknown report type - " + t);
      }
      if (!actualRepTypes.containsKey(t)) {
        OrganizationReportTypeEntity newType = new OrganizationReportTypeEntity();
        newType.setOrganization(orgEntity);
        newType.setReportType(newTypeRef);
        if (!organization.isActive()) {
          newType.setDeactivated(OffsetDateTime.now());
        }
        repTypeRepo.save(newType);
        actualRepTypes.put(t, newType);
      }
    }

    repo.flush();
    eventService.newEvent(
        EventType.ORGANIZATION_SAVE, getUser(), EventType.ORGANIZATION_SAVE.getTable() + ":" + org.getId(), true, null);

  }

}
