/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.rest.McdsNotFoundException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.Permission;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/manage")
@Validated
public class PermissionsController {

  private final PermissionService permServ;
  private final EventLogService elogService;

  @Autowired
  public PermissionsController(PermissionService permServ, EventLogService elogService) {
    this.permServ = permServ;
    this.elogService = elogService;
  }

  @PostMapping("/permissions")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public ResponseEntity<Void> savePermissions(@RequestBody @Valid List<Permission> permissions) {
    try {
      permServ.savePermissions(permissions);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      if (e instanceof McdsException) {
        throw e;
      }
      throw new McdsSystemException("Unexpected system error", e);
    }
  }


  @GetMapping(value = "/permissions", produces = "application/json;charset=UTF-8")
  @ResponseBody
  @PreAuthorize("hasRole('ROLE_MANAGER')")
  public List<Permission> getPermissions(
      @RequestParam(name = "erpCode", required = false) String erpCode,
      @RequestParam(name = "organizationRegNr", required = false) String orgRegNr,
      @RequestParam(name = "businessArea", required = false) String businessArea,
      @RequestParam(name = "reportTypeCode", required = false) String reportTypeCode) {
    try {
      List<Permission> permissions = permServ.findPermissions(erpCode, orgRegNr, businessArea, reportTypeCode);
      if (permissions.isEmpty()) {
        throw new McdsNotFoundException();
      }
      return permissions;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }
}
