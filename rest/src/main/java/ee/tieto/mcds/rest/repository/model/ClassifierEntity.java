/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "classifier", schema = "mcds", catalog = "mcds")
public class ClassifierEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_orga")
    private OrganizationEntity organization;

    @OneToMany(mappedBy = "classifier", fetch = FetchType.LAZY)
    private List<ClassifierVersionEntity> classifierVersions;

    @OneToMany(mappedBy = "classifier", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ClassifierItemEntity> classifierItems;

    @OneToMany(mappedBy = "classifier", fetch = FetchType.LAZY)
    private List<ClassifierAttributeEntity> classifierAttributes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OrganizationEntity getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationEntity organization) {
        this.organization = organization;
    }

    public List<ClassifierVersionEntity> getClassifierVersions() {
        return this.classifierVersions;
    }

    public void setClassifierVersions(List<ClassifierVersionEntity> classifierVersions) {
        this.classifierVersions = classifierVersions;
    }

    public List<ClassifierItemEntity> getClassifierItems() {
        return this.classifierItems;
    }

    public void setClassifierItems(List<ClassifierItemEntity> classifieritems) {
        this.classifierItems = classifieritems;
    }

    public List<ClassifierAttributeEntity> getClassifierAttributes() {
        return classifierAttributes;
    }

    public void setClassifierAttributes(List<ClassifierAttributeEntity> classifierAttributes) {
        this.classifierAttributes = classifierAttributes;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ClassifierEntity))
            return false;

        ClassifierEntity other = (ClassifierEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
