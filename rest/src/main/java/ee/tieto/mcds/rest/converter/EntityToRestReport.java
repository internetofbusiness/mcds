/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.Report;
import ee.tieto.mcds.rest.model.ReportStatus;
import ee.tieto.mcds.rest.repository.model.ReportEntity;
import ee.tieto.mcds.rest.repository.model.ReportStatusEntity;
import org.springframework.stereotype.Component;

@Component
public class EntityToRestReport extends Converter<ReportEntity, Report> {

  private Converter<ReportStatusEntity, ReportStatus> statusConverter;

  public EntityToRestReport(Converter<ReportStatusEntity, ReportStatus> statusConverter) {
    this.statusConverter = statusConverter;
  }

  @Override
  public Report convert(ReportEntity source, Report destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setId(source.getReportUuid());
    ReportStatusEntity status = source.getReportStatuses().get(0);
    if (iterate) {
      destination.setStatuses(statusConverter.convert(source.getReportStatuses()));
    }
    destination.setRegNr(source.getOrganization().getRegnr());
    destination.setReportType(source.getReportType().getCode());
    destination.setDeadline(source.getDeadline());

    return destination;
  }
}
