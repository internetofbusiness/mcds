/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.rest.service.XbrlService;
import ee.tieto.mcds.validator.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/debug")
public class DebugController {

  @Autowired
  private XbrlService xbrlService;

  @GetMapping("/processXbrl")
  public ResponseEntity<ValidationResult> processXbrlPackage(@RequestParam(name = "uuid") String uuid) {
    ValidationResult result = xbrlService.process(uuid);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping("/prepareSJIds")
  public void saveSourceJournal() {
    xbrlService.saveSourceJournal();
  }

  @GetMapping("/headers")
  public Map<String, String> getHeaders(@RequestHeader Map<String, String> headers) {
    return headers;
  }
}
