/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.config;

import ee.tieto.mcds.validator.XbrlValidatorFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ValidatorFactoryInitializer {

  private final XbrlValidatorFactory factory;

  public ValidatorFactoryInitializer(XbrlValidatorFactory factory) {
    this.factory = factory;
  }

  @PostConstruct
  private void init() {
    this.factory.init();
  }
}
