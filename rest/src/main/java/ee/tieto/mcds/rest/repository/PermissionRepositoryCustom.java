/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository;

import ee.tieto.mcds.rest.repository.model.PermissionEntity;

import java.time.OffsetDateTime;
import java.util.List;

public interface PermissionRepositoryCustom {

  List<PermissionEntity> findPermissionsIgnoreNulls(String erpCode, String orgRegNr, String businessArea, String reportTypeCode, boolean findOnlyActivePermissions);
  List<PermissionEntity> findPermissionsUseNulls (String erpCode, String orgRegNr, String businessArea, String reportTypeCode);
  List<PermissionEntity> findByErpCodeAndDate(String erpCode, OffsetDateTime dateTime);
  boolean isPermittedForErp(String erpCode, String regNr, String businessArea, OffsetDateTime time, boolean ignoreBusinessArea);

}
