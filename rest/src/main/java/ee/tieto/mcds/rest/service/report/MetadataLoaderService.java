/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.service.report.model.ReportMetadata;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MetadataLoaderService {

  private final ClassifierItemRepository clItemRepo;

  public MetadataLoaderService(ClassifierItemRepository clItemRepo) {
    this.clItemRepo = clItemRepo;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public List<ReportMetadata> loadReportMetadata(String version) {
    List<ClassifierItemEntity> clItems = clItemRepo.findByClassifierCodeAndVersionCodeAndDate(
        version, Classifier.REPORT_TYPE.name(), OffsetDateTime.now().toLocalDate());

    return clItems.stream()
        .map(ReportMetadata::fromEntity)
        .filter(rm-> !rm.isEmpty())
        .map(rm -> rm.get())
        .collect(Collectors.toList());
  }

}
