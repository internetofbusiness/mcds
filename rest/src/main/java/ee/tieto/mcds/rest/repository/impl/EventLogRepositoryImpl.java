/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.impl;

import ee.tieto.mcds.rest.repository.EventLogRepositoryCustom;
import ee.tieto.mcds.rest.repository.ParameterSetter;
import ee.tieto.mcds.rest.repository.model.EventLogEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EventLogRepositoryImpl implements EventLogRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<EventLogEntity> findEvents(OffsetDateTime startTime, OffsetDateTime endTime, Boolean success, List<String> eventType) {
        String query = "SELECT e FROM EventLogEntity e WHERE e.created >= :startTime AND e.created <= :endTime";

        Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        if (success != null) {
            query += " AND e.success = :success";
            params.put("success", success);
        }
        if (eventType != null && !eventType.isEmpty()){
            StringBuilder sb = new StringBuilder();
            sb.append(" AND e.eventType.code IN (");
            for (String type : eventType) {
                sb.append("'").append(type).append("'").append(",");
            }
            sb.setLength(sb.length() - 1);
            sb.append(")");
            query += sb.toString();
        }

        TypedQuery<EventLogEntity> qu = entityManager.createQuery(query, EventLogEntity.class);
        qu = ParameterSetter.setParameters(qu, params);
        return qu.getResultList();
    }
}
