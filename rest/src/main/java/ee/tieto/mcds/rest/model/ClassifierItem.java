/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

import java.time.LocalDate;
import java.util.List;

public class ClassifierItem {
  private String code;
  private String name;
  private LocalDate validFrom;
  private LocalDate validUntil;
  private Integer seqNr;
  private String upperClassifierItemCode;
  private List<ClassifierAttribute> classifierAttributes;
  private List<ClassifierItemLink> links;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDate validFrom) {
    this.validFrom = validFrom;
  }

  public LocalDate getValidUntil() {
    return validUntil;
  }

  public void setValidUntil(LocalDate validUntil) {
    this.validUntil = validUntil;
  }

  public Integer getSeqNr() {
    return seqNr;
  }

  public void setSeqNr(Integer seqNr) {
    this.seqNr = seqNr;
  }

  public String getUpperClassifierItemCode() {
    return upperClassifierItemCode;
  }

  public void setUpperClassifierItemCode(String upperClassifierItemCode) {
    this.upperClassifierItemCode = upperClassifierItemCode;
  }

  public List<ClassifierAttribute> getClassifierAttributes() {
    return classifierAttributes;
  }

  public void setClassifierAttributes(List<ClassifierAttribute> classifierAttributes) {
    this.classifierAttributes = classifierAttributes;
  }

  public List<ClassifierItemLink> getLinks() {
    return links;
  }

  public void setLinks(List<ClassifierItemLink> links) {
    this.links = links;
  }
}
