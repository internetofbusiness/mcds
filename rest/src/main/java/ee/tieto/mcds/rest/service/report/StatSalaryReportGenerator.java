/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.rest.repository.AccountingEntryRepository;
import ee.tieto.mcds.rest.repository.ClassifierRepository;
import ee.tieto.mcds.rest.repository.ReportRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.service.AbstractService;
import ee.tieto.mcds.rest.service.report.model.ReportDataCollector;
import ee.tieto.mcds.rest.service.report.model.ReportMetadata;
import ee.tieto.mcds.rest.service.report.model.StatSalaryReportMetadata;
import ee.tieto.mcds.rest.service.report.model.StatSalaryTemplate;
import org.dom4j.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;

@Component
public class StatSalaryReportGenerator extends AbstractService implements ReportGenerator {
  private static final Logger log = LoggerFactory.getLogger(StatSalaryReportGenerator.class);

  private final ReportRepository reportRepo;
  private final ReportConfig reportConfig;
  private final AccountingEntryRepository entryRepository;
  private final ClassifierRepository classifierRepository;

  public StatSalaryReportGenerator(
      ReportRepository reportRepo,
      ReportConfig reportConfig,
      AccountingEntryRepository entryRepository,
      ClassifierRepository classifierRepository) {
    this.reportRepo = reportRepo;
    this.reportConfig = reportConfig;
    this.entryRepository = entryRepository;
    this.classifierRepository = classifierRepository;
  }

  @Override
  @Transactional
  public void generate(OrganizationEntity org, ReportMetadata meta, boolean force) {
    if (!isMustGenerate(org, meta, force)) {
      log.debug("Report {} must not be generated fro organization {}", meta.getReportType().name(), org.getRegnr());
      return;
    }

    log.debug("Generating report {} fro organization {}", meta.getReportType().name(), org.getRegnr());
    try {
      Document template = StatSalaryTemplate.loadTemplate(reportConfig, (StatSalaryReportMetadata)meta);
      OffsetDateTime genTime = OffsetDateTime.now();
      String uId = StatSalaryTemplate.createId(org, genTime);
      StatSalaryTemplate.setContent(template, (StatSalaryReportMetadata)meta, org, uId, genTime);
      ReportDataCollector collector = generateReport(org, meta, template);
      collector.setReportId(uId);
      saveReport(org, meta, template, collector);
      eventService.newEvent(
          EventType.REPORT_GENERATE, getUser(),
          EventType.REPORT_GENERATE.getTable() + ":" + collector.getReportId(),
          true, "Report:" + meta.getReportType().name() + " Organization:" + org.getRegnr());
    } catch (Exception e) {
      log.error("Error generatin report {} fro organization {}", meta.getReportType().name(), org.getRegnr());
      eventService.newEventNewTransaction(
          EventType.REPORT_GENERATE, getUser(),
          EventType.REPORT_GENERATE.getTable() + ":null",
          false,
          "Report:" + meta.getReportType().name() +
              " Organization:" + org.getRegnr() + " ERROR:" + e.getMessage());
      throw e;
    }
  }

  private void saveReport(OrganizationEntity org, ReportMetadata meta, Document doc, ReportDataCollector collector) {
    ClassifierItemEntity reportType = classifierRepository.findClassifier(Classifier.REPORT_TYPE.name())
        .get(0).getClassifierItems()
        .stream().filter(rt -> rt.getCode().equals(meta.getReportType().name()))
        .findFirst().get();
    collector.setReportTypeId(reportType.getId());

    log.trace("SAVE report: id_org={}, repType_id={}, submissionDate={}, collector={}",
        org.getId(), reportType.getId(), meta.getSubmissionDate(), collector);
    reportRepo.saveReport(
        org.getId(),
        reportType.getId(),
        meta.getSubmissionDate(),
        meta.getPeriodStart(),
        meta.getPeriodEnd(),
        doc.asXML(),
        collector);
  }

  public ReportDataCollector generateReport(OrganizationEntity org, ReportMetadata meta, Document template) {
    ReportDataCollector collector = new ReportDataCollector();
    StatSalaryReportGeneratorHelper helper = new StatSalaryReportGeneratorHelper(entryRepository, classifierRepository, meta);
    return helper.generateReport(org, template, collector);
  }

  private boolean isMustGenerate(OrganizationEntity org, ReportMetadata meta, boolean force) {
    if (force) {
      log.trace("Must generate - force is true");
      return true;
    }
    OffsetDateTime genTime = reportRepo.findExistsGeneratedReportTime(org.getRegnr(), meta.getReportType(), meta.getPeriodStart(), meta.getPeriodEnd());
    if (genTime == null) {
      log.trace("Must generate - old report not found");
      return true;
    }
    boolean ret = reportRepo.isEntriesChangedInPeriod(org.getRegnr(), meta.getPeriodStart(), meta.getPeriodEnd(), genTime);
    if (ret) {
      log.trace("Must generate - entries added to period");
    }
    log.trace("Must not be generate - entries not added to period");
    return ret;
  }

}
