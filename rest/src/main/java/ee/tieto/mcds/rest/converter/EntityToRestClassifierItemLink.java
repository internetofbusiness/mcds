/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.ClassifierItemLink;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemLinkEntity;
import org.springframework.stereotype.Component;


@Component
public class EntityToRestClassifierItemLink extends Converter<ClassifierItemLinkEntity, ClassifierItemLink> {
  @Override
  public ClassifierItemLink convert(ClassifierItemLinkEntity source, ClassifierItemLink destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setDescription(source.getDescription());
    ClassifierItemEntity item;
    if (source.getClassifierItem1().getId().equals(source.getId())) {
      item = source.getClassifierItem2();
    } else {
      item = source.getClassifierItem1();
    }
    destination.setLinkedClassifierItemCode(item.getCode());
    destination.setLinkedClassifierCode(item.getClassifier().getCode());
    destination.setLinkType(source.getLinkType());

    return destination;
  }

}
