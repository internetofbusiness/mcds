/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.impl;

import ee.tieto.mcds.rest.repository.AccountingEntryRepositoryCustom;
import ee.tieto.mcds.rest.repository.ParameterSetter;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.model.AccountingEntryEntity;
import ee.tieto.mcds.rest.repository.model.AccountingEntryEntityLightReadOnly;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

@Repository
public class AccountingEntryRepositoryImpl implements AccountingEntryRepositoryCustom {

  @PersistenceContext
  private EntityManager entityManager;
  @Value("${mcds.target-application.invoice-operator:e-invoicing_operator}")
  private String targetApplicationInvoice;

  @Override
  public List<AccountingEntryEntity> findEntries(UUID sessionId, OffsetDateTime receivedFrom, OffsetDateTime receivedUntil, String entryIdentifier, String erpCode, List<String> regnrList) {
    String query = "SELECT e FROM AccountingEntryEntity e WHERE 1=1";

    StringBuilder sb = new StringBuilder();
    sb.append(" AND e.organization.regnr IN (");
    for (String regnr : regnrList) {
      sb.append("'").append(regnr).append("'").append(",");
    }
    sb.setLength(sb.length() - 1);
    sb.append(")");
    query += sb.toString();

    Map<String, Object> params = new HashMap<>();
    query += " AND e.pacage.erp.code = :erpCode";
    params.put("erpCode", erpCode);

    if (sessionId != null) {
      query += " AND e.pacage.packageUuid = :sessionId";
      params.put("sessionId", sessionId);
    }
    if (entryIdentifier != null) {
      query += " AND e.entryUuid = :entryIdentifier";
      params.put("entryIdentifier", entryIdentifier);
    }
    if (receivedFrom != null) {
      query += " AND e.pacage.created >= :receivedFrom";
      params.put("receivedFrom", receivedFrom);
    }
    if (receivedUntil != null) {
      query += " AND e.pacage.created <= :receivedUntil";
      params.put("receivedUntil", receivedUntil);
    }

    if (sessionId == null && receivedFrom == null && receivedUntil == null && entryIdentifier == null) {
      query += " AND e.created >= :currentMonth";
      OffsetDateTime currentMonth = OffsetDateTime.now().truncatedTo(ChronoUnit.DAYS).withDayOfMonth(1);
      params.put("currentMonth", currentMonth);
    }

    TypedQuery<AccountingEntryEntity> qu = entityManager.createQuery(query, AccountingEntryEntity.class);
    qu = ParameterSetter.setParameters(qu, params);
    return qu.getResultList();
  }


  @Override
  public List<AccountingEntryEntity> findEntries(String erpCode, String regNr, OffsetDateTime from, OffsetDateTime until,
                                                 String entryType, Set<String> entryIdentifiers, OffsetDateTime findTime) {

    StringBuilder select = new StringBuilder(
        "select ae " +
        "from " +
            "AccountingEntryEntity ae " +
            "join PackageEntity p on p = ae.pacage " +
            "join ClassifierItemEntity erp on erp = p.erp " +
            "join OrganizationEntity org on org = ae.organization ");
    StringBuilder where = new StringBuilder(
        "where " +
            "erp.code = :erpCode " +
            "and org.regnr = :regNr " +
            "and exists (" +
              "select p from " +
                "PermissionEntity p " +
                "join ClassifierItemEntity pe on pe = p.erp " +
              "where " +
                "(p.deactivated is null or p.deactivated > :findTime) " +
                "and p.organization = org " +
                "and p.reportType is null " +
                "and p.erp = erp " +
                "and (p.businessArea is null or p.businessArea = ae.businessArea) " +
            ") ");

    Map<String, Object> params = new HashMap<>();
    params.put("erpCode", erpCode);
    params.put("regNr", regNr);
    params.put("findTime", findTime);

    if (from != null) {
      where.append("and ae.created >= :fromDate ");
      params.put("fromDate", from);
    }
    if (until != null) {
      where.append("and ae.created < :untilDate ");
      params.put("untilDate", until);
    }

    if (entryType != null) {
      select.append("join ClassifierItemEntity et on et = ae.entryType ")
      .append("join ClassifierVersionEntity cv on cv = et.classifierVersion ");
      where.append("and (et.code = :entryType and cv.code = ae.rulesName) ");
      params.put("entryType", entryType);
    }
    if (entryIdentifiers != null && !entryIdentifiers.isEmpty()) {
      where.append("and ae.entryUuid in (:entryIdentifiers) ");
      params.put("entryIdentifiers", entryIdentifiers);
    }

    String query = select.append(where).toString();
    TypedQuery<AccountingEntryEntity> qu = entityManager.createQuery(query, AccountingEntryEntity.class);
    qu = ParameterSetter.setParameters(qu, params);
    List<AccountingEntryEntity> ret = qu.getResultList();
    return ret;
  }

  @Override
  public List<AccountingEntryEntity> findInvoiceEntries(String regNr, OffsetDateTime from, OffsetDateTime until,
                                                 String entryType, Set<String> entryIdentifiers, OffsetDateTime findTime) {

    StringBuilder select = new StringBuilder(
        "select ae " +
            "from " +
            "AccountingEntryEntity ae " +
            "join PackageEntity p on p = ae.pacage " +
            "join OrganizationEntity org on org = ae.organization ");
    StringBuilder where = new StringBuilder(
        "where " +
            "ae.targetApplication = :targetApplicationInvoice " +
            "and ae.valid = true ");

    Map<String, Object> params = new HashMap<>();
    params.put("targetApplicationInvoice", targetApplicationInvoice);

    if (from != null) {
      where.append("and ae.created >= :fromDate ");
      params.put("fromDate", from);
    }
    if (until != null) {
      where.append("and ae.created <= :untilDate ");
      params.put("untilDate", until);
    }

    if (regNr != null) {
      where.append("and org.regnr = :regNr ");
      params.put("regNr", regNr);
    }
    if (entryType != null) {
      select.append("join ClassifierItemEntity et on et = ae.entryType ")
          .append("join ClassifierVersionEntity cv on cv = et.classifierVersion ");
      where.append("and (et.code = :entryType and cv.code = ae.rulesName) ");
      params.put("entryType", entryType);
    }
    if (entryIdentifiers != null && !entryIdentifiers.isEmpty()) {
      where.append("and ae.entryUuid in (:entryIdentifiers) ");
      params.put("entryIdentifiers", entryIdentifiers);
    }

    String query = select.append(where).toString();
    TypedQuery<AccountingEntryEntity> qu = entityManager.createQuery(query, AccountingEntryEntity.class);
    qu = ParameterSetter.setParameters(qu, params);
    List<AccountingEntryEntity> ret = qu.getResultList();
    return ret;
  }

  @Override
  public List<AccountingEntryEntityLightReadOnly> findEntries(String erpCode, UUID sessionId, String entryId, OffsetDateTime from, OffsetDateTime until, OffsetDateTime findTime) {
    String select =
        "with erp_rights as (" +
            "select " +
              "p.id_orga as reg_nr, p.business_area business_area " +
            "from " +
              "\"permission\" p " +
              "join classifier_item ci on ci.id = p.erp " +
              "join classifier c2 on c2.id = ci.id_class " +
            "where " +
              "c2.code = :clName " +
              "and ci.code = :erpCode " +
              "and p.report_type is null " +
              "and (p.deactivated is null or p.deactivated > :findTime) " +
        ") " +
        "select " +
          "ae.id, ae.id_pack, ae.id_orga, ae.entry_type, ae.valid, ae.rules_name, ae.entry_uuid, ae.created, ae.source_journal_id, ae.target_application " +
        "from " +
          "accounting_entry ae " +
          "join package p on p.id = ae.id_pack " +
        "where " +
          "exists (" +
            "select 1 from erp_rights er " +
            "where " +
            "ae.id_orga = er.reg_nr and (er.business_area is null or er.business_area = ae.business_area)) ";

    Map<String, Object> params = new HashMap<>();
    params.put("clName", Classifier.ERP.name());
    params.put("erpCode", erpCode);
    params.put("findTime", findTime);

    if (sessionId != null) {
      select += "and p.package_uuid = :sessionId ";
      params.put("sessionId", sessionId);
    }
    if (entryId != null) {
      select += "and ae.entry_uuid = :entryId ";
      params.put("entryId", entryId);
    }
    if (from != null) {
      select += "and ae.created >= :fromDate ";
      params.put("fromDate", from);
    }
    if (until != null) {
      select += "and ae.created <= :untilDate ";
      params.put("untilDate", until);
    }

    Query qu = entityManager.createNativeQuery(select, AccountingEntryEntityLightReadOnly.class);
    ParameterSetter.setParameters(qu, params);

    @SuppressWarnings("unchecked")
    List<AccountingEntryEntityLightReadOnly> entryes = qu.getResultList();
    return entryes;
  }

  @Override
  public List<AccountingEntryEntityLightReadOnly> findInvoiceEntries(UUID sessionId, String entryId,
                                                              OffsetDateTime from, OffsetDateTime until) {
    String select =
            "select " +
              "ae.id, ae.id_pack, ae.id_orga, ae.entry_type, ae.valid, ae.rules_name, ae.entry_uuid, ae.created, ae.source_journal_id, ae.target_application " +
            "from " +
              "accounting_entry ae " +
              "join package p on p.id = ae.id_pack " +
            "where " +
              "ae.target_application = 'e-invoicing_operator' " +
              "and ae.valid = true ";

    Map<String, Object> params = new HashMap<>();

    if (sessionId != null) {
      select += "and p.package_uuid = :sessionId ";
      params.put("sessionId", sessionId);
    }
    if (entryId != null) {
      select += "and ae.entry_uuid = :entryId ";
      params.put("entryId", entryId);
    }
    if (from != null){
      select += "and ae.created >= :fromDate ";
      params.put("fromDate", from);
    }
    if (until != null) {
      select += "and ae.created <= :untilDate ";
      params.put("untilDate", until);
    }

    Query qu = entityManager.createNativeQuery(select, AccountingEntryEntityLightReadOnly.class);
    ParameterSetter.setParameters(qu, params);

    @SuppressWarnings("unchecked")
    List<AccountingEntryEntityLightReadOnly> entryes = qu.getResultList();
    return entryes;
  }


  @Override
  public Stream<AccountingEntryEntity> findOrganizationEntriesInPostingPeriod(String regNr, LocalDate fromDate, LocalDate untilDate) {
    String select2 =
        "select " +
          "ae.* " +
        "from " +
          "accounting_entry ae " +
          "join organization o2 on o2.id = ae.id_orga " +
        "where " +
          "o2.regnr = :regNr " +
          "and exists ( " +
            "select aed.id from accounting_entry_detail aed " +
            "where " +
              "aed.id_acen = ae.id " +
              "and aed.posting_date >= :fromDate " +
              "and aed.posting_date <= :untilDate " +
          ")";

    @SuppressWarnings("unchecked")
    Stream<AccountingEntryEntity> resultStream = (Stream<AccountingEntryEntity>)entityManager.createNativeQuery(select2, AccountingEntryEntity.class)
        .setHint("org.hibernate.fetchSize", "1")
        .setHint("org.hibernate.cacheable", "false")
        .setHint("org.hibernate.readOnly", "true")
        .setParameter("regNr", regNr)
        .setParameter("fromDate", fromDate)
        .setParameter("untilDate", untilDate)
        .getResultStream();
    return resultStream;
  }

  public void detach(AccountingEntryEntity entity) {
    this.entityManager.detach(entity);
  }

  //DEBUG
  public Stream<AccountingEntryEntity> findAllCustom() {
    return entityManager.createQuery("select ae from AccountingEntryEntity ae", AccountingEntryEntity.class)
        .getResultStream();
  }

}
