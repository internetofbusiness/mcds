/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.impl;

import ee.tieto.mcds.rest.repository.ParameterSetter;
import ee.tieto.mcds.rest.repository.ReportRepositoryCustom;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.ReportStatus;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.repository.model.ReportEntity;
import ee.tieto.mcds.rest.repository.model.ReportEntityLightReadOnly;
import ee.tieto.mcds.rest.repository.model.ReportStatusEntity;
import ee.tieto.mcds.rest.service.report.model.ReportDataCollector;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.*;
import java.util.*;

@Repository
public class ReportRepositoryImpl implements ReportRepositoryCustom {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<ReportEntity> findReportsIgnoreNulls(List<String> regnrList, ReportStatus status, String reportType, LocalDate startDate, LocalDate endDate) {
    String query = "SELECT r FROM ReportEntity r WHERE r.organization.regnr IN :regnrList ";

    Map<String, Object> params = new HashMap<>();
    params.put("regnrList", regnrList);

    if (status != null) {
      query += " AND (SELECT s.status FROM r.reportStatuses s WHERE s.created = (SELECT MAX(s.created) FROM r.reportStatuses s)) = :status";
      params.put("status", status);
    }
    if (reportType != null) {
      query += " AND r.reportType.code = :reportType AND r.reportType.classifier.code = '" + Classifier.REPORT_TYPE.name() + "' ";
      params.put("reportType", reportType);
    }
    if (startDate != null) {
      query += " AND r.deadline >= :startDate";
      params.put("startDate", startDate);
    }
    if (endDate != null) {
      query += " AND r.deadline <= :endDate";
      params.put("endDate", endDate);
    }

    TypedQuery<ReportEntity> qu = entityManager.createQuery(query, ReportEntity.class);
    qu = ParameterSetter.setParameters(qu, params);
    return qu.getResultList();
  }

  @Override
  public OffsetDateTime findExistsGeneratedReportTime(String regNr, ReportType reportType, LocalDate periodFrom, LocalDate periodUntil) {
    Set<ReportStatus> allowedStatuses =
        Set.of(ReportStatus.GENERATED, ReportStatus.SUBMITED, ReportStatus.SUBMITED_NOT_VALID);

    String query =
        "select r " +
        "from " +
            "ReportEntityLightReadOnly r " +
            "join OrganizationEntity  o on o = r.organization " +
            "join ClassifierItemEntity rType on rType = r.reportType " +
        "where " +
            " o.regnr = :regNr " +
            "and rType.code = :reportType " +
            "and r.periodFrom = :periodFrom and r.periodUntil = :periodUntil " +
        "order by r.created desc ";


    List<ReportEntityLightReadOnly> reports = entityManager.createQuery(query, ReportEntityLightReadOnly.class)
      .setParameter("regNr", regNr)
      .setParameter("reportType", reportType.name())
      .setParameter("periodFrom", periodFrom)
      .setParameter("periodUntil", periodUntil)
      .getResultList();
    if (reports == null || reports.isEmpty()) {
      return null;
    }
    List<ReportStatusEntity> statuses = reports.get(0).getReportStatuses();
    if (statuses == null || statuses.isEmpty()) {
      return null;
    }
    if (allowedStatuses.contains(statuses.get(0).getStatus())) {
      return reports.get(0).getCreated();
    }
    return null;
  }

  @Override
  public boolean isEntriesChangedInPeriod(String regNr, LocalDate periodFrom, LocalDate periodUntil, OffsetDateTime lastGenerationTime) {
    String select =
        "select ae.id " +
        "from " +
            "OrganizationEntity o " +
            "join AccountingEntryEntityLightReadOnly ae on ae.organization = o " +
            "join AccountingEntryDetailEntity aed on aed.accountingEntry = ae " +
        "where " +
            "o.regnr = :regNr " +
            "and ae.created > :lastGenerationTime and aed.postingDate >= :periodFrom and aed.postingDate <= :periodUntil ";

    List<?> result = entityManager.createQuery(select)
        .setParameter("regNr", regNr)
        .setParameter("lastGenerationTime", lastGenerationTime)
        .setParameter("periodFrom", periodFrom)
        .setParameter("periodUntil", periodUntil)
        .getResultList();
    return result != null && !result.isEmpty();
  }

  @Override
  public void saveReport(Integer id_orga, Integer report_type, LocalDate deadline,
                         LocalDate periodFrom, LocalDate periodUntil,
                         String content, ReportDataCollector collector) {

    String insertReport = "insert into mcds.report (report_uuid, id_orga, report_type, deadline, content, period_from, period_until) " +
        "values (:report_uuid, :id_orga, :report_type, :decline, :content, :periodFrom, :periodUntil) " +
        "returning id";
    Integer reportId = (Integer)entityManager.createNativeQuery(insertReport)
        .setParameter("report_uuid", collector.getReportId())
        .setParameter("id_orga", id_orga)
        .setParameter("report_type", report_type)
        .setParameter("decline", deadline)
        .setParameter("content", content)
        .setParameter("periodFrom", periodFrom)
        .setParameter("periodUntil", periodUntil)
        .getSingleResult();

    String insertReportEntry = "insert into mcds.report_entry (id_repo, id_acen) " +
        "values (:id_repo, :id_acen)";
    int counter = 0;
    for (Integer idAcen : collector.getEntryIds()) {
      counter++;
      entityManager.createNativeQuery(insertReportEntry)
          .setParameter("id_repo", reportId)
          .setParameter("id_acen", idAcen)
          .executeUpdate();
      if (counter > 20) {
        entityManager.flush();
      }
    }

    String insertStatus = "insert into mcds.report_status " +
        "(id_repo, status, info) " +
        "values (:id_repo, :status, :info)";
    entityManager.createNativeQuery(insertStatus)
        .setParameter("id_repo", reportId)
        .setParameter("status", collector.getReportStatus().name())
        .setParameter("info", collector.getInformation())
        .executeUpdate();
  }

  @Override
  public void refresh(ReportEntity entity) {
    entityManager.refresh(entity);
  }
}
