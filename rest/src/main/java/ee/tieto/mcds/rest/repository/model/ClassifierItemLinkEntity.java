/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "classifier_item_link", schema = "mcds", catalog = "mcds")
public class ClassifierItemLinkEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "id_clit1", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassifierItemEntity classifierItem1;

    @JoinColumn(name = "id_clit2", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassifierItemEntity classifierItem2;

    @Column(name = "link_type")
    private String linkType;

    @Basic
    @Column(name = "description")
    private String description;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClassifierItemEntity getClassifierItem1() {
        return classifierItem1;
    }

    public void setClassifierItem1(ClassifierItemEntity classifierItem1) {
        this.classifierItem1 = classifierItem1;
    }

    public ClassifierItemEntity getClassifierItem2() {
        return classifierItem2;
    }

    public void setClassifierItem2(ClassifierItemEntity classifierItem2) {
        this.classifierItem2 = classifierItem2;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ClassifierItemLinkEntity))
            return false;

        ClassifierItemLinkEntity other = (ClassifierItemLinkEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
