/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service;

import ee.tieto.mcds.rest.model.EventLog;
import ee.tieto.mcds.rest.repository.constants.EventType;

import java.time.OffsetDateTime;
import java.util.List;

public interface EventLogService {

  void newEvent(EventType eventType, String erpCode, String relatedRecords, Boolean success, String message);
  void newEventNewTransaction(EventType eventType, String erpCode, String relatedRecords, Boolean success, String message);
  List<EventLog> findEvents(OffsetDateTime startTime, OffsetDateTime endTime, Boolean success, List<String> eventType);
}
