/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.Classifier;
import ee.tieto.mcds.rest.model.ClassifierVersion;
import ee.tieto.mcds.rest.repository.model.ClassifierEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierVersionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class EntityToRestClassifier extends Converter<ClassifierEntity, Classifier> {

  private final Converter<ClassifierVersionEntity, ClassifierVersion> versionConverter;

  @Autowired
  public EntityToRestClassifier(Converter<ClassifierVersionEntity, ClassifierVersion> versionConverter) {
    this.versionConverter = versionConverter;
  }

  @Override
  public Classifier convert(ClassifierEntity source, Classifier destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setCode(source.getCode());
    destination.setDescription(source.getDescription());
    destination.setName(source.getName());
    destination.setOwnerRegNr(source.getOrganization() == null ? null : source.getOrganization().getRegnr());
    if (iterate) {
      destination.setVersions(versionConverter.convert(source.getClassifierVersions(), true));
    }

    return destination;
  }

}
