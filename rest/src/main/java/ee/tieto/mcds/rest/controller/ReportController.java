/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.Report;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.service.ReportService;
import ee.tieto.mcds.rest.service.report.ReportScheduled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/report")
@Validated
public class ReportController {

  private final ReportService reportService;
  private final ReportScheduled reportScheduled;

  @Autowired
  public ReportController(ReportService reportService, ReportScheduled reportScheduled) {
    this.reportService = reportService;
    this.reportScheduled = reportScheduled;
  }

  @GetMapping(value = "/status", produces = "application/json;charset=UTF-8")
  @ResponseBody
  public List<Report> getReports(
          Authentication authentication,
          @Valid @NotEmpty @RequestParam(name = "regNr") List<String> regnrList,
          @RequestParam(name = "reportStatus", required = false) String status,
          @Valid @NotBlank @RequestParam(name = "reportType") String reportType,
          @RequestParam(name = "fromDate", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate startDate,
          @RequestParam(name = "untilDate", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate endDate) {
    try {

      List<Report> reports = reportService.getReports(
          authentication.getName(), regnrList, status, reportType, startDate, endDate);
      return reports;
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @GetMapping(value = "/preview/{id}", produces = "application/xml;charset=UTF-8")
  @ResponseBody
  public String getReportContent(
      Authentication authentication,
      @PathVariable("id") String id) {
    try {
      return reportService.getReportContent(authentication.getName(), id);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  @GetMapping(value = "/cancel/{id}", produces = "application/json;charset=UTF-8")
  @ResponseBody
  public Report cancelReport(
      Authentication authentication,
      @PathVariable("id") String id) {
    try {
      return reportService.cancelReport(authentication.getName(), id);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  @GetMapping(value = "/generate", produces = "application/json;charset=UTF-8")
  public ResponseEntity<Void> generate(
      @RequestParam(value = "force", required = false, defaultValue = "false") boolean force,
      @RequestParam(value = "reportType") ReportType reportType,
      @RequestParam(value = "regNr") String regNr,
      @RequestParam(value = "periodDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          LocalDate periodDate) {
    reportScheduled.execute(force, regNr, periodDate, reportType);
    return new ResponseEntity<>(null, HttpStatus.OK);
  }

  @GetMapping(value = "/send/{id}")
  @ResponseBody
  public void sendReport(@PathVariable("id") String id) {
    try {
      reportService.sendEntry(id);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @GetMapping(value = "/estat/returnData/{submitId}")
  @ResponseBody
  public void getEstatReturnData(@PathVariable("submitId") String id) {
    try {
      reportService.estatReturnData(id);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @GetMapping(value = "/estat/returnError/{submitId}")
  @ResponseBody
  public void getEstatReturnError(@PathVariable("submitId") String id) {
    try {
      reportService.estatReturnError(id);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }
}
