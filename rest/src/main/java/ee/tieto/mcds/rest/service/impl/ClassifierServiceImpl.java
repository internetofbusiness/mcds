/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.rest.McdsDataIntegrityException;
import ee.tieto.mcds.rest.McdsNotFoundException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.converter.Converter;
import ee.tieto.mcds.rest.model.*;
import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.ClassifierRepository;
import ee.tieto.mcds.rest.repository.ClassifierVersionRepository;
import ee.tieto.mcds.rest.repository.model.ClassifierEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierVersionEntity;
import ee.tieto.mcds.rest.service.ClassifierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ClassifierServiceImpl implements ClassifierService {
  private static final Logger log = LoggerFactory.getLogger(ClassifierServiceImpl.class);

  private final ClassifierRepository repo;
  private final ClassifierItemRepository clItemRepo;
  private final ClassifierVersionRepository clVerRepo;
  private final Converter<ClassifierEntity, Classifier> clConverter;
  private final Converter<ClassifierVersionEntity, ClassifierVersion> clvConverter;
  private final Converter<ClassifierItemEntity, ClassifierItem> ciConverter;
  private final ClassifierEstatImporter classifierEstatImporter;

  @Autowired
  public ClassifierServiceImpl(
      ClassifierRepository repo,
      ClassifierItemRepository clItemRepo,
      ClassifierVersionRepository clVerRepo,
      Converter<ClassifierEntity, Classifier> clConverter,
      Converter<ClassifierVersionEntity, ClassifierVersion> clvConverter, Converter<ClassifierItemEntity, ClassifierItem> ciConverter, ClassifierEstatImporter classifierEstatImporter) {
    this.repo = repo;
    this.clItemRepo = clItemRepo;
    this.clVerRepo = clVerRepo;
    this.clConverter = clConverter;
    this.clvConverter = clvConverter;
    this.ciConverter = ciConverter;
    this.classifierEstatImporter = classifierEstatImporter;
  }

  @Override
  @Transactional
  public Classifier getClassifier(String classifierCode, String versionCode) {
    try {
      if (versionCode == null) {
        return getClassifierInternal(classifierCode);
      }
      return getClassifierInternal(classifierCode, versionCode);
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  @Override
  @Transactional
  public void getExternalClassifierStat() {
    List<StatClassifier> classifiers = classifierEstatImporter.importClassifiers();
    if (classifiers == null || classifiers.isEmpty()) {
      return;
    }

    classifiers.forEach(c-> getExternalClassifierStat(c.getCodeListCode(), null));
  }

  @Override
  @Transactional
  public List<Classifier> getClassifierList() {
    List<ClassifierEntity> classifierEntries = repo.findAll();
    List<Classifier> classifiers = clConverter.convert(classifierEntries, false);
    Map<String, Classifier> map = classifiers.stream().collect(Collectors.toMap(Classifier::getCode, c->c));
    classifierEntries.forEach(c-> {
      List<ClassifierVersionEntity> versions = c.getClassifierVersions();
      map.get(c.getCode()).setVersions(clvConverter.convert(versions, false));
    });

    return classifiers;
  }

  @Override
  @Transactional
  public void getExternalClassifierStat(String classifierCode, LocalDate date) {
    log.debug("Importing classifier {}", classifierCode);
    List<StatClassifierElement> classifiers = classifierEstatImporter.importClassifierElement(classifierCode, date);
    log.debug("Elements - {}", classifiers.size());
    if (classifiers.isEmpty()) {
      return;
    }

    // Classifier
    StatClassifierElement cl = classifiers.get(0);
    List<ClassifierEntity> entities = repo.findClassifier(cl.getCodeListCode());
    if (entities.size() > 1) throw new McdsDataIntegrityException("Classifier by code - expected one, found " + entities.size());
    if (entities.size() == 1) repo.delete(entities.get(0));
    ClassifierEntity e = new ClassifierEntity();
    e.setCode(cl.getCodeListCode());
    e.setName(cl.getCodeListName());
    e.setClassifierItems(new ArrayList<>());
    // Classifier item
    int counter = 0;
    for (StatClassifierElement c : classifiers) {
      ClassifierItemEntity i;
      List<ClassifierItemEntity> items = clItemRepo.findClassifierItemByCodes(c.getCodeListCode(), c.getCode());
      if (items.isEmpty()) {
        i = new ClassifierItemEntity();
      } else if (items.size() > 1) {
        throw new McdsDataIntegrityException("ClassifierItem by classifer code, item code - expected one, found " + items.size());
      } else {
        i = items.get(0);
      }
      i.setClassifier(e);
      i.setCode(c.getCode());
      i.setText(c.getName_ET());  // TODO: Mitmekeelsust ei ole. Üle vaadata, kas ET või EN (põgusalt peale vaadates tundub, et stati klf on eestikeelne nimetus olemas, kuid ingilsekeelne võib puududa)
      i.setValidFrom(LocalDate.parse(c.getValidFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
      if (c.getValidUntilDate() != null && !c.getValidUntilDate().isBlank()) {
        i.setValidUntil(LocalDate.parse(c.getValidUntilDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
      }
      e.getClassifierItems().add(i);
      log.trace("{}. Item {}-{}", counter++, classifierCode, e.getCode());
    }
    repo.save(e);
    // lisame parent klassifikaatorid teise tsükliga, sest otsime eelnevalt lisatud klf seast ja et nad oleks olemas
    counter = 0;
    for (StatClassifierElement c : classifiers) {
      if (c.getParentCode() != null) {
        log.trace("{}. {}-{} parent - {}", counter++, classifierCode, c.getCode(), c.getParentCode());
        ClassifierItemEntity i = clItemRepo.findClassifierItemByCodes(c.getCodeListCode(), c.getCode()).get(0);
        i.setParentItem(clItemRepo.findClassifierItemByCodes(c.getCodeListCode(), c.getParentCode()).get(0));
      }
    }
  }

  private Classifier getClassifierInternal(String classifierCode, String versionCode) {
    List<ClassifierVersionEntity> versionEntities = clVerRepo.findClassifier(classifierCode, versionCode);
    if (versionEntities == null || versionEntities.isEmpty()) {
      throw new McdsNotFoundException("Classifier NOT FOUND");
    }

    Classifier cl = clConverter.convert(versionEntities.stream().findFirst().get().getClassifier(), false);
    List<ClassifierVersion> versions = clvConverter.convert(versionEntities);
    cl.setVersions(versions);
    return cl;
  }

  private Classifier getClassifierInternal(String classifierCode) {
    List<ClassifierEntity> entities = repo.findClassifier(classifierCode);
    if (entities == null || entities.isEmpty()) {
      throw new McdsNotFoundException("Classifier NOT FOUND");
    }

    ClassifierEntity entry = entities.get(0);
    List<ClassifierItemEntity> itemEntities = entry.getClassifierItems();
    Classifier classifier = clConverter.convert(entry);
    if (classifier.getVersions() == null || classifier.getVersions().isEmpty()) {
      ClassifierVersion cv = new ClassifierVersion();
      cv.setItems(ciConverter.convert(itemEntities, false));
      List<ClassifierVersion> versions = new ArrayList<>(1);
      versions.add(cv);
      classifier.setVersions(versions);
    }
    return classifier;
  }

}
