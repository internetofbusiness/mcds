/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report.model;

import ee.tieto.mcds.rest.repository.model.ClassifierAttributeValueEntity;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class StatSalaryReportMetadata extends ReportMetadata {
  public enum AttributeName {
    ACCOUNT_SUB_TYPE("ACCOUNT_SUB_TYPE", true),
    CREATOR("CREATOR", false),
    DEFAULT_CURRENCY("DEFAULT_CURRENCY", false),
    ENTRIES_COMMENT("ENTRIES_COMMENT", false),
    ENTRIES_TYPE("ENTRIES_TYPE", false),
    LANGUAGE("LANGUAGE", false),
    SOURCE_APPLICATION("SOURCE_APPLICATION", false),
    SOURCE_JOURNAL("SOURCE_JOURNAL_ID", true),
    UNIT("UNIT_ID", false);

    final String attributeName;
    final boolean multivalued;

    AttributeName(String name, boolean multivalued) {
      this.attributeName = name;
      this.multivalued = multivalued;
    }

    public boolean isMultivalued() {
      return this.multivalued;
    }

    public static AttributeName ofAttributeName(String name) {
      for (AttributeName e : AttributeName.values()) {
        if (e.attributeName.equals(name)) {
          return e;
        }
      }
      return null;
    }
  }

  private String unit;
  private String entriesType;
  private String language;
  private String creatorName;
  private String comment;
  private String reportSource;
  private String currency;
  private Set<String> accountSubType;
  private Set<String> sourceJournalId;


  private StatSalaryReportMetadata(ClassifierItemEntity clItem) {
    super(clItem);
    List<ClassifierAttributeValueEntity> cav = clItem.getClassifierAttributeValues();
    cav.forEach(this::setValue);
  }

  public static Optional<ReportMetadata> fromEntity(ClassifierItemEntity clItem) {
    StatSalaryReportMetadata metadata = new StatSalaryReportMetadata(clItem);
    return Optional.of(metadata);
  }

  private void setValue(ClassifierAttributeValueEntity value) {
    AttributeName name = AttributeName.ofAttributeName(value.getClassifierAttribute().getCode());
    String val = value.getValue();
    if (name == null) {
      return;
    }
    switch (name) {
      case ACCOUNT_SUB_TYPE:
        if (this.accountSubType == null) {
          this.accountSubType = new HashSet<>();
        }
        this.accountSubType.add(val);
        break;
      case CREATOR:
        this.creatorName = val;
        break;
      case DEFAULT_CURRENCY:
        this.currency = val;
        break;
      case ENTRIES_COMMENT:
        this.comment = val;
        break;
      case ENTRIES_TYPE:
        this.entriesType = val;
        break;
      case LANGUAGE:
        this.language = val;
        break;
      case SOURCE_APPLICATION:
        this.reportSource = val;
        break;
      case SOURCE_JOURNAL:
        if (this.sourceJournalId == null) {
          this.sourceJournalId = new HashSet<>();
        }
        this.sourceJournalId.add(val);
        break;
      case UNIT:
        this.unit = val;
        break;
    }
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getEntriesType() {
    return entriesType;
  }

  public void setEntriesType(String entriesType) {
    this.entriesType = entriesType;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getCreatorName() {
    return creatorName;
  }

  public void setCreatorName(String creatorName) {
    this.creatorName = creatorName;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getReportSource() {
    return reportSource;
  }

  public void setReportSource(String reportSource) {
    this.reportSource = reportSource;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Set<String> getAccountSubType() {
    return accountSubType;
  }

  public void setAccountSubType(Set<String> accountSubType) {
    this.accountSubType = accountSubType;
  }

  public Set<String> getSourceJournalId() {
    return sourceJournalId;
  }

  public void setSourceJournalId(Set<String> sourceJournalId) {
    this.sourceJournalId = sourceJournalId;
  }

}
