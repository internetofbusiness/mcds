/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "acc_entry_repoort_type", schema = "mcds", catalog = "mcds")
public class AccEntryRepoortTypeEntity {

  @Id
  @Column(name = "id", nullable = false)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="id_acen")
  private AccountingEntryEntity accountingEntry;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="report_type")
  private ClassifierItemEntity reportType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public AccountingEntryEntity getAccountingEntry() {
    return accountingEntry;
  }

  public void setAccountingEntry(AccountingEntryEntity accountingEntry) {
    this.accountingEntry = accountingEntry;
  }

  public ClassifierItemEntity getReportType() {
    return reportType;
  }

  public void setReportType(ClassifierItemEntity reportType) {
    this.reportType = reportType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AccEntryRepoortTypeEntity)) {
      return false;
    }

    AccEntryRepoortTypeEntity other = (AccEntryRepoortTypeEntity) o;
    return id != null && id.equals(other.getId());
  }

  @Override
  public int hashCode() {
    return 31;
  }
}
