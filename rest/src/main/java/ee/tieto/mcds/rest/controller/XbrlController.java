/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.McdsValidationException;
import ee.tieto.mcds.rest.model.PackageStatus;
import ee.tieto.mcds.rest.service.XbrlService;
import ee.tieto.mcds.validator.ValidationResult;
import ee.tieto.mcds.validator.ValidationResult.MessageRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/xbrl")
public class XbrlController {

  private final XbrlService xbrlService;

  @Autowired
  public XbrlController(XbrlService xbrlService) {
    this.xbrlService = xbrlService;
  }

  // Kirjendite vastuvõtmine UC-001
  @PostMapping(value = "/entries/add", produces = "application/json;charset=UTF-8", consumes = "application/xml")
  public ResponseEntity<PackageStatus> xbrlAdd(
      Authentication aut,
      InputStream body,
      @RequestHeader(name = "xbrlVersion") String xbrlVersion) {

    try {
      String xbrl = StreamUtils.copyToString(body, StandardCharsets.UTF_8);
      if (xbrl == null || xbrl.isBlank()) {
        throw new McdsValidationException("xbrl is empty");
      }
      String sessionId = xbrlService.saveXbrl(xbrl, aut.getName(), xbrlVersion);

      //TODO run validator asynchroneously
      ValidationResult result = xbrlService.validate(xbrl, sessionId, xbrlVersion);
      PackageStatus status = buildPackageStatus(sessionId, xbrlVersion, OffsetDateTime.now(), result);
      if (!result.isValid()) {
        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
      }

      result = xbrlService.process(sessionId);
      status = buildPackageStatus(sessionId, xbrlVersion, OffsetDateTime.now(), result);
      return new ResponseEntity<>(status, HttpStatus.OK);
    } catch (Exception e) {
      if (e instanceof McdsException) {
        throw (McdsException)e;
      }
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  private PackageStatus buildPackageStatus(String sessionId, String version, OffsetDateTime timeOfReceiving, ValidationResult result) {
    PackageStatus pgStatus = new PackageStatus();
    pgStatus.setSessionId(sessionId);
    pgStatus.setTimeOfReceiving(timeOfReceiving);
    pgStatus.setXbrlGlVersion(version);
    if (result == null || result.isValid()) {
      pgStatus.setStatus(ee.tieto.mcds.rest.repository.constants.PackageStatus.SCHEMA_VALID);
    } else {
      pgStatus.setStatus(ee.tieto.mcds.rest.repository.constants.PackageStatus.SCHEMA_NOT_VALID);
      for (MessageRow message : result.getMessages()) {
        pgStatus.addError(message.getSeverity(), message.getMessage());
      }
    }
    return pgStatus;
  }

  // Kirjendi ja selle valideerimise tulemuste väljastamine UC-003
  @GetMapping(value = "/entries/status", produces = "application/json;charset=UTF-8")
  @ResponseBody
  public List<PackageStatus> xbrlStatus(
          Authentication aut,
          @RequestParam(name = "sessionId", required = false) UUID sessionId,
          @RequestParam(name = "receivedFrom", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate receivedFrom,
          @RequestParam(name = "receivedUntil", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate receivedUntil,
          @RequestParam(name = "entryIdentifier", required = false) String entryIdentifier) {
    try {
      return xbrlService.getEntriesStatus(aut.getName(), sessionId, entryIdentifier, receivedFrom, receivedUntil);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

  // IOB-UC-004 Kirjendite väljastamine
  @GetMapping(value = "/entries/{regNr}", produces = "application/json;charset=UTF-8")
  @ResponseBody
  public List<String> getEntries(@PathVariable("regNr") String organizationIdentifier,
                                           @RequestParam(value = "fromDate", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate fromDate,
                                           @RequestParam(value = "untilDate", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate untilDate,
                                           @RequestParam(value = "entriesType", required = false) String entriesType,
                                           @RequestParam(value = "entryIdentifier", required = false) Set<String> entryIdentifiers) {

    try {
      return xbrlService.getEntries(organizationIdentifier, null, fromDate,
          untilDate, entriesType, entryIdentifiers);
    } catch (McdsException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException("Unexpected system error", e);
    }
  }

}
