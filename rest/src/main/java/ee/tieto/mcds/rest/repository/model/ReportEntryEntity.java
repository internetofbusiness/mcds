/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "report_entry", schema = "mcds", catalog = "mcds")
public class ReportEntryEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "id_repo", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ReportEntity report;

    @JoinColumn(name = "id_acen", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private AccountingEntryEntity accountingEntry;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ReportEntity getReport() {
        return report;
    }

    public void setReport(ReportEntity report) {
        this.report = report;
    }

    public AccountingEntryEntity getAccountingEntry() {
        return accountingEntry;
    }

    public void setAccountingEntry(AccountingEntryEntity accountingEntry) {
        this.accountingEntry = accountingEntry;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ReportEntryEntity))
            return false;

        ReportEntryEntity other = (ReportEntryEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
