/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import ee.tieto.mcds.rest.model.Permission;
import ee.tieto.mcds.rest.repository.model.PermissionEntity;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
public class EntityToRestPermission extends Converter<PermissionEntity, Permission> {

  public Permission convert(PermissionEntity source, Permission destination, boolean iterate) {
    if (source == null) {
      return null;
    }

    destination.setErpCode(source.getErp().getCode());
    destination.setRegNr(source.getOrganization() == null ? null : source.getOrganization().getRegnr());
    destination.setBusinessArea(source.getBusinessArea());
    destination.setReportType(source.getReportType() == null ? null : source.getReportType().getCode());
    if (source.getDeactivated() == null || source.getDeactivated().compareTo(OffsetDateTime.now()) > 0) {
      destination.setActive(true);
    } else {
      destination.setActive(false);
    }

    return destination;
  }
}
