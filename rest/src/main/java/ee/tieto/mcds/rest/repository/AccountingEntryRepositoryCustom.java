/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository;

import ee.tieto.mcds.rest.repository.model.AccountingEntryEntity;
import ee.tieto.mcds.rest.repository.model.AccountingEntryEntityLightReadOnly;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

public interface AccountingEntryRepositoryCustom {
  List<AccountingEntryEntity> findEntries(UUID sessionId, OffsetDateTime receivedFrom, OffsetDateTime receivedUntil,
                                          String entryIdentifier, String erpCode, List<String> regnrList);

  List<AccountingEntryEntity> findEntries(String erpCode, String regNr, OffsetDateTime from, OffsetDateTime until,
                                          String entryType, Set<String> entryIdentifiers, OffsetDateTime finTime);

  List<AccountingEntryEntity> findInvoiceEntries(String regNr, OffsetDateTime from, OffsetDateTime until,
                                                 String entryType, Set<String> entryIdentifiers, OffsetDateTime finTime);

  List<AccountingEntryEntityLightReadOnly> findEntries(String erpCode, UUID sessionId, String entryId,
                                                       OffsetDateTime from, OffsetDateTime until, OffsetDateTime findTime);

  List<AccountingEntryEntityLightReadOnly> findInvoiceEntries(UUID sessionId, String entryId,
                                                       OffsetDateTime from, OffsetDateTime until);

  Stream<AccountingEntryEntity> findOrganizationEntriesInPostingPeriod(String regNr, LocalDate fromDate, LocalDate untilDate);

  void detach(AccountingEntryEntity entity);

  Stream<AccountingEntryEntity> findAllCustom();

}
