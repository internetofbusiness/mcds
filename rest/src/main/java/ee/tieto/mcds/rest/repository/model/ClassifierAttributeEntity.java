/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "classifier_attribute", schema = "mcds", catalog = "mcds")
public class ClassifierAttributeEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "code", nullable = false)
    private String code;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_clas")
    private ClassifierEntity classifier;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "format", nullable = false)
    @Enumerated(EnumType.STRING)
    private Format format;

//    @OneToMany(mappedBy = "classifierAttribute", fetch = FetchType.EAGER)
//    private List<ClassifierAttributeValueEntity> classifierAttributeValues;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ClassifierEntity getClassifier() {
        return classifier;
    }

    public void setClassifier(ClassifierEntity classifier) {
        this.classifier = classifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

//    public List<ClassifierAttributeValueEntity> getClassifierAttributeValues() {
//        return classifierAttributeValues;
//    }
//
//    public void setClassifierAttributeValues(List<ClassifierAttributeValueEntity> classifierAttributeValues) {
//        this.classifierAttributeValues = classifierAttributeValues;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ClassifierAttributeEntity))
            return false;

        ClassifierAttributeEntity other = (ClassifierAttributeEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
