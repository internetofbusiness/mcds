/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.impl;

import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.converter.EntityToRestEventLog;
import ee.tieto.mcds.rest.model.EventLog;
import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.EventLogRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.constants.Constants;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.model.EventLogEntity;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.McdsDataIntegrityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class EventLogServiceImpl implements EventLogService {

  private final ClassifierItemRepository classItemRepo;
  private final EventLogRepository eventRepo;
  private final EntityToRestEventLog eventConverter;

  @Autowired
  public EventLogServiceImpl(ClassifierItemRepository classItemRepo, EventLogRepository eventRepo, EntityToRestEventLog eventConverter) {
    this.classItemRepo = classItemRepo;
    this.eventRepo = eventRepo;
    this.eventConverter = eventConverter;
  }


  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void newEvent(EventType eventType, String erpCode, String relatedRecords, Boolean success, String message) {
    if (eventType == null) {
      throw new McdsSystemException("evenType can't be null");
    }
    if (success == null) {
      throw new McdsSystemException("Boolean success can't be null");
    }
    EventLogEntity entity = new EventLogEntity();
    entity.setEventType(classItemRepo.findByClassifierCodeAndItemCode(
        Classifier.EVENT_TYPE.name(), eventType.name())
        .orElseThrow(
            () -> new McdsDataIntegrityException(String.format("Can not find item: classifier=%s item=%s",
                Classifier.EVENT_TYPE.name(), eventType.name())
            )));
    if (erpCode != null && !Constants.SYSTEM_ERP_CODE.equals(erpCode)) {
      entity.setErp(classItemRepo.findByClassifierCodeAndItemCode(
          Classifier.ERP.name(), erpCode)
          .orElseThrow(
              () -> new McdsDataIntegrityException(String.format("Can not find erp: code=%s", erpCode))
          ));
    }
    entity.setRelatedRecords(relatedRecords);
    entity.setSuccess(success);
    entity.setMessage(message);
    eventRepo.save(entity);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void newEventNewTransaction(EventType eventType, String erpCode, String relatedRecords, Boolean success, String message) {
    newEvent(eventType, erpCode,  relatedRecords, success, message);
  }

  @Override
  @Transactional
  public List<EventLog> findEvents(OffsetDateTime startTime, OffsetDateTime endTime, Boolean success, List<String> eventType) {
    return eventConverter.convert(eventRepo.findEvents(startTime, endTime, success, eventType));
  };
}
