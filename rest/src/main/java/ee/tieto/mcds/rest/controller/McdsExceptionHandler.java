/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.controller;

import ee.tieto.mcds.McdsException;
import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.model.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;


@ControllerAdvice
public class McdsExceptionHandler extends ResponseEntityExceptionHandler {
  private static final Logger log = LoggerFactory.getLogger(McdsExceptionHandler.class);

  @ExceptionHandler(McdsException.class)
  public final ResponseEntity<Object> handleMcdsRestException(McdsException e) {
    if (e instanceof McdsSystemException) {
      log.error(e.getMessage(), e);
    } else {
      log.error(e.getMessage());
    }
    Error error = new Error(e.getCode(), e.getMessage());
    return new ResponseEntity<>(error, HttpStatus.resolve(e.getHttpStatus()));
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  public ResponseEntity<Error> handleConstraintViolationException(ConstraintViolationException exception) {
    final StringBuilder message = new StringBuilder("[");
    int count = 0;
    int size = exception.getConstraintViolations().size();
    for (ConstraintViolation<?> cv : exception.getConstraintViolations()) {
      message.append("{")
          .append("name:").append(cv.getPropertyPath()).append(", ")
          .append("message:\"").append(cv.getMessage()).append("\", ")
          .append("value:").append(cv.getInvalidValue())
          .append("}");
      if (count < size-1) {
        message.append(",");
      }
      count++;
    }

    Error error = new Error(400, message.toString());
    return new ResponseEntity<>(error, HttpStatus.resolve(400));
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    Error err; // = new Error(status.value(), null);
    BindingResult bres = ex.getBindingResult();
    if (bres.hasErrors()) {
      final StringBuilder message = new StringBuilder();
      for (ObjectError e : bres.getAllErrors()) {
        message.append("Error in object ").append(e.getObjectName()).append(" - ")
            .append(e.getDefaultMessage()).append("; ");
      }
      err = new Error(status.value(), message.toString());
    } else {
      err = new Error(status.value(), "Unknown binding error");
    }
    return handleExceptionInternal(ex, err, headers, status, request);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(
      Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

    if (HttpStatus.INTERNAL_SERVER_ERROR == status) {
      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
    }
    Object bodyInternal = body;
    if (bodyInternal == null) {
      bodyInternal = new Error(status.value(), ex.getMessage());
    }
    return new ResponseEntity<>(bodyInternal, headers, status);
  }

}
