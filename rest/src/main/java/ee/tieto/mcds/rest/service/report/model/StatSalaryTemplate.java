/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report.model;

import ee.tieto.mcds.McdsSystemException;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.service.report.ReportConfig;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class StatSalaryTemplate {
  private static final Logger log = LoggerFactory.getLogger(StatSalaryTemplate.class);

  public static Document setContent(Document doc, StatSalaryReportMetadata meta, OrganizationEntity org,
                                    String uId, OffsetDateTime genTime) {
    DateTimeFormatter simpleDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    try {
      Element elem = getElement(doc.getRootElement(), "xbrli:context");
      setText(elem, "xbrli:entity/xbrli:identifier", org.getRegnr());
      setAttributeText(elem, "xbrli:entity/xbrli:identifier", "scheme", org.getRegAuthority());
      setText(elem, "xbrli:period/xbrli:instant", genTime.format(simpleDate));
      setText(doc.getRootElement(), "xbrli:unit[@id='currency_unit_placeholder']/xbrli:measure", meta.getUnit());
      setAttributeText(doc.getRootElement(), "xbrli:unit[@id='currency_unit_placeholder']", "id", meta.getUnit().substring(meta.getUnit().indexOf(":") + 1).toLowerCase());
      Element entriesElem = getElement(doc.getRootElement(), "gl-cor:accountingEntries");
      elem = getElement(entriesElem, "gl-cor:documentInfo");
      setText(elem, "gl-cor:uniqueID", uId);
      setText(elem, "gl-cor:language", meta.getLanguage());
      setText(elem, "gl-cor:creationDate", genTime.format(simpleDate));
      setText(elem, "gl-bus:creator", meta.getCreatorName());
      setText(elem, "gl-cor:entriesComment", meta.getComment());
      setText(elem, "gl-cor:periodCoveredStart", meta.getPeriodStart().format(simpleDate));
      setText(elem, "gl-cor:periodCoveredEnd", meta.getPeriodEnd().format(simpleDate));
      setText(elem, "gl-bus:sourceApplication", meta.getReportSource());
      setText(elem, "gl-muc:defaultCurrency", meta.getCurrency());
      elem = getElement(entriesElem, "gl-cor:entityInformation/gl-bus:organizationIdentifiers");
      setText(elem, "gl-bus:organizationIdentifier", org.getRegnr());
      setText(elem, "gl-bus:organizationDescription", org.getRegAuthority());
      return doc;
    } catch (McdsSystemException e) {
      throw e;
    } catch (Exception e) {
      throw new McdsSystemException(e.getMessage(), e);
    }
  }

  public static String createId(OrganizationEntity org, OffsetDateTime dateTime) {
    return org.getRegnr() + "-" + dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-nnnnnnnnn"));
  }

  public static Document loadTemplate (ReportConfig reportConfig, StatSalaryReportMetadata meta) {
    File file = new File(reportConfig.getPath());
    String template = reportConfig.getTemplate(meta.getReportType());
    if (template == null) {
      throw new McdsSystemException("Report config for report " + meta.getItemVersion() + " not found");
    }
    file = new File(file, template);
    if (!file.exists()) {
      throw new McdsSystemException("Can not find report template - " + file.getAbsolutePath());
    }

    DocumentFactory dFactory = DocumentFactory.getInstance();
    try {
      SAXReader reader = new SAXReader(dFactory);
      return reader.read(file);
    } catch (Exception e) {
      throw new McdsSystemException(e);
    }
  }

  private static void setText(Element elem, String path, String value) {
    try {
      getElement(elem, path).setText(value);
    } catch (Exception e) {
      throw new McdsSystemException("Report template init exception - path:" + path);
    }
  }

  private static void setAttributeText(Element elem, String path, String attribute, String value) {
    try {
      getElement(elem, path).attribute(attribute).setValue(value);
    } catch (Exception e) {
      throw new McdsSystemException("Report template init exception - path:" + path);
    }
  }

  private static Element getElement(Element elem, String path) {
    try {
      Node node = createXpath(path).selectSingleNode(elem);
      return (Element)node;
    } catch (Exception e) {
      throw new McdsSystemException("Report template init exception - path:" + path);
    }
  }

  private static final Map<String, String> pathNamespaces = new HashMap<>();
  static {
    pathNamespaces.put("xbrli", "http://www.xbrl.org/2003/instance");
    pathNamespaces.put("gl-cor", "http://www.xbrl.org/int/gl/cor/2015-03-25");
    pathNamespaces.put("gl-bus", "http://www.xbrl.org/int/gl/bus/2015-03-25");
    pathNamespaces.put("gl-muc", "http://www.xbrl.org/int/gl/muc/2015-03-25");
  }

  private static XPath createXpath(String path) {
    XPath xPath = DocumentHelper.createXPath(path);
    xPath.setNamespaceURIs(pathNamespaces);
    return xPath;
  }

}
