/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.converter;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Converter<S, D> {

  public D convert(S source) {
    return convert(source, true);
  }

  public D convert(S source, boolean iterate) {
    D destination = createDestination();
    return convert(source, destination, iterate);
  }


  public D convert(S source, D destination) {
    return convert(source, destination, true);
  }

  public abstract D convert(S source, D destination, boolean iterate);



  public List<D> convert(List<S> source) {
    return convert(source, true);
  }

  public List<D> convert(List<S> source, boolean iterate) {
    if (source == null) {
      return null;
    }
    return source.stream().map(s -> convert(s, iterate)).collect(Collectors.toList());
  }


  @SuppressWarnings("unchecked")
  private D createDestination() {
    ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
    @SuppressWarnings("unchecked")
    Class<D> type = (Class<D>) superClass.getActualTypeArguments()[1];
    try {
      Constructor<D> constructor = type.getDeclaredConstructor();
      return constructor.newInstance();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
