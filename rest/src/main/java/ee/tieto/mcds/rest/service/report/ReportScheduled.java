/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service.report;

import ee.tieto.mcds.rest.repository.OrganizationRepository;
import ee.tieto.mcds.rest.repository.PermissionRepository;
import ee.tieto.mcds.rest.repository.constants.Constants;
import ee.tieto.mcds.rest.repository.constants.EventType;
import ee.tieto.mcds.rest.repository.constants.ReportType;
import ee.tieto.mcds.rest.repository.model.OrganizationEntity;
import ee.tieto.mcds.rest.service.AbstractService;
import ee.tieto.mcds.rest.service.EventLogService;
import ee.tieto.mcds.rest.service.McdsRestrictedException;
import ee.tieto.mcds.rest.service.report.model.ReportMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@Component
public class ReportScheduled extends AbstractService {
  private static final Logger log = LoggerFactory.getLogger(ReportScheduled.class);

  //TODO load from config
  private static final String REPORT_VERSION = "REPORT_TYPE_V1";

  private final MetadataLoaderService metaLoader;
  private final OrganizationRepository orgRepo;
  private final ReportGeneratorFactory generatorFactory;
  private final EventLogService eventLogService;
  private final PermissionRepository permRepo;

  public ReportScheduled(
      MetadataLoaderService metaLoader,
      OrganizationRepository orgRepo,
      ReportGeneratorFactory generatorFactory, EventLogService eventLogService, PermissionRepository permRepo) {

    this.metaLoader = metaLoader;
    this.orgRepo = orgRepo;
    this.generatorFactory = generatorFactory;
    this.eventLogService = eventLogService;
    this.permRepo = permRepo;
  }

  @Scheduled(cron="0 0 2 * * *") //Every day at 02:00
  public void scheduler() {
    log.info("Scheduler started");
    try {
      execute(false, null, null, null);
      submitReports();
      eventLogService.newEvent(
          EventType.SCHEDULER_EXECUTED,
          Constants.SYSTEM_ERP_CODE,
          null,
          true,
          "ReportScheduled"
      );
    } catch (Exception e) {
      eventLogService.newEvent(
          EventType.SCHEDULER_EXECUTED,
          Constants.SYSTEM_ERP_CODE,
          null,
          false,
          "ReportScheduled - " + e.getMessage()
      );
      throw e;
    }
  }

  public void submitReports() {

  }

  public void execute(boolean force, String regNr, LocalDate dateInPeriod, ReportType reportType) {
    log.debug("Execute with parameters force={} regNr={} dateInPeriod={}", force, regNr, dateInPeriod);
    List<ReportMetadata> reportMetadatas = metaLoader.loadReportMetadata(REPORT_VERSION);
    if (reportMetadatas == null) {
      log.debug("Metadata loaded for 0 reports");
      return;
    }
    log.debug("Metadata loaded for {} reports", reportMetadatas.size());

    for (ReportMetadata meta : reportMetadatas) {
      if (reportType != null && reportType != meta.getReportType()) {
        continue;
      }
      if (dateInPeriod != null) {
        meta.setOtherPeriod(dateInPeriod, null);
      }
      if (!force && !meta.isMustGenerate()) {
        continue;
      }
      log.debug("Metadata {} initialized, dates are: period={}, periodStart={}, periodEnd={}, generationDate={}, submissionDate={}",
          meta.getReportType().name(), meta.getPeriod().name(), meta.getPeriodStart(),
          meta.getPeriodEnd(), meta.getGenerationDate(), meta.getSubmissionDate());

      List<OrganizationEntity> reportOrganizations =
          orgRepo.findOrganizationsForActiveReportType(meta.getReportType().name(), OffsetDateTime.now());
      log.debug("Found {} organizations for report {}",
          reportOrganizations == null ? 0 : reportOrganizations.size(), meta.getReportType().name());
      reportOrganizations.forEach(o -> reportToOrganization(o, meta, force, regNr));
    }
  }

  private void reportToOrganization(OrganizationEntity org, ReportMetadata meta, boolean force, String regNr) {
    if (regNr != null && !regNr.equals(org.getRegnr())) {
      return;
    }
    if (getUser() != null &&
        !permRepo.isPermittedForErp(
            getUser(), org.getRegnr(), null, OffsetDateTime.now(), false)) {
      throw new McdsRestrictedException("Organization " + org.getRegnr() + " is restricted for erp " + getUser());
    }
    generatorFactory.getGenerator(meta).generate(org, meta, force);
    log.debug("Report {} fro organization {} sucessfully created", meta.getReportType().name(), org.getRegnr());

  }
}
