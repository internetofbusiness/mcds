/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "permission", schema = "mcds", catalog = "mcds")
public class PermissionEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "erp", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassifierItemEntity erp;

    @JoinColumn(name = "id_orga")
    @ManyToOne(fetch = FetchType.EAGER)
    private OrganizationEntity organization;

    @Column(name = "business_area")
    private String businessArea;

    @JoinColumn(name = "report_type")
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassifierItemEntity reportType;

    @Column(name = "deactivated")
    private OffsetDateTime deactivated;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClassifierItemEntity getErp() {
        return erp;
    }

    public void setErp(ClassifierItemEntity erp) {
        this.erp = erp;
    }

    public OrganizationEntity getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationEntity organization) {
        this.organization = organization;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public ClassifierItemEntity getReportType() {
        return reportType;
    }

    public void setReportType(ClassifierItemEntity reportType) {
        this.reportType = reportType;
    }

    public OffsetDateTime getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(OffsetDateTime deactivated) {
        this.deactivated = deactivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PermissionEntity))
            return false;

        PermissionEntity other = (PermissionEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
