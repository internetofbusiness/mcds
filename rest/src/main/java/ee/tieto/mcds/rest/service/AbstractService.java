/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.service;

import ee.tieto.mcds.rest.repository.ClassifierItemRepository;
import ee.tieto.mcds.rest.repository.constants.Classifier;
import ee.tieto.mcds.rest.repository.model.ClassifierItemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public abstract class AbstractService {

  @Autowired
  protected ClassifierItemRepository classifierRepo;
  @Autowired
  protected EventLogService eventService;

  protected AbstractService() {
  }

  public String getUser() {
    SecurityContext holder = SecurityContextHolder.getContext();
    if (holder == null) {
      return null;
    }
    Authentication aut = holder.getAuthentication();
    if (aut == null) {
      return null;
    }
    Object princ = aut.getPrincipal();
    if (princ == null) {
      return null;
    }
    return ((User) princ).getUsername();
  }

  public ClassifierItemEntity getErp() {
    String user = getUser();
    if (user == null) {
      return null;
    }
    return classifierRepo.findByClassifierCodeAndItemCode(Classifier.ERP.name(), user).get();
  }
}
