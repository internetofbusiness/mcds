/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.model;

import java.util.List;

public class InvoiceInfo {
  private String content;
  private String compilationTime;
  private List<InvoiceStatus> status;

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getCompilationTime() {
    return compilationTime;
  }

  public void setCompilationTime(String compilationTime) {
    this.compilationTime = compilationTime;
  }

  public List<InvoiceStatus> getStatus() {
    return status;
  }

  public void setStatus(List<InvoiceStatus> status) {
    this.status = status;
  }
}
