/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import org.springframework.data.annotation.Immutable;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Immutable
@Entity
@Table(name = "accounting_entry", schema = "mcds", catalog = "mcds")
public class AccountingEntryEntityLightReadOnly {

  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "entry_uuid", nullable = false)
  private String entryUuid;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id_pack")
  private PackageEntityLightReadOnly pacage;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="id_orga")
  private OrganizationEntity organization;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name="entry_type", nullable = false)
  private ClassifierItemEntity entryType;

  @Column(name = "target_application")
  private String targetApplication;

  @Column(name = "valid", nullable = false)
  private Boolean valid;

  @Column(name = "rules_name")
  private String rulesName;

  @Column(name = "source_journal_id")
  private String sourceJournalId;

  @Column(name = "created", nullable = false)
  private OffsetDateTime created;

  @OneToMany(mappedBy = "accountingEntry", fetch = FetchType.LAZY)
  private List<ValidationResultEntity> validationResults;

  @OneToMany(mappedBy = "accountingEntry", fetch = FetchType.LAZY)
  private List<ReportEntryEntity> reportEntryes;

  @OneToMany(mappedBy = "accountingEntry", fetch = FetchType.LAZY)
  private List<AccountingEntryDetailEntity> entryDetails;

  @OneToOne(mappedBy = "accountingEntry")
  private InvoiceEntity invoice;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEntryUuid() {
    return entryUuid;
  }

  public void setEntryUuid(String entryUuid) {
    this.entryUuid = entryUuid;
  }

  public PackageEntityLightReadOnly getPackage() {
    return pacage;
  }

  public void setPackage(PackageEntityLightReadOnly pacage) {
    this.pacage = pacage;
  }

  public OrganizationEntity getOrganization() {
    return organization;
  }

  public void setOrganization(OrganizationEntity organization) {
    this.organization = organization;
  }

  public ClassifierItemEntity getEntryType() {
    return entryType;
  }

  public void setEntryType(ClassifierItemEntity entryType) {
    this.entryType = entryType;
  }

  public String getTargetApplication() {
    return targetApplication;
  }

  public void setTargetApplication(String targetApplication) {
    this.targetApplication = targetApplication;
  }

  public Boolean getValid() {
    return valid;
  }

  public void setValid(Boolean valid) {
    this.valid = valid;
  }

  public String getRulesName() {
    return rulesName;
  }

  public void setRulesName(String rulesName) {
    this.rulesName = rulesName;
  }

  public String getSourceJournalId() {
    return sourceJournalId;
  }

  public void setSourceJournalId(String sourceJournalId) {
    this.sourceJournalId = sourceJournalId;
  }

  public List<AccountingEntryDetailEntity> getEntryDetails() {
    return entryDetails;
  }

  public void setEntryDetails(List<AccountingEntryDetailEntity> entryDetails) {
    this.entryDetails = entryDetails;
  }

  public OffsetDateTime getCreated() {
    return created;
  }

  public void setCreated(OffsetDateTime created) {
    this.created = created;
  }

  public List<ValidationResultEntity> getValidationResults() {
    return validationResults;
  }

  public void setValidationResults(List<ValidationResultEntity> validationResults) {
    this.validationResults = validationResults;
  }

  public List<ReportEntryEntity> getReportEntryes() {
    return reportEntryes;
  }

  public void setReportEntryes(List<ReportEntryEntity> reportEntryes) {
    this.reportEntryes = reportEntryes;
  }

  public PackageEntityLightReadOnly getPacage() {
    return pacage;
  }

  public void setPacage(PackageEntityLightReadOnly pacage) {
    this.pacage = pacage;
  }

  public InvoiceEntity getInvoice() {
    return invoice;
  }

  public void setInvoice(InvoiceEntity invoice) {
    this.invoice = invoice;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AccountingEntryEntity)) {
      return false;
    }

    AccountingEntryEntity other = (AccountingEntryEntity) o;
    return id != null && id.equals(other.getId());
  }

  @Override
  public int hashCode() {
    return 31;
  }

}
