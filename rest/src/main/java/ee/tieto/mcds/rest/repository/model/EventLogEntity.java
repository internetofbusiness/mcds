/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "event_log", schema = "mcds", catalog = "mcds")
public class EventLogEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "event_type", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassifierItemEntity eventType;

    @JoinColumn(name = "erp")
    @ManyToOne(fetch = FetchType.EAGER)
    private ClassifierItemEntity erp;

    @Column(name = "related_records")
    private String relatedRecords;

    @Column(name = "success", nullable = false)
    private Boolean success;

    @Column(name = "message", length = -1)
    private String message;

    @Column(name = "created", nullable = false)
    private OffsetDateTime created;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClassifierItemEntity getEventType() {
        return eventType;
    }

    public void setEventType(ClassifierItemEntity eventType) {
        this.eventType = eventType;
    }

    public ClassifierItemEntity getErp() {
        return erp;
    }

    public void setErp(ClassifierItemEntity erp) {
        this.erp = erp;
    }

    public String getRelatedRecords() {
        return relatedRecords;
    }

    public void setRelatedRecords(String relatedRecords) {
        this.relatedRecords = relatedRecords;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof EventLogEntity))
            return false;

        EventLogEntity other = (EventLogEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
