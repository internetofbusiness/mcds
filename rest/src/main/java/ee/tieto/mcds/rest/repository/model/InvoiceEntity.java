/*
 * My Company Data System (MCDS)
 * Copyright (C) 2020  Internet of Business mycompanydataservice@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ee.tieto.mcds.rest.repository.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Table(name = "invoice", schema = "mcds", catalog = "mcds")
public class InvoiceEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "id_acen", nullable = false)
    @OneToOne(fetch = FetchType.EAGER)
    private AccountingEntryEntityLightReadOnly accountingEntry;

    @Column(name = "invoice", nullable = false)
    private String invoice;

    @Column(name = "generation_time", nullable = false)
    private OffsetDateTime generationTime;

    @Column(name = "created", nullable = false)
    private OffsetDateTime created;

    @OneToMany(mappedBy = "invoice", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<InvoiceStatusEntity> invoiceStatuses;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AccountingEntryEntityLightReadOnly getAccountingEntry() {
        return accountingEntry;
    }

    public void setAccountingEntry(AccountingEntryEntityLightReadOnly accountingEntry) {
        this.accountingEntry = accountingEntry;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public OffsetDateTime getGenerationTime() {
        return generationTime;
    }

    public void setGenerationTime(OffsetDateTime generationTime) {
        this.generationTime = generationTime;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public List<InvoiceStatusEntity> getInvoiceStatuses() {
        return invoiceStatuses;
    }

    public void setInvoiceStatuses(List<InvoiceStatusEntity> invoiceStatuses) {
        this.invoiceStatuses = invoiceStatuses;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof InvoiceEntity))
            return false;

        InvoiceEntity other = (InvoiceEntity) o;
        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
